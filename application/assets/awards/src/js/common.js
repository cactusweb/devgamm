/**
 * Created by burre on 28.08.15.
 */

angular.module('DGCommon', [
    'ngResource'
])

    .service('appConfig', ['$location', '$http', '$q', function ($location, $http, $q) {
        var self = this;

        this.consoleUrl = $('#currentConferenceConsoleUrl').val();
        this.conference = $('#currentConferenceID').val();

    }])

    .factory('DGAServer', ['$http', 'appConfig', function ($http, appConfig) {
        return {
            checkToken: function (token) {
                return $http.get(appConfig.consoleUrl + '/awards/checktoken/?token=' + token + '&_conference=' + appConfig.conference);
            },
            getCategories: function () {
                return $http.get(appConfig.consoleUrl + '/gameCategoryNew/index?_conference=' + appConfig.conference);
            },
            getTechnologies: function () {
                return $http.get(appConfig.consoleUrl + '/gameTechnology/index?_conference=' + appConfig.conference);
            },
            getNominations: function () {
                return $http.get(appConfig.consoleUrl + '/gameNomination/index?_conference=' + appConfig.conference);
            },
            getShowcases: function () {
                return $http.get(appConfig.consoleUrl + '/showcase/schedule?_conference=' + appConfig.conference);
            },
            getSettings: function () {
                return $http.get(appConfig.consoleUrl + '/settings/index?_conference=' + appConfig.conference);
            },
            getBPData: function (token) {
                return $http.get(appConfig.consoleUrl + '/participant/getPartnerData?token=' + token + '&_conference=' + appConfig.conference);
            },
            saveBPData: function (data) {
                console.log(data);
                return $http({
                    method: 'POST',
                    url: appConfig.consoleUrl + '/participant/partnerUpdate?_conference=' + appConfig.conference,
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            },
            doBPLogin: function (ticket, email) {
                return $http({
                    method: 'POST',
                    url: appConfig.consoleUrl + '/awards/clientAuth?_conference=' + appConfig.conference,
                    data: {ticket: ticket, email: email},
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            },
            getMember: function (id, type, token) {
                return $http({
                    method: 'POST',
                    url: appConfig.consoleUrl + '/participant/getMember?_conference=' + appConfig.conference,
                    data: {id: id, type: type, token: token}
                });
            },
            sendMessage: function (data, token) {
                return $http({
                    method: 'POST',
                    url: appConfig.consoleUrl + '/participant/sendMessage?_conference=' + appConfig.conference,
                    data: {data: data, token: token}
                });
            }
        };
    }])
/**
 * Check URL for a protocol prefix.
 */
    .directive('validLink', function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                var LINK_REGEXP = /^(http|ftp)/;
                //console.log(ctrl);
                ctrl.$validators.validlink = function (modelValue, viewValue) {
                    if (ctrl.$isEmpty(modelValue)) {
                        return true;
                    }

                    if (!LINK_REGEXP.test(viewValue)) {
                        ctrl.$setViewValue('http://' + viewValue);
                        ctrl.$render();
                        //console.log('Wrong link!');
                        return false;
                    }
                    //console.log(modelValue);
                    //console.log(viewValue);
                    return true;
                }
            }

        }
    })

    .directive('checkSum', function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$validators.validSum = function (modelValue, viewValue) {
                    var correctSum = parseInt(attrs.n1) + parseInt(attrs.n2);
                    if (modelValue != correctSum) {
                        //console.log('Wrong sum!');
                        return false;
                    }
                    return true;
                }
            }
        }
    })

    .directive('validFile', function () {
        return {
            require: 'ngModel',
            link: function (scope, el, attrs, ngModel) {
                ngModel.$render = function () {
                    ngModel.$setViewValue(el.val());
                };

                el.bind('change', function () {
                    scope.$apply(function () {
                        ngModel.$render();
                    });
                });
            }
        };
    });
