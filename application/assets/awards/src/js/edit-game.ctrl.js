angular.module('DGA')
    .controller('EditGameCtrl', [
        '$scope', '$location', '$localStorage', '$upload', 'appConfig', 'DGAServer',
        function ($scope, $location, $localStorage, $upload, appConfig, DGAServer) {

            var urlSearch = $location.search();

            $localStorage.regForm = {
                step1: {passed: true},
                step2: {passed: false},
                step3: {passed: false},
                step4: {passed: true},
                step5: {passed: true},
                step6: {passed: true}
            };

            $scope.checkMD5Hash = function (hash) {
                var md5Pattern = /^[a-f0-9]{32}$/gm;
                return md5Pattern.test(hash);
            };

            if (!urlSearch.token) {
                $scope.$state.go('error-msg', {msg: "Your token is missing!"});
                return;
            } else if (!$scope.checkMD5Hash(urlSearch.token)) {
                $scope.$state.go('error-msg', {msg: "Your token has wrong format!"});
                return;
            }

            DGAServer.checkToken(urlSearch.token).success(function (data) {
                if (data.error) {
                    switch (data.error) {
                        case 404:
                            $scope.$state.go('error-msg', {msg: "Your token is wrong!"});
                            break;
                        case 403:
                            $scope.$state.go('error-msg', {msg: "Your token has expired!"});
                            break;
                    }
                } else {
                    console.log(data);
                    $localStorage.edit = {};
                    $localStorage.edit.data = data;
                    $localStorage.regForm.token = data.token;
                    $scope.$state.go('step2');
                }

            });
        }]);