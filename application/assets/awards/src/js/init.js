/**
 * DevGamm Awards module.
 *
 * victor.burre@gmail.com, 2015
 */
angular.module('DGA', [
    'ui.router',
    'ngResource',
    'ngStorage',
    'ngAnimate',
    'angularFileUpload',
    'DGCommon'
])
    .config([
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider
                .when('', '/')
                .otherwise('/step-1');

            $stateProvider
                .state('step1', {
                    url: '/step-1',
                    templateUrl: '/awards/partials/step-1.php',
                    controller: 'Step1Ctrl'
                })
                .state('step2', {
                    url: '/step-2',
                    templateUrl: '/awards/partials/step-2.php',
                    controller: 'Step2Ctrl'
                })
                .state('step3', {
                    url: '/step-3',
                    templateUrl: '/awards/partials/step-3.php',
                    controller: 'Step3Ctrl'
                })
                .state('step4', {
                    url: '/step-4',
                    templateUrl: '/awards/partials/step-4.php',
                    controller: 'Step4Ctrl'
                })
                .state('step5', {
                    url: '/step-5',
                    templateUrl: '/awards/partials/step-5.php',
                    controller: 'Step5Ctrl'
                })
                .state('step6', {
                    url: '/step-6',
                    templateUrl: '/awards/partials/step-6.php',
                    controller: 'Step6Ctrl'
                })
                .state('success', {
                    url: '/success',
                    templateUrl: '/awards/partials/success.php',
                    controller: 'SuccessCtrl'
                })
                .state('edit-game', {
                    url: '/edit-game',
                    templateUrl: '/awards/partials/step-2.php',
                    controller: 'EditGameCtrl'
                })
                .state('error-msg', {
                    url: '/ooops',
                    templateUrl: '/awards/partials/ooops.php',
                    params: {msg: 'Some shit happens!'}
                });
        }
    ])
    .run(['$rootScope', '$state', '$stateParams', '$localStorage', 'appConfig',
        function ($rootScope, $state, $stateParams, $localStorage, appConfig) {

            $localStorage.edit = {};
            if ($localStorage.regForm == undefined || ($localStorage.regForm !== undefined && $localStorage.regForm.date == undefined) ) {
                var d = new Date();
                $localStorage.regForm = {
                    step1: {passed: false},
                    step2: {passed: false},
                    step3: {passed: false},
                    step4: {passed: false},
                    step5: {passed: false},
                    step6: {passed: false},
                    date: d.toDateString()
                };
            }

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }])
;
