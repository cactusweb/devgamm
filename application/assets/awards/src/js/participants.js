/**
 * Created by burre on 28.08.15.
 */

angular.module('DGBP', [
    'ngResource',
    'ngStorage',
    'ngAnimate',
    'angularFileUpload',
    'DGCommon'
])
    .run(['$rootScope', '$sessionStorage', 'appConfig', 'DGAServer',
        function ($rootScope, $sessionStorage, appConfig, DGAServer) {
            var config = appConfig.conference;

            if (!$sessionStorage.bp) {
                $sessionStorage.bp = {};
            }
        }])

    .controller('ParticipantsCtrl', [
        '$scope', '$q', '$location', '$localStorage', '$sessionStorage', '$upload', 'appConfig', 'DGAServer', '$sce',
        function ($scope, $q, $location, $localStorage, $sessionStorage, $upload, appConfig, DGAServer, $sce) {
            $scope.edit_own_data = true;

            $scope.identity = {
                ticket: '',
                email: ''
            };

            $scope.BPData = {
                photo: {
                    name: 'http://devgamm.com/moscow2015/images/user_avatar.gif'
                },
                photo_url: 'http://devgamm.com/moscow2015/images/user_avatar.gif',
                looking_for: '',
                offer: '',
                website: ''
            };

            $scope.formIdentity = {
                showErrorMessage: false,
            };

            $scope.formEdit = {
                filePreloader: false,
                showErrorMessage: false,
                open: false
            };

            $scope.modal_message = '';

            $scope.$watch('BPData.photo', function (newValue, oldValue) {
                if (angular.isArray(newValue) && newValue != oldValue) {
                    $scope.BPData.photo.name = newValue[0].name;
                    $scope.formEdit.upload = true;
                    $scope.formEdit.uploadFile(newValue);
                }
            });

            $scope.getBPData = function () {
                var deferred = $q.defer();

                if ($sessionStorage.bp.token == undefined) {
                    deferred.reject('Token missing!');
                } else {
                    DGAServer.getBPData($sessionStorage.bp.token)
                        .success(function (data, status, headers, config) {
                            $sessionStorage.bp.data = data;
                            deferred.resolve(data);
                        })
                        .error(function (data, status, headers, config) {
                            switch (status) {
                                case 401:
                                    $sessionStorage.bp.token = '';
                                    break;
                                case 404:
                                    console.log(data);
                                    break;
                            }

                            deferred.reject(data);
                        });
                }

                return deferred.promise;
            };

            /**
             *
             */
            $scope.doLogin = function () {
                DGAServer.doBPLogin($scope.identity.ticket, $scope.identity.email)
                    .success(function (data, status, headers, config) {
                        $sessionStorage.bp.token = data.access_token;
                        $('#identity').modal('hide');
                        $scope.getBPData().then(function (data) {
                            $scope.doNext();
                        })
                    })
                    .error(function (data, status, headers, config) {
                        if (status == 404) {
                            console.log(data);
                            $scope.formIdentity.message = data.message;
                            $scope.formIdentity.showErrorMessage = true;
                        }
                    })
            };

            /**
             *
             * @returns {*}
             */
            $scope.checkAllowedConversation = function () {
                var deferred = $q.defer();

                DGAServer.getSettings()
                    .success(function (settings) {
                        $sessionStorage.bp.conversation_on = parseInt(settings.conversation_on);
                        $sessionStorage.bp.conversation_off_msg = settings.conversation_off_msg;
                        $sessionStorage.bp.pitch_match_url = settings.pitch_match_url;
                        if ($sessionStorage.bp.conversation_on == 1) {
                            deferred.resolve(true);
                        } else {
                            deferred.reject($sessionStorage.bp.pitch_match_url);
                        }
                    })
                    .error(function (data) {
                        deferred.reject(false);
                    });

                return deferred.promise;
            };

            /**
             *
             * @returns {*}
             */
            $scope.checkLogin = function () {
                var deferred = $q.defer();

                if ($sessionStorage.bp.token == undefined) {
                    $('#identity').modal('show');
                    deferred.reject(false);
                } else {
                    deferred.resolve(true);
                }
                return deferred.promise;
            };

            /**
             * Action after login
             */
            $scope.doNext = function () {
                if ($sessionStorage.bp.next != undefined) {
                    switch ($sessionStorage.bp.next.action) {
                        case 'edit':
                            $scope.formEdit.edit();
                            break;
                        case 'makeAppointment':
                            $scope.makeAppointment($sessionStorage.bp.next.args[0], $sessionStorage.bp.next.args[1]);
                            break;
                        default:
                            break;
                    }
                }
            };

            /**
             *
             */
            $scope.formEdit.edit = function () {
                var args = [];

                if ($scope.formEdit.open) {
                    $scope.formEdit.hide();
                    return;
                }

                $scope.checkLogin()
                    .then(function () {
                        $scope.formEdit.populate($sessionStorage.bp.data).show();
                    }, function () {
                        $sessionStorage.bp.next = {
                            action: 'edit',
                            args: args
                        };
                    })
            };


            /**
             *
             * @param data
             */
            $scope.formEdit.populate = function (data) {
                $scope.BPData.photo = {name: data.photo};
                $scope.BPData.photo_url = data.photo;
                $scope.BPData.looking_for = data.lookingfor_en;
                $scope.BPData.offer = data.propose_en;
                $scope.BPData.website = data.company_url;
                return $scope.formEdit;
            };

            $scope.formEdit.show = function () {
                $scope.formEdit.open = true;
                return $scope.formEdit;
            };

            $scope.formEdit.hide = function () {
                $scope.formEdit.open = false;
                return $scope.formEdit;
            };

            /**
             *
             * @param data
             * @returns {{token: (*|string), photo: (string|*|string), lookingfor_en: (string|*), propose_en: (string|*), company_url: (string|*)}}
             */
            $scope.formEdit.prepareToSave = function (data) {
                return {
                    token: $sessionStorage.bp.token,
                    photo: data.photo.name,
                    lookingfor_en: data.looking_for,
                    propose_en: data.offer,
                    company_url: data.website
                };
            };

            /**
             *
             */
            $scope.formEdit.submit = function () {
                DGAServer.saveBPData($scope.formEdit.prepareToSave($scope.BPData))
                    .success(function (data, status, headers, config) {
                        $scope.showInfoModal('Your changes was saved successfully!');
                        $sessionStorage.bp.data = data;
                        $scope.formEdit.hide();

                        $("#notify").find("button").on("click", function () {
                            document.location.reload();
                        });

                        setTimeout(function () {
                            document.location.reload();
                        }, 3000);
                    })
                    .error(function (data, status, headers, config) {
                        switch (status) {
                            case 401:
                                delete $sessionStorage.bp.token;
                                break;
                            case 404:
                                console.log(data);
                                break;
                        }
                    });
            };

            /**
             *
             * @param file
             */
            $scope.formEdit.uploadFile = function (file) {
                $scope.upload = $upload.upload({
                    url: appConfig.consoleUrl + '/participant/partnerPhoto/?_conference=' + appConfig.conference,
                    method: 'POST',
                    headers: {'Content-Type': file.type != '' ? file.type : 'application/octet-stream'}, // only for html5
                    withCredentials: false,
                    file: file,
                    data: {'token': $sessionStorage.bp.token}
                }).progress(function (evt) {
                    $scope.formEdit.photo_error = false;
                    $scope.formEdit.filePreloader = true;
                }).success(function (data, status, headers, config) {
                    $scope.formEdit.filePreloader = false;
                    $scope.BPData.photo_url = data.photo_url;
                }).error(function (data) {
                    $scope.formEdit.filePreloader = false;
                    $scope.formEdit.photo_error = true;
                    console.log('File upload ERROR!', data);
                });
            };


            $scope.formAppointment = {
                member: {
                    id: '',
                    type: '',
                    name: '',
                    email: ''
                },
                url: window.location.href
            };

            $scope.formAppointment.submit = function () {
                DGAServer.sendMessage($scope.formAppointment, $sessionStorage.bp.token)
                    .success(function (data, status, headers, config) {
                        $scope.showInfoModal('Your message has been sent successfully!');
                        $('#bp-appointment').modal('hide');
                        $scope.formAppointment.body = '';
                        $scope.formAppointment.answer = '';
                        $scope.formBPApp.captcha.$setUntouched();
                    })
                    .error(function (data, status, headers, config) {
                        switch (status) {
                            case 401:
                                delete $sessionStorage.bp.token;
                                break;
                            case 404:
                                console.log(data);
                                break;
                        }
                    });
            };

            $scope.makeAppointment = function (id, type) {
                var args = [id, type];
                $scope.checkAllowedConversation().then(function (data) {
                    $scope.checkLogin()
                        .then(function () {
                            DGAServer.getMember(id, type, $sessionStorage.bp.token)
                                .success(function (data, status, headers, config) {
                                    $scope.formAppointment.member.id = id;
                                    $scope.formAppointment.member.type = type;
                                    $scope.formAppointment.member.name = data.name;
                                    $scope.formAppointment.member.email = data.email;
                                    $scope.formAppointment.name = $sessionStorage.bp.data.name;
                                    $scope.formAppointment.email = $sessionStorage.bp.data.email;
                                    $scope.formAppointment.n1 = Math.floor(Math.random() * (50 - 1)) + 1;
                                    $scope.formAppointment.n2 = Math.floor(Math.random() * (50 - 1)) + 1;
                                    $('#bp-appointment').modal('show');
                                })
                                .error(function (data, status, headers, config) {
                                    switch (status) {
                                        case 401:
                                            delete $sessionStorage.bp.token;
                                            break;
                                        case 404:
                                            console.log(data);
                                            break;
                                    }
                                });
                        }, function () {
                            $sessionStorage.bp.next = {
                                action: 'makeAppointment',
                                args: args
                            };
                        });
                }, function (data) {
                    window.location = data;
                    //$scope.showInfoModal(data);
                })
            };

            $scope.showInfoModal = function (msg) {
                $scope.modal_message = $sce.trustAsHtml(msg);
                $('#notify').modal('show');
            }

        }]);