/**
 * Created by burre on 05.05.15.
 */

angular.module('DGA')
    .controller('Step1Ctrl', [
        '$scope', '$localStorage', '$http', 'appConfig',
        function ($scope, $localStorage, $http, appConfig) {
            $scope.editMode = !!($localStorage.edit && $localStorage.edit.data && $localStorage.edit.data.token);
            if ($scope.editMode) {
                $scope.$state.go('step2');
                return;
            }

            $scope.form = {blocked: false};
            $scope.ticket = {};
            $scope.ticket.error = {};
            $scope.ticket.full = '';
            $scope.ticket.forceCheck = false;

            if ($localStorage.regForm.step1 && $localStorage.regForm.step1.ticket) {
                $scope.ticket = $localStorage.regForm.step1.ticket;
                $scope.ticket.forceCheck = true;
            }

            $scope.$watchGroup(['formS1.$invalid', 'ticket.forceCheck'], function (nV, oV) {
                if (($scope.formS1.$dirty && !nV[0]) || nV[1]) {
                    var ticket = $scope.ticket.t1 + $scope.ticket.t2 + $scope.ticket.t3 + $scope.ticket.t4;
                    $scope.form.blocked = true;
                    $scope.ticket.error = {};

                    $http.get(appConfig.consoleUrl + '/awards/checkticket/?ticket=' + ticket + '&_conference=' + appConfig.conference)
                        .success(function (data, status, headers, config) {
                            $scope.ticket.error = {};
                            $scope.form.blocked = false;
                            if (data.error) {
                                switch (data.error) {
                                    case 'wrong_data':
                                    case 'wrong_code':
                                        $scope.ticket.error.notExists = true;
                                        break;
                                    case 'taken':
                                        console.log('TEST! taken!');
                                        $scope.ticket.error.alreadyUsed = true;
                                        $scope.ticket.error.game_title = data.game_title;
                                        break;
                                    default :
                                        console.log('ERROR!');
                                        console.log(data.error);
                                        break;
                                }
                                $scope.ticket.forceCheck = false;
                                console.log($scope.ticket);
                            } else {
                                if (data.id) {
                                    $scope.ticket.info = data;
                                } else {
                                    delete $scope.ticket.info;
                                }
                            }
                        })
                        .error(function (data, status, headers, config) {
                            console.log(data);
                            $scope.form.blocked = false;
                        });
                }
            });

            $scope.$watch('ticket.t1', function (val) {
                if (val && val.length === 4) {
                    $('.ticket-number .t2').focus();
                }
            });
            $scope.$watch('ticket.t2', function (val) {
                if (val && val.length === 4) {
                    $('.ticket-number .t3').focus();
                }
            });
            $scope.$watch('ticket.t3', function (val) {
                if (val && val.length === 4) {
                    $('.ticket-number .t4').focus();
                }
            });

            $scope.form.ticketHasError = function () {
                return !$.isEmptyObject($scope.ticket.error);
            };

            // submit form and go ahead
            $scope.form.submit = function () {
                if ($.isEmptyObject($scope.ticket.error)) {
                    $localStorage.regForm.step1.ticket = $scope.ticket;
                    $localStorage.regForm.step1.ticket.full = $scope.ticket.t1 + $scope.ticket.t2 + $scope.ticket.t3 + $scope.ticket.t4;
                    $localStorage.regForm.step1.passed = true;
                    $scope.$state.go('step2');
                }
            };
        }
    ]);