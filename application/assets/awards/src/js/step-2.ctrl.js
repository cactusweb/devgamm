/**
 * Created by burre on 05.05.15.
 */

angular.module('DGA')
    .controller('Step2Ctrl', [
        '$scope', '$localStorage', '$upload', 'appConfig', 'DGAServer',
        function ($scope, $localStorage, $upload, appConfig, DGAServer) {
            $scope.editMode = !!($localStorage.edit && $localStorage.edit.data && $localStorage.edit.data.token);

            if (!$localStorage.regForm.step1.passed) $scope.$state.go('step1');

            $scope.form = {};
            $scope.game = {
                screenshots: [],
                platform: {},
                tech: {},
                icon: {}
            };
            $scope.game.error = {};
            $scope.game.platform = {};

            if ($localStorage.regForm.step2 && $localStorage.regForm.step2.game) {
                $scope.game = $localStorage.regForm.step2.game;
            }

            if ($.isEmptyObject($scope.game.platform)) {

                DGAServer.getCategories().success(function (categories) {
                    //console.log(categories);
                    $scope.game.platform = categories;
                    //console.log($scope.game.platform);

                    if ($scope.editMode) {
                        for (var cat in $localStorage.edit.data.platforms) {
                            $scope.game.platform[cat].checked = true;
                            $scope.game.platform[cat].link = $localStorage.edit.data.platforms[cat].link;
                            for (var subcat in $localStorage.edit.data.platforms[cat].childs) {
                                //console.log(subcat);
                                $scope.game.platform[cat].childs[subcat].checked = true;
                                $scope.game.platform[cat].childs[subcat].link = $localStorage.edit.data.platforms[cat].childs[subcat].link;
                                $scope.game.platform[cat].childs[subcat].public_link = $localStorage.edit.data.platforms[cat].childs[subcat].public_link;
                            }
                        }

                        $scope.game.description = $localStorage.edit.data.description;
                        $scope.game.devStage = $localStorage.edit.data.stage;
                        $scope.game.freeToPlay = $localStorage.edit.data.f2p;
                        $scope.game.linkVideo = $localStorage.edit.data.video;
                        $scope.game.premium = $localStorage.edit.data.premium;
                        $scope.game.title = $localStorage.edit.data.title;
                    }
                });
            }

            if ($.isEmptyObject($scope.game.tech)) {
                DGAServer.getTechnologies().success(function (technologies) {
                    $scope.game.tech = technologies;
                    if ($scope.editMode) {
                        for (var i = 0; i < $localStorage.edit.data.technology.length; i++) {
                            var selected = $localStorage.edit.data.technology[i];
                            for (var j = 0; j < $scope.game.tech.length; j++) {
                                if ($scope.game.tech[j].id === selected.id) {
                                    $scope.game.tech[j].checked = true;
                                    $scope.game.tech[j].comment = selected.comment;
                                }
                            }
                        }
                    }
                });
            }

            $scope.form.categoryChanged = function (category) {
                if (category.checked) {
                    $scope.game.error.platform = false;
                    $scope.game.platform[category.parent_id].checked = true; // check parent
                    if (category.parent_id) {
                        category.link = '';
                        category.public_link = 1;
                    }
                }
            };


            $scope.$watch('form.icon', function (newValue, oldValue) {
                if (angular.isArray(newValue) && newValue != oldValue) {
                    $scope.game.icon = {
                        'name': newValue[0].name
                    };
                    $scope.form.upload4 = true;
                    $scope.form.uploadFile(newValue, 'icon');
                }
            });

            $scope.form.deleteIcon = function () {
                2
                $scope.game.icon = {};
                console.log($scope.game);
            };

            $scope.$watch('form.screenshot', function (newValue, oldValue) {
                if (angular.isArray(newValue) && newValue != oldValue) {
                    $scope.game.error.screenshots = false;
                    $scope.form.upload = true;
                    $scope.form.uploadFile(newValue);
                }
            });

            $scope.form.deleteScreenshot = function (index) {
                $scope.game.screenshots.splice(index, 1);
                console.log($scope.game);
            };

            /**
             * File uploading.
             * @param file
             * @param type
             */
            $scope.form.uploadFile = function (file, type) {
                if (type === undefined) type = 'pic';
                var ticket = $scope.editMode ? $localStorage.edit.data.ticket : $localStorage.regForm.step1.ticket.full;

                $scope.upload = $upload.upload({
                    url: appConfig.consoleUrl + '/awards/file/?_conference=' + appConfig.conference,
                    method: 'POST',
                    headers: {'Content-Type': file.type != '' ? file.type : 'application/octet-stream'}, // only for html5
                    withCredentials: false,
                    file: file,
                    data: {ticket: ticket, type: type}
                }).progress(function (evt) {
                    $scope.form.filePreloader = true;
                }).success(function (data, status, headers, config) {
                        console.log(data);
                        $scope.form.filePreloader = false;
                        $scope.game.error.ico = false;
                        if (type == 'icon') {
                            if (!data.hasOwnProperty('ticket')) {
                                $scope.game.icon = {};
                                $scope.game.error.ico = true;
                            }
                        } else {
                            if (!data.hasOwnProperty('error')) {
                                $scope.game.screenshots.push({'name': data.name});
                            } else {
                                $scope.game.error.screenshots = true;
                                console.log(data);
                            }
                        }
                    }
                ).error(function (data) {
                        $scope.form.filePreloader = false;
                        console.log('File upload ERROR!', data);
                    });
            };

            // submit form and go ahead
            $scope.form.submit = function () {
                var screenshotIsOk = $scope.editMode ? true : $scope.game.screenshots.length > 0;
                var formValid = $scope.form.isOkPlatform() &&
                    $scope.form.isOkTech() &&
                    $scope.form.isOkStage() &&
                    $scope.form.isOkIcon() &&
                    $scope.form.isOkLink() &&
                    screenshotIsOk;

                if (formValid) {
                    $localStorage.regForm.step2.game = $scope.game;
                    $localStorage.regForm.step2.passed = true;
                    console.log($localStorage.regForm);
                    $scope.$state.go('step3');
                } else {
                    return false;
                }
            };

            /**
             * Is any platform selected?
             * @returns {boolean}
             */
            $scope.form.isOkPlatform = function () {
                if ($scope.game.platform) {
                    $scope.game.error.platform = true;
                    loop1:
                        for (var cat in $scope.game.platform) {
                            for (var subcat in $scope.game.platform[cat].childs) {
                                if ($scope.game.platform[cat].childs[subcat].hasOwnProperty('checked') &&
                                    $scope.game.platform[cat].childs[subcat].checked) {
                                    $scope.game.error.platform = false;
                                    break loop1;
                                }
                            }
                        }
                }
                return !$scope.game.error.platform;
            };

            /**
             * Is any technology selected?
             * @returns {boolean}
             */
            $scope.form.isOkTech = function () {
                if ($scope.game.tech) {
                    $scope.game.error.tech = true;
                    for (var tech in $scope.game.tech) {
                        if ($scope.game.tech[tech].hasOwnProperty('checked') && $scope.game.tech[tech].checked) {
                            $scope.game.error.tech = false;
                            break;
                        }
                    }
                }
                return !$scope.game.error.tech;
            };

            $scope.form.isOkStage = function () {
                $scope.game.error.stage = !!!$scope.game.devStage;
                return !$scope.game.error.stage;
            };

            $scope.form.isOkIcon = function () {
                if ($scope.editMode) return true
                else return !$.isEmptyObject($scope.game.icon);
            };

            /**
             * Is any valid link for selected platform?
             * @returns {boolean}
             */
            $scope.form.isOkLink = function () {
                var urlPattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/,
                    retval = false;

                loop1:
                    for (var cat in $scope.game.platform) {
                        for (var subcat in $scope.game.platform[cat].childs) {
                            if ($scope.game.platform[cat].childs[subcat].hasOwnProperty('link') &&
                                $scope.game.platform[cat].childs[subcat].link !== '' &&
                                urlPattern.test($scope.game.platform[cat].childs[subcat].link)) {
                                retval = true;
                                break loop1;
                            }
                        }
                    }
                return retval;
            };

            // check if any property is equal to true
            $scope.form._hasTrue = function (obj) {
                var retval = false;
                if (obj) {
                    for (var t in obj) if (obj[t] === true) retval = true;
                }
                return retval;
            };

            /**
             * Uncheck elements of checkbox-group and delete related link for versions.
             */
            $scope.form.uncheckAll = function (cat) {
                if (!cat.checked) {
                    for (var i in cat.childs) {
                        cat.childs[i].checked = false;
                        cat.childs[i].link = '';
                    }
                }
            };
        }]);
