/**
 * Created by burre on 05.05.15.
 */

angular.module('DGA')
    .controller('Step3Ctrl', ['$scope', '$localStorage', '$http', 'appConfig',
        function ($scope, $localStorage, $http, appConfig) {
        $scope.editMode = !!($localStorage.edit && $localStorage.edit.data && $localStorage.edit.data.token);

        if (!$localStorage.regForm.step2.passed) $scope.$state.go('step2');

        $scope.form = {};
        $scope.dev = {type: null};
        $scope.dev.error = {};

        if ($localStorage.regForm.step1 &&
            $localStorage.regForm.step1.ticket &&
            $localStorage.regForm.step1.ticket.info) {
            $scope.dev.author = $localStorage.regForm.step1.ticket.info.firstname + ' ' + $localStorage.regForm.step1.ticket.info.lastname;
            $scope.dev.name = $scope.dev.author;
            $scope.dev.email = $localStorage.regForm.step1.ticket.info.email;
            $scope.dev.phone = $localStorage.regForm.step1.ticket.info.phone;
            $scope.dev.country = $localStorage.regForm.step1.ticket.info.country;
            $scope.dev.city = $localStorage.regForm.step1.ticket.info.city;
        }

        if ($scope.editMode) {
            $scope.dev.author = $localStorage.edit.data.author;
            $scope.dev.type = $localStorage.edit.data.author_type;
            $scope.dev.name = $localStorage.edit.data.contact_person;
            $scope.dev.email = $localStorage.edit.data.email;
            $scope.dev.phone = $localStorage.edit.data.phone;
            $scope.dev.skype = $localStorage.edit.data.skype;
            $scope.dev.country = $localStorage.edit.data.location.country;
            $scope.dev.city = $localStorage.edit.data.location.city;
            $scope.dev.site = $localStorage.edit.data.link.company;
            $scope.dev.team = $localStorage.edit.data.team_size;
        }

        if ($localStorage.regForm.step3 && $localStorage.regForm.step3.dev) {
            $scope.dev = $localStorage.regForm.step3.dev;
        }

        // submit form and go ahead
        $scope.form.submit = function () {
            if ($scope.form.isOkDevType() && $scope.form.isOkTeamSize()) {
                $localStorage.regForm.step3.dev = $scope.dev;
                $localStorage.regForm.step3.passed = true;

                if ($scope.editMode) {
                    $http({
                        method: 'POST',
                        url: appConfig.consoleUrl + '/awards/updategame/?_conference=' + appConfig.conference,
                        data: $localStorage.regForm,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data, status, headers, config) {
                        console.log(data);
                        if (data == 'ok') {
                            $scope.$state.go('success');
                        }
                    }).error(function (data, status, headers, config) {
                    });
                } else {
                    $scope.$state.go('step4');
                }
            } else {
                return false;
            }
        };

        $scope.form.isOkDevType = function () {
            $scope.dev.error.type = $scope.dev.type == undefined;
            return !$scope.dev.error.type;
        };

        $scope.form.isOkTeamSize = function () {
            $scope.dev.error.team = $scope.dev.team == "";
            return !$scope.dev.error.team;
        };
    }]);
