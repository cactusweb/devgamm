/**
 * Created by burre on 05.05.15.
 */

angular.module('DGA')
    .controller('Step4Ctrl', ['$scope', '$localStorage', 'DGAServer', '$sce',
        function ($scope, $localStorage, DGAServer, $sce) {
            $scope.editMode = !!($localStorage.edit && $localStorage.edit.data && $localStorage.edit.data.token);
            if ($scope.editMode) {
                $scope.$state.go('step2');
                return;
            }


            if (!$localStorage.regForm.step3.passed) $scope.$state.go('step3');

            $scope.form = {};
            $scope.awards = {};

            if ($localStorage.regForm.step4 && $localStorage.regForm.step4.awards) {
                $scope.awards = $localStorage.regForm.step4.awards;
            }

            if ($.isEmptyObject($scope.awards.nominations)) {
                DGAServer.getNominations().success(function (nominations) {
                    $scope.awards.nominations = nominations;
                });
            }

            DGAServer.getSettings().success(function (settings) {
                var deadline = new Date(settings.deadline_date_4);
                var today = new Date();
                var diff = deadline - today;

                $scope.awards.allowed = diff >= 0;
                if ($scope.awards.allowed) {
                    $scope.awards.deadline_message = settings.deadline_message_4_pre;
                } else {
                    $scope.awards.deadline_message = settings.deadline_message_4_post;
                }
                $scope.awards.link = settings.awards_link;
            });

            $scope.awards.getDeadlineMessage = function(){
                return $sce.trustAsHtml($scope.awards.deadline_message);
            };

            // submit form and go ahead
            $scope.form.submit = function () {
                if ($scope.awards.add) {
                    $localStorage.regForm.step4.awards = $scope.awards;
                }
                $localStorage.regForm.step4.passed = true;
                $scope.$state.go('step5');
            };
        }]);
