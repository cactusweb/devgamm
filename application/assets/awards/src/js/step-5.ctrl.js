/**
 * Created by burre on 05.05.15.
 */

angular.module('DGA')
    .controller('Step5Ctrl', ['$scope', '$localStorage', 'DGAServer', '$sce',
        function ($scope, $localStorage, DGAServer, $sce) {
            $scope.editMode = !!($localStorage.edit && $localStorage.edit.data && $localStorage.edit.data.token);
            if ($scope.editMode) {
                $scope.$state.go('step2');
                return;
            }

            if (!$localStorage.regForm.step4.passed) $scope.$state.go('step4');

            $scope.form = {};
            $scope.lynch = {};
            $scope.raiseWarning = false;

            if ($localStorage.regForm.step5 && $localStorage.regForm.step5.lynch) {
                $scope.lynch = $localStorage.regForm.step5.lynch;
            }

            // what is development stage of game
            $scope.checkStage = function() {
                console.log($localStorage.regForm.step2.game);
                var game = $localStorage.regForm.step2.game;
                if ((game.devStage != undefined && game.devStage == "4") ||
                    (game.devStage != undefined && game.devStage == "5")) {
                    $scope.lynch.add = false;
                    $scope.raiseWarning = true;
                }
            };

            DGAServer.getSettings().success(function (settings) {
                var deadline = new Date(settings.deadline_date_5);
                var today = new Date();
                var diff = deadline - today;

                $scope.lynch.allowed = diff >= 0;

                if ($scope.lynch.allowed) {
                    $scope.lynch.deadline_message = settings.deadline_message_5_pre;
                } else {
                    $scope.lynch.deadline_message = settings.deadline_message_5_post;
                }
                $scope.lynch.link = settings.lynch_link;
            });

            $scope.lynch.getDeadlineMessage = function(){
                return $sce.trustAsHtml($scope.lynch.deadline_message);
            };

            $scope.form.submit = function () {
                if ($scope.lynch.add) {
                    $localStorage.regForm.step5.lynch = $scope.lynch;
                }
                $localStorage.regForm.step5.passed = true;
                $scope.$state.go('step6');
            };
        }]);
