/**
 * Created by burre on 05.05.15.
 */

angular.module('DGA')
    .controller('Step6Ctrl', [
        '$scope', '$localStorage', '$http', '$upload', 'appConfig', 'DGAServer', '$sce',
        function ($scope, $localStorage, $http, $upload, appConfig, DGAServer, $sce) {
            $scope.editMode = !!($localStorage.edit && $localStorage.edit.data && $localStorage.edit.data.token);
            if ($scope.editMode) {
                $scope.$state.go('step2');
                return;
            }

            if (!$localStorage.regForm.step5.passed) $scope.$state.go('step5');

            $scope.form = {};
            $scope.play = {};
            $scope.play.logo = {};
            $scope.play.showcase = {};
            $scope.play.error = {};

            console.log($localStorage.regForm);

            if ($localStorage.regForm.step6 && $localStorage.regForm.step6.play) {
                $scope.play = $localStorage.regForm.step6.play;
            }

            if ($.isEmptyObject($scope.play.showcase)) {
                DGAServer.getShowcases().success(function (showcase) {
                    $scope.play.showcase = showcase;
                    //console.log($scope.play.showcase);
                });
            }

            DGAServer.getSettings().success(function (settings) {
                var deadline = new Date(settings.deadline_date_6);
                var today = new Date();
                var diff = deadline - today;

                $scope.play.allowed = diff >= 0;
                if ($scope.play.allowed) {
                    $scope.play.deadline_message = settings.deadline_message_6_pre;
                } else {
                    $scope.play.deadline_message = settings.deadline_message_6_post;
                }
                $scope.play.link = settings.gammplay_link;
            });

            $scope.play.getDeadlineMessage = function(){
                return $sce.trustAsHtml($scope.play.deadline_message);
            };

            $scope.$watch('form.logo', function (newValue, oldValue) {
                if (angular.isArray(newValue) && newValue != oldValue) {
                    $scope.form.uploadFile(newValue);
                }
            });

            $scope.$watch('form.logo', function (newValue, oldValue) {
                if (angular.isArray(newValue) && newValue != oldValue) {
                    $scope.play.logo = {
                        'name': newValue[0].name
                    };
                    $scope.form.upload = true;
                    $scope.form.uploadFile(newValue, $scope.form.upload);
                }
            });

            $scope.form.deleteLogo = function () {
                $scope.play.logo = {};
                //console.log($scope.play);
            };

            $scope.form.submit = function () {
                console.log('regForm: ', $localStorage.regForm);
                if ($scope.play.add) {
                    var formValid = $scope.form.isOkDevices() &&
                        $scope.form.isOkShowcase() &&
                        $scope.form.isOkLogo();
                    if (formValid) {
                        $localStorage.regForm.step6.play = $scope.play;
                    } else {
                        return false;
                    }
                }
                $localStorage.regForm.step6.passed = true;

                $http({
                    method: 'POST',
                    url: appConfig.consoleUrl + '/awards/addgame/?_conference=' + appConfig.conference,
                    data: $localStorage.regForm,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data, status, headers, config) {
                    console.log(data);
                    if (data == 'ok') {
                        $scope.$state.go('success');
                    }
                }).error(function (data, status, headers, config) {
                });
            };

            $scope.form.isOkDevices = function () {
                if (!$scope.play.device || !$scope.form._hasTrue($scope.play.device)) {
                    $scope.play.error.device = true;
                } else {
                    $scope.play.error.device = false;
                }
                return !$scope.play.error.device;
            };

            $scope.form.isOkShowcase = function () {
                if ($scope.play.showcase) {
                    $scope.play.error.showcase = true;
                    for (var showcase in $scope.play.showcase) {
                        if ($scope.play.showcase[showcase].hasOwnProperty('checked') && $scope.play.showcase[showcase].checked) {
                            $scope.play.error.showcase = false;
                            break;
                        }
                    }
                }
                return !$scope.play.error.showcase;
            };

            // check if any property is equal to true
            $scope.form._hasTrue = function (obj) {
                var retval = false;
                if (obj) {
                    for (var t in obj) if (obj[t] === true) retval = true;
                }
                return retval;
            };

            $scope.form.isOkLogo = function () {
                return !$.isEmptyObject($scope.play.logo);
            };

            $scope.form.uploadFile = function (file) {
                $scope.upload = $upload.upload({
                    url: appConfig.consoleUrl + '/awards/file/?_conference=' + appConfig.conference,
                    method: 'POST',
                    headers: {'Content-Type': file.type != '' ? file.type : 'application/octet-stream'}, // only for html5
                    withCredentials: false,
                    file: file,
                    data: {'ticket': $localStorage.regForm.step1.ticket.full}
                }).progress(function (evt) {
                    $scope.form.filePreloader = true;
                }).success(function (data, status, headers, config) {
                    $scope.form.filePreloader = false;
                    //console.log('file upload success!', data);
                }).error(function (data) {
                    $scope.form.filePreloader = false;
                    console.log('File upload ERROR!', data);
                });
            };
        }]);
