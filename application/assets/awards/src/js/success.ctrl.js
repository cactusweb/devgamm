/**
 * Created by burre on 05.05.15.
 */

angular.module('DGA')
    .controller('SuccessCtrl', [
        '$scope', '$localStorage',
        function ($scope, $localStorage) {
            if (!$localStorage.regForm.step1.passed ||
                !$localStorage.regForm.step2.passed ||
                !$localStorage.regForm.step3.passed ||
                !$localStorage.regForm.step4.passed ||
                !$localStorage.regForm.step5.passed ||
                !$localStorage.regForm.step6.passed ) {
                $scope.$state.go('step1');
            } else {
                $localStorage.regForm = {
                    step1: {passed: false},
                    step2: {passed: false},
                    step3: {passed: false},
                    step4: {passed: false},
                    step5: {passed: false},
                    step6: {passed: false}
                }
            }
        }]);