Activity = {
    _loading: $('<div id="stats-loading"><div></div></div>'),
    _loadingTimer: 1,
    _loadingFrame: 1,

    _animate_loading: function() {
        if (!Activity._loading.is(':visible')) {
            clearInterval(Activity._loadingTimer);
            return;
        }
        $('div', Activity._loading).css('top', (Activity._loadingFrame * -40) + 'px');
        Activity._loadingFrame = (Activity._loadingFrame + 1) % 12;
    },
    init: function() {
        $('body').append($(this._loading));
    },
    start: function() {
        clearInterval(this._loadingTimer);
        $(this._loading).show();
        this._loadingTimer = setInterval(this._animate_loading, 66);
    },
    stop: function() {
        $(this._loading).hide();
    }
};