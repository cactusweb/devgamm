$(document).ready(function() {
    Activity.init();
    $('input, textarea').on('keyup change', function() {
        $(this).parents('form').find('.save-row').removeAttr('style');
    });
    $('.save-row').on('click', function() {
        var data = $(this).parents('form').serialize(),
            that = this,
            id = $(this).attr('id').substr(2);
        if (id) {
            data += '&id=' + id;
        }
        $.post('/' + $('h3').attr('data-type') + '?action=' + $('h3').attr('data-action'), data, function(r) {
            if (r) {
                try {
                    data = JSON.parse(r);
                } catch (e) {
                    alert(r);
                }
                if (data.redirect) {
                    window.location(data.redirect);
                }
                return false;
            }
        });
    });
    $('.delete-category').on('click', function() {
        var self = this;
        var id = $(this).attr('data-id');
        if (!confirm('Do you want delete category?')) {
            return false;
        }

        $.post('/' + $('h3').attr('data-type') + '?action=remove', {id:id}, function(r) {
            if (r) {
                try {
                    data = JSON.parse(r);
                } catch (e) {
                    alert(r);
                }
                return false;
            }
            $(self).parent().parent().parent().remove();
        });
    });
});
