$(document).ready(function() {
    $('.formRow').hide();
    $('.form-type').change(function() {
        showForm(this);
    });
    $('.faq').fancybox({
        transitionIn: 'none',
        transitionOut: 'none',
        overlayShow: false,
        hideOnContentClick: true,
        autoDimensions: true
    });
    $('.ticket-code').on('keyup', function(e) {
        if (4 == $(this).val().length) {
            if ($(this).next().hasClass('ticket-code')) {
                $(this).next().focus();
            } else {
                var ticket = '';
                $('.ticket-code').each(function(index) {
                    ticket += $(this).val();
                });
                $.get('/game/isTicketExist?ticket=' + ticket , function(r) {
                    if (r) {
                        $('#error').text(r).show();
                    } else {
                        $('#error').hide();
                    }
                });
            }
        }
    });
    $('.ticket-code').keydown(function(e){
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
    });
});
$('.loaded').ready(function() {
    if (!$('.loaded').length) {
        return false;
    }
    showForm($('[name="SF_contest_selectForm"]:checked'));
});
function showForm(button) {
    $('.formRow').show();
    var b = (3 == $(button).val()) ? 2 : $(button).val();
    for (i in d = [0, 'flash', 'social', 'html5', 'mobile', 'steam']) {
        if (i != b) {
            $('.' + d[i] + '-type').hide();
        }
    }
    $('.' + d[b] + '-type').show();
    $('[name="category_id"]').val($(button).val());
}
function txtcounter(id, count) {
    var el = document.getElementById(id);
    var lost = count - el.value.length
    if (lost < 0) {
        el.value = el.value.substr(0, count);
        lost = 0;
    }
    document.getElementById(id+'_count').innerHTML = lost;
}