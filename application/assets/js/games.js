$(document).ready(function() {
    Activity.init();
    $('select').on('change', function() {
        $(this).parent().parent().parent().parent().find('.save-row').removeAttr('style');
    });
    $('input, textarea').on('keyup', function() {
        $(this).parents('form').find('.save-row').removeAttr('style');
    });
    $('.save-row').on('click', function() {
        var data = $(this).parents('form').serialize(),
            that = this;
        $.post('/games?action=save&type=games', data, function(r) {
            if (r) {
                try {
                    data = JSON.parse(r);
                } catch (e) {
                    alert(r);
                }
                if (data.redirect) {
                    window.location(data.redirect);
                }
                return false;
            }
        });
    });
    $('.approve').on('click', function() {
        var that = this;
        $.get('/approve?id=' + $(this).attr('data-id') + '&approve=' + ($(this).hasClass('btn-success') ? 0 : 1), function (data) {
            if ($(that).hasClass('btn-success')) {
                $(that).removeClass('btn-success');
                $(that).text('NOT Approved');
            } else {
                $(that).addClass('btn-success');
                $(that).text('Approved');
            }
        });
    });
    $('.remove').on('click', function() {
        if (!confirm('Are you sure want delete ' + $(this).parents('.participant').find('input[name="email"]').val() + '?')) {
            return false;
        }
        Activity.start();
        var that = this;
        $.post(
            '/games?action=remove',
            {id: $(this).parent().attr('data-id')},
            function (data) {
                $(that).parents('.game').parent().remove();
                Activity.stop();
            }
        );
    });
    $('.up-rating').on('click', function() {
        if (1 == $(this).parents('form').index()) {
            return false;
        }
        Activity.start();
        var that = this;
        $.post(
            '/games?action=up',
            {id: $(this).parent().attr('data-id')},
            function (data) {
                if (!data) {
                    return false;
                }
                var e = $(that).parents('form').clone();
                $($('form').eq($(that).parents('form').index() - 2)).before($(e));
                $(that).parents('form').remove();
                Activity.stop();
            }
        );
    });
    $('.down-rating').on('click', function() {
        if (($('.participant').length) == $(this).parents('form').index()) {
            return false;
        }
            console.log($('.participant').length);
        Activity.start();
        var that = this;
        $.post(
            '/games?action=down',
            {id: $(this).parent().attr('data-id')},
            function (data) {
                if (!data) {
                    return false;
                }
                var e = $(that).parents('form').clone();
                $($('form').eq($(that).parents('form').index())).after($(e));
                $(that).parents('form').remove();
                Activity.stop();
            }
        );
    });
});
