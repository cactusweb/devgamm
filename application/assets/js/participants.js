$(document).ready(function() {
    Activity.init();
    $('select').on('change', function() {
        $(this).parent().parent().parent().parent().find('.save-row').removeAttr('style');
    });
    $('input, textarea').on('keyup', function() {
        $(this).parent().parent().parent().parent().find('.save-row').removeAttr('style');
    });
    $('.checkbox').on('change', function() {
        $(this).parent().parent().parent().parent().find('.save-row').removeAttr('style');
        if ($(this).attr('checked')) {
            $(this).val('1');
            $(this).next().remove();
        } else {
            $(this).val('0');
            $(this).after("<input type=\"hidden\" name=\"" + $(this).attr("name") + "\" value=0>")

        }
    });
    $('input[type=checkbox]').each(function () {
       if (this.checked) {
           $(this).val(1);
       }
    });
    $('.save-row').on('click', function() {
        var data = $(this).parents('form').serialize(),
            that = this,
            id = $(this).attr('id');
        if (id) {
            data += '&id=' + id;
        }
        $.post('/' + $('h3').text() + '?action=save', data, function(r) {
            if (r) {
                alert(r);
                return false;
            }
            $(that).parents('form').find('.photo-preview').attr('src', $('.hero-unit').attr('data-image-url') + $(that).parents('form').find('.photo').val());
            $(that).parents('form').find('.company-logo-preview').attr('src', $('.hero-unit').attr('data-image-url') + $(that).parents('form').find('.logo').val());
        });
    });
    $('.remove').on('click', function() {
        if (!confirm('Are you sure want delete ' + $(this).parents('.participant').find('input[name="email"]').val() + '?')) {
            return false;
        }
        Activity.start();
        var that = this;
        $.post(
            '/' + $('h3').text() + '?action=remove',
            {id: $(this).parent().attr('data-id'), participant: true},
            function (data) {
                $(that).parents('.participant').parent().remove();
                Activity.stop();
            }
        );
    });
    $('.up-rating').on('click', function() {
        if (1 == $(this).parents('form').index()) {
            return false;
        }
        Activity.start();
        var that = this;
        $.post(
            '/' + $('h3').text() + '?action=up',
            {id: $(this).parent().attr('data-id')},
            function (data) {
                if (!data) {
                    return false;
                }
                var e = $(that).parents('form').clone();
                $($('form').eq($(that).parents('form').index() - 2)).before($(e));
                $(that).parents('form').remove();
                Activity.stop();
            }
        );
    });
    $('.down-rating').on('click', function() {
        if (($('.participant').length) == $(this).parents('form').index()) {
            return false;
        }
            console.log($('.participant').length);
        Activity.start();
        var that = this;
        $.post(
            '/' + $('h3').text() + '?action=down',
            {id: $(this).parent().attr('data-id')},
            function (data) {
                if (!data) {
                    return false;
                }
                var e = $(that).parents('form').clone();
                $($('form').eq($(that).parents('form').index())).after($(e));
                $(that).parents('form').remove();
                Activity.stop();
            }
        );
    });
});
