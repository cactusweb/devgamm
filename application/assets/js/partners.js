$(document).ready(function() {
    Activity.init();
    $('.btn-large').on('click', function() {
        var p = $(this).parents('tr').attr('data-id');
        if (confirm('Are you shure want to toggle ' + p + '?')) {
            var that = this;
            Activity.start();
            $.post(
                $('table').attr('data-url'),
                {p: p, s: $(this).hasClass('btn-success') ? 0 : 1},
                function(data) {
                    Activity.stop();
                    try {
                        data = JSON.parse(data);
                    } catch (e) {
                        return false;
                    }
                    if (data.error) {
                        alert(data.error);
                        return false;
                    }
                    if (1 == data.s) {
                        $(that).addClass('btn-success').text('ON');
                    } else {
                        $(that).removeClass('btn-success').text('OFF');
                    }
                }
            );
        }
    });
    $('.set-delivery-rate').on('keyup', function() {
        $(this).parents('tr').find('.update-partner').show();
        return false;
    });
    $('.set-countries').on('keyup', function() {
        $(this).parents('tr').find('.update-partner').show();
        return false;
    });
    $('.update-partner a').on('click', function() {
        Activity.start();
        var that = this;
        $.post(
            $('table').attr('data-update'),
            {
                p: $(that).parents('tr').attr('data-id'),
                r: $(that).parents('tr').find('.set-delivery-rate').val(),
                c: $(that).parents('tr').find('.set-countries').val(),
            },
            function (data) {
                Activity.stop();
                try {
                    data = JSON.parse(data);
                } catch (e) {
                    return false;
                }
                if (data.error) {
                    alert(data.error);
                    return false;
                }
                $(that).parent().hide();
            }
        );
    });
    $('[data-type=edit]').on('click', function() {
        Activity.start();
        $.post($('#add-partner').attr('data-edit'), {p: $(this).text()}, function (data) {
            Activity.stop();
            try {
                data = JSON.parse(data);
            } catch (e) {
                return false;
            }
            if (!data) {
                return false;
            }
            if (data.error) {
                alert(data.error);
                return false;
            }
            if (data.redirect) {
                window.location = data.redirect;
                return false;
            }
            for (i in data) {
                switch (i) {
                    case 'a':
                        $('#app-title').val(data.a).attr('readOnly', true);
                        break;
                    case 'p':
                        $('#partner-marker').val(data.p).attr('readOnly', true);
                        break;
                    case 'u':
                        $('#partner-url').val(data.u);
                        break;
                    case 'f':
                        var inc = 0;
                        for (j in data.f) {
                            $('#add-partner .creator').trigger('click');
                            $('#add-partner .keys').eq(inc).val(j);
                            $('#add-partner .values').eq(inc).val(data['f'][j]);
                            inc++;
                        }
                        break;
                }
            }
        });
        $('#add-partner-label').text($(this).text());
        $('#add-partner').modal('show');
    });
    $('#save-partner').click(function() {
        Activity.start();
        $.post($('form').attr('action'), $('form').serialize(), function (data) {
            Activity.stop();
            try {
                data = JSON.parse(data);
            } catch (e) {
                return false;
            }
            if ($.isEmptyObject(data)) {
                return false;
            }
            if ('New partner' != $('#add-partner-label').text()) {
                $('#marker' + $('#add-partner-label').text()).remove();
            }
            $('#add-partner').modal('hide');
            $('table tbody').append('<tr id="marker' + data.p + '"><td>' + data.a + '</td><td><a href="" onclick="return false;" data-type="edit">' + data.p + '</td><td>' + (data.e ? '<a href="" onclick="return false;" class="btn btn-large btn-success" data-id="' + data.p + '">ON</a>' : '<a href="" onclick="return false;" class="btn btn-large" data-id="' + data.p + '">OFF</a>') + '</td></tr>');
        });
    });
    $('#add-partner').on('hide', function() {
        $(this).find('input').val('').removeAttr('readOnly');
        $('#add-partner-label').text('New partner');
        $(this).find('.clone').remove();
    });
});
