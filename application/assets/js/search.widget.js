$(document).ready(function() {
    Activity.init();
    $('#get-data').click(function() {
        Activity.start();
        $.ajax({
            url: $('form').attr('action'),
            type: 'POST',
            data: $('form').serialize(),
            success: function(html) {
                Activity.stop();
                try {
                    data = $.parseJSON(html);
                    if (!$.isEmptyObject(data) && data.redirect) {
                        window.location = data.redirect;
                        return false;
                    }
                } catch(e) {
                }

                var callback = $('#search-tabs').attr('data-callback');
                if (callback.length) {
                    window[callback](html);
                } else {
                    $('#answer').html(html);
                }
            },
            error: function(a,e,t) {
                Activity.stop();
                $('#answer').html('<div class="error">Some error occuired</div>');
            },
            complete: function() {
                $('#search-tabs a:last').tab('show');
            }
        });
        return false;
    });
    $('.date-picker').datepicker({ dateFormat: 'yy-mm-dd'});
    $(".date-picker[name='date_time[from]']").datepicker('setDate', new Date());
    $(".date-picker[name='date_time[to]']").datepicker('setDate', new Date());
    $('#search-tabs').tab
    if (1 == $('#search-tabs').attr('data'))
    {
        $('#get-data').trigger('click');
        $('#search-tabs').attr('data', 0);
    }
});
