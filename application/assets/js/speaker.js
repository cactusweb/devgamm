$(document).ready(function(){
    $('.color-value').spectrum({
        preferredFormat: "hex",
        showInput: true,
        change: function(color) {
            $('#color-' + $(this).attr('data-id')).val($(this).val(color.toHexString()));
        }
    });
});