<?php

class ActiveRecord extends CActiveRecord
{
    protected $_alias = '';

    private $_cacheKeyPrefix = 'cache';

    public function __call($name, $arguments)
    {
        $name = str_replace('ViaCache', '', $name, $isCache);
        if (isset($this->_getHandlers()[$name]))
        {
            $arguments = $arguments[0];
            return $isCache ? $this->_processViaCache($name, $arguments) : $this->_process($name, $arguments);
        }

        return parent::__call($name, $arguments);
    }

    /**
     * multiplyInsert
     * Provides multiply insert into DB: INSERT INTO ... VALUES (...), (...), ...
     * $data can be one of:
     * array(
     *     array(
     *         'f1' => 'value',
     *         'f2' => 'value',
     *     ),
     *     array(
     *         'f1' => 'value',
     *         'f2' => 'value',
     *     ),
     *     array(
     *         'f1' => 'value',
     *         'f2' => 'value',
     *     ),
     * );
     * For simple multi insert: INSERT INTO ... (f1, f2) VALUES (value, value), (value, value), ...
     * array(
     *     '_multiply' => array(
     *         array(
     *             'f1' => 'value',
     *             'f2' => 'value',
     *         ),
     *         array(
     *             'f1' => 'value',
     *             'f2' => 'value',
     *         ),
     *     ),
     * );
     * For cases when rows are stackable:
     *  +-----+-----+-----+
     *  | id  | key | val |
     *  +-----+-----+-----+
     *  | 1   | k   | v   |
     *  +-----+-----+-----+
     *  | 1   | k1  | v1  |
     *  +-----+-----+-----+
     *  | 1   | k2  | v2  |
     *  +-----+-----+-----+
     *
     *  $onDuplicate consists fields, that should be updated on duplicating keys
     *  Can be set as string (field) or array of fields
     *
     * @param array $data
     * @param bool $isIgnore
     * @access public
     * @return bool
     */
    public function multiplyInsert($data, $isIgnore = false, $onDuplicate = array())
    {
        if (empty($data) || !is_array($data))
        {
            throw new CException('Incorrect input dataset');
        }
        $fields = $ps = $values = array();
        foreach ($data as $key => $row)
        {
            if (empty($fields))
            {
                $fields = implode('`,`', (isset($row['_multiply'][0]) ? array_keys($row['_multiply'][0]) : array_keys($row)));
            }
            $ps = array();
            if (isset($row['_multiply']) && is_array($row['_multiply']))
            {
                foreach ($row['_multiply'] as $field => $value)
                {
                    foreach ($value as $mField => $mValue)
                    {
                        $ps['_multiply'][$field][':' . $mField . '_' . $field . '_' . $key] = $mValue;
                    }
                }
            }
            else
            {
                foreach ($row as $field => $value)
                {
                    $ps[':' . $field . '_' . $key] = $value;
                }
            }
            $values[$key] = $ps;
        }
        $sql = 'INSERT ' . ($isIgnore ? 'IGNORE' : '') . ' INTO ' . $this->tableName() . ' (`' . $fields . '`) VALUES ';
        unset($fields);
        $ps = array();
        if (isset($row['_multiply']) && is_array($row['_multiply']))
        {
            foreach ($values as $key => $row)
            {
                foreach ($row['_multiply'] as $data)
                {
                    $sql .= '(' . implode(',', array_keys($data)) . '),';
                    $ps = array_merge($ps, $data);
                }
            }
        }
        else
        {
            foreach ($values as $key => $row)
            {
                $sql .= '(' . implode(',', array_keys($row)) . '),';
                $ps = array_merge($ps, $row);
            }
        }
        unset($values);
        $sql = substr($sql, 0, -1);
        if (!empty($onDuplicate))
        {
            $sql .= ' ON DUPLICATE KEY UPDATE ';
            if (!is_array($onDuplicate))
            {
                $sql .= '`' . $onDuplicate . '`=VALUES(`' . $onDuplicate . '`)';
            }
            else
            {
                foreach ($onDuplicate as $f)
                {
                    $sql .= '`' . $f . '`=VALUES(`' . $f . '`),';
                }
                $sql = substr($sql, 0, -1);
            }
        }
        return $this->getDbConnection()->createCommand($sql)->bindValues($ps)->execute();
    }

    public function getAlias()
    {
        if (empty($this->_alias))
        {
            return $this->tableName();
        }
        return $this->_alias;
    }

    public function flushCacheKey($key)
    {
        Yii::app()->cache->delete($this->_cacheKeyPrefix . $key);
    }

    public function findByPk($pk, $conds = array(), $params = array())
    {
        $pk = (isset($pk['pk'])) ? $pk['pk'] : $pk;
        return parent::findByPk($pk, $conds, $params);
    }

    public function executeQuery($sql, $params, $type = 'queryRow')
    {
        if (empty($sql) || !is_array($sql))
        {
            throw new CException(__METHOD__ . ' - invalid sql or placeholders specified', E_USER_ERROR);
        }
        if (!in_array($type, array('queryAll', 'queryColumn', 'queryRow', 'queryScalar')))
        {
            $type = 'queryRow';
        }
        if (!isset($sql['select']) || !isset($sql['from']))
        {
            throw new CException(__METHOD__ . ' - invalid sql specified', E_USER_ERROR);
        }
        $obj = $this->getDbConnection()->createCommand()
            ->select($sql['select'], (isset($sql['selectOptions']) ? $sql['selectOptions'] : ''))
            ->from($sql['from'] . (isset($sql['force']) ? ' FORCE INDEX(' . $sql['force'] . ')' : ''));
        if (!empty($sql['where']))
        {
            $obj->where($sql['where']);
        }
        if (!empty($sql['group']))
        {
            $obj->group($sql['group']);
        }
        if (!empty($sql['order']))
        {
            $obj->order($sql['order']);
        }
        if (!empty($sql['limit']))
        {
            if (is_array($sql['limit']))
            {
                $obj->limit($sql['limit'][1], $sql['limit'][0]);
            }
            else
            {
                $obj->limit($sql['limit']);
            }
        }
        if (isset($sql['join']))
        {
            if (isset($sql['join']['type']) && isset($sql['join']['table']) && isset($sql['join']['where']))
            {
                $obj->{$sql['join']['type']}($sql['join']['table'], $sql['join']['where']);
            }
            elseif (is_array($sql['join']))
            {
                foreach ($sql['join'] as $d)
                {
                    if (!isset($d['type']) || !isset($d['table']) || !isset($d['where']))
                    {
                        continue;
                    }
                    $obj->{$d['type']}($d['table'], $d['where']);
                }
            }
        }
        if (!empty($params))
        {
            $obj->bindValues($params);
        }
        if (isset($sql['selectOptions']) && $sql['selectOptions'] == 'SQL_CALC_FOUND_ROWS')
        {
            $result = $obj->$type();
            if (!empty($result) && is_array($result))
            {
                $totalRows = $this->getDbConnection()->createCommand('SELECT FOUND_ROWS()')->queryScalar();
                foreach ($result as &$elem)
                {
                    $elem['totalRows'] = $totalRows;
                }
            }
            return $result;
        }
        return $obj->$type();
    }

    public function parseWhere($attributes, $isFilterAttributes = true)
    {
        $where = '';
        $ps = array();
        $parsedAttributes = $attributes;
        if ($isFilterAttributes)
        {
            $parsedAttributes = $this->filterAttributes($attributes);
        }
        if (empty($parsedAttributes))
        {
            return $where;
        }
        $alias = empty($this->alias) ? $this->tableName() : $this->alias;
        foreach ($parsedAttributes as $attr => $value)
        {
            $operand = '=';
            if (isset($value) && is_array($value))
            {
                $operand = $value[0];
                $value = $value[1];
            }
            if (in_array($operand, array('NOT IN', 'IN')) && is_array($value))
            {
                $where .= $alias . '.' . $attr . ' ' . $operand . ' (' . implode(',', $value) . ')';
            }
            elseif ('BETWEEN' == $operand && is_array($value))
            {
                $where .= $alias . '.' . $attr . ' BETWEEN '  . $value[0] . ' AND ' . $value[1];
            }
            else
            {
                $where .= $alias . '.' . $attr . ' ' . $operand . ' :' . $alias . $attr;
                $ps[':' . $alias . $attr] = $value;
            }
            $where .= ' AND ';
        }
        return array('where' => substr($where, 0, -5), 'ps' => $ps);
    }


    public function filterAttributes($attributes)
    {
        if (empty($attributes) || !is_array($attributes))
        {
            return false;
        }
        $parsedAttributes = array();
        foreach ($this->_getAvailableAttributes() as $attr => $validator)
        {
            if (!isset($attributes[$attr]))
            {
                continue;
            }
            switch ($validator)
            {
                case 'int':
                    if (!is_array($attributes[$attr]))
                    {
                        $value = isset($attributes[$attr]) && is_numeric($attributes[$attr]) ? $attributes[$attr] : null;
                    }
                    else
                    {
                        $value = $attributes[$attr];
                    }
                    break;
                case 'string':
                    if (!is_array($attributes[$attr]))
                    {
                        $value = isset($attributes[$attr]) && is_string($attributes[$attr]) ? $attributes[$attr] : null;
                    }
                    else
                    {
                        $value = $attributes[$attr];
                    }
                    break;
                default:
                    if (!method_exists($this, $validator))
                    {
                        $value = null;
                    }
                    else
                    {
                        $value = $this->$validator($attr, $attributes[$attr]);
                    }
                    break;
            }
            if (is_null($value))
            {
                continue;
            }
            $parsedAttributes[$attr] = $value;
        }
        return $parsedAttributes;
    }

    protected function _getAvailableAttributes()
    {
        return array();
    }

    private function _getHandlers()
    {
        return array(
            'find' => true,
            'findAll' => true,
            'findByPk' => true,
            'findAllByPk' => true,
            'findByAttributes' => true,
            'findAllByAttributes' => true,
            'getAllByCriteria' => true,
            'getRowByCriteria' => true,
            'getColumnByCriteria' => true,
            'getScalarByCriteria' => true,
        );
    }

    /**
     * _processViaCache
     * Processing request via cache
     * $name - one of _getCacheHandlers()
     * $arguments consists of
     * 'conditions' - required
     * 'pk'
     * 'params'
     * 'duration' - default 3600,
     * 'cacheKey' - if empty, generating key as md5 of conditions
     *
     * @param string $name
     * @param array $arguments
     * @access private
     * @return object | array of objects
     */
    private function _processViaCache($name, $arguments)
    {
        if (empty($arguments['cacheKey']))
        {
            if (is_array($arguments))
            {
                $arguments['cacheKey'] = serialize($arguments);
            }
            $arguments['cacheKey'] = md5($arguments['cacheKey']);
        }

        $key = $this->_cacheKeyPrefix . $arguments['cacheKey'];
        $duration = !empty($arguments['duration']) ? $arguments['duration'] : 3600;

        if (false !== ($result = Yii::app()->cache->get($key)))
        {
            return $result;
        }

        $name = str_replace('ViaCache', '', $name);

        $result = $this->_process($name, $arguments);

        if ($result)
        {
            Yii::app()->cache->set($key, $result, $duration);
        }
        return $result;
    }

    private function _process($name, $arguments)
    {
        $result = null;

        switch ($name) {
            case 'find':
            case 'findAll':
                $result = $this->$name((isset($arguments['conditions']) ? $arguments['conditions'] : ''), (isset($arguments['params']) ? $arguments['params'] : array()));
                break;

            case 'findByPk':
            case 'findAllByPk':
                if (empty($arguments['pk']))
                {
                    throw new CException(__METHOD__ . ' - empty primary key', E_USER_ERROR);
                }
                $result = $this->$name($arguments['pk'], (isset($arguments['conditions']) ? $arguments['conditions'] : ''), (isset($arguments['params']) ? $arguments['params'] : array()));
                break;

            case 'findByAttributes':
            case 'findAllByAttributes':
                if (empty($arguments['attributes']))
                {
                    throw new CException(__METHOD__ . ' - empty attributes', E_USER_ERROR);
                }
                $result = $this->$name($arguments['attributes'], (isset($arguments['conditions']) ? $arguments['conditions'] : ''), (isset($arguments['params']) ? $arguments['params'] : array()));
                break;
            case 'getByCriteria':
            case 'getAllByCriteria':
            case 'getRowByCriteria':
            case 'getColumnByCriteria':
            case 'getScalarByCriteria':
                if (empty($arguments['conditions']))
                {
                    throw new CException(__METHOD__ . ' - incorrect sql specified', E_USER_ERROR);
                }
                $type = str_replace(array('get', 'ByCriteria'), array('query', ''), $name);
                $result = $this->executeQuery(
                    $arguments['conditions'],
                    !empty($arguments['params']) ? $arguments['params'] : array(),
                    $type
                );
                break;

            default:
                throw new CException(__METHOD__ . ' - unknoun method : {' . $name . '}', E_USER_ERROR);
                break;
        }
        return $result;
    }
}