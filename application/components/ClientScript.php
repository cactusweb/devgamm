<?php

Yii::import('application.vendors.ExtendedClientScript.ExtendedClientScript');

class ClientScript extends ExtendedClientScript
{
    protected static $_assetsPath = null;

    /**
    * Exclude certain files from inclusion. array('/path/to/excluded/file') Useful for fixed base
    * and incidental additional CSS.
    */
    public $excludeCssFiles = array();


    public function registerScriptFile($url, $position = NULL, array $htmlOptions = Array())
    {
        if (0 === strpos($url, "/"))
        {
            return parent::registerScriptFile($url, $position, $htmlOptions);
        }
        return parent::registerScriptFile($this->publishScriptFile($url), $position, $htmlOptions);
    }

    public function registerCssFile($url, $media = '')
    {
        if (0 === strpos($url, '/'))
        {
            return parent::registerCssFile($url);
        }
        return parent::registerCssFile($this->publishCssFile($url), $media);
    }

    public function publishScriptFile($url)
    {
        if (!isset(self::$_assetsPath))
        {
            self::$_assetsPath = Yii::getPathOfAlias('application.assets');
        }
        return Yii::app()->assetManager->publish(self::$_assetsPath . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . $url);
    }

    public function publishCssFile($url)
    {
        if (!isset(self::$_assetsPath))
        {
            self::$_assetsPath = Yii::getPathOfAlias('application.assets');
        }
        return Yii::app()->assetManager->publish(self::$_assetsPath . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . $url);
    }

    /**
    * Will combine/compress JS and CSS if wanted/needed, and will continue with original
    * renderHead afterwards
    *
    * @param <type> $output
    */
    public function renderHead(&$output)
    {
        if ($this->combineFiles)
        {
            $this->combineJs = $this->combineCss = true;
        }

        $this->renderJs($output, parent::POS_HEAD);

        if (!$this->combineCss)
        {
            return CClientScript::renderHead($output);
        }

        if (0 == count($this->cssFiles))
        {
            return CClientScript::renderHead($output);
        }

        $cssFiles = array();
        foreach ($this->cssFiles as $url => $media)
        {
            $startPos = strrpos($url, '/');
            $file = substr($url, $startPos+1, strlen($url));

            if (!empty($this->excludeCssFiles) && isset($this->excludeCssFiles[$file]) || $this->isRemoteFile($url))
            {
                continue;
            }
            $cssFiles[$media][$url] = $url;
            $this->cssFiles[$url] = (0 === strpos($media, '_') ? '' : $media);
        }

        foreach ($cssFiles as $media => $url)
        {
            if (0 === strpos($media, '_'))
            {
                $this->combineAndCompress('css', $url, '');
            }
            else
            {
                $this->combineAndCompress('css', $url, $media);
            }
        }

        $cssFiles = array();
        $baseUrl = Yii::app()->Core->getBaseUrl();
        foreach ($this->cssFiles as $url => $media)
        {
            $cssFiles[$baseUrl . $url] = $media;
        }
        $this->cssFiles = $cssFiles;
        $jsFiles = array();
        foreach ($this->scriptFiles as $p => $package)
        {
            foreach ($package as $i => $u)
            {
                $jsFiles[$p][$baseUrl . $i] = $baseUrl . $u;
            }
        }
        $this->scriptFiles = $jsFiles;

        CClientScript::renderHead($output);
    }

    /**
     * @param <type> $output
     * @param <type> $pos
     */
    public function renderJs($output, $pos)
    {
        if ($this->combineJs && isset($this->scriptFiles[$pos]) && 0 !== count($this->scriptFiles[$pos]))
        {
            $jsFiles = $this->scriptFiles[$pos];

            foreach ($jsFiles as &$fileName)
            {
                 $startPos = strrpos($fileName, '/');
                 $file = substr($fileName, $startPos+1, strlen($fileName));

                 (!empty($this->excludeFiles) && isset($this->excludeFiles[$file]) || $this->isRemoteFile($fileName)) && $fileName = false;
            }

            $jsFiles = array_filter($jsFiles);
            $this->combineAndCompress('js', $jsFiles, $pos);
        }
    }

    /**
     * Renders the specified core javascript library.
     */
    public function renderCoreScripts()
    {
        if (null === $this->coreScripts)
        {
            return;
        }

        $cssFiles = array();
        $jsFiles = array();

        foreach ($this->coreScripts as $name => $package)
        {
            $baseUrl = $this->getPackageBaseUrl($name);
            if (!empty($package['js']))
            {
                foreach ($package['js'] as $js)
                {
                    $jsFiles[$baseUrl . '/' . $js] = $baseUrl . '/' . $js;
                }
            }

            if (empty($package['css']))
            {
                continue;
            }
            foreach ($package['css'] as $media => $css)
            {
                $m = !empty($media) ? $media : '';
                if (!is_array($css))
                {
                    $cssFiles[$baseUrl . '/' . $css] = $m;
                }
                else
                {
                    foreach ($css as $file)
                    {
                        $cssFiles[$baseUrl . '/' . $file] = $m;
                    }
                }
            }
        }
        // merge in place
        if (!empty($cssFiles))
        {
            foreach ($this->cssFiles as $cssFile => $media)
            {
                $cssFiles[$cssFile] = $media;
            }

            $this->cssFiles = $cssFiles;
        }

        if (!empty($jsFiles))
        {
            if (isset($this->scriptFiles[$this->coreScriptPosition]))
            {
                foreach ($this->scriptFiles[$this->coreScriptPosition] as $url)
                {
                    $jsFiles[$url] = $url;
                }
            }
            $this->scriptFiles[$this->coreScriptPosition] = $jsFiles;
        }
    }
}