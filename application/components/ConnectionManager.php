<?php

class ConnectionManager
{
    static private $_db = array();

    public static function setDb($dbConfig, $extensionClass)
    {
        $extensionClass = (string)$extensionClass;
        if (empty($extensionClass) || !is_string($extensionClass) || !class_exists($extensionClass, false))
        {
            return false;
        }
        if (isset(self::$_db[$extensionClass]))
        {
            return true;
        }

        self::$_db[$extensionClass] = Yii::createComponent($dbConfig);
        self::$_db[$extensionClass]->init();
        self::$_db[$extensionClass]->active = true;
        if (method_exists(self::$_db[$extensionClass], "setConnectionName"))
        {
            self::$_db[$extensionClass]->setConnectionName($extensionClass);
        }
    }

    public static function getDbConnection($extensionClass)
    {
        if (!isset(self::$_db[$extensionClass]))
        {
            throw new CException(__METHOD__ . ' - can\'t get db connection', E_USER_ERROR);
        }
        return self::$_db[$extensionClass];
    }
}