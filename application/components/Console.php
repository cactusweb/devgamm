<?php

class Console extends CConsoleCommand
{
    public function sendDebugMail($body, $subject = 'debug')
    {
        mail(!empty(Yii::app()->params['debug_mail']) ? Yii::app()->params['debug_mail'] : 'perov@maximagroup.com', $subject, print_r($body, true));
    }
}