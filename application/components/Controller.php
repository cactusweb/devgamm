<?php

class Controller extends CController
{
    public $layout = 'main';
    public $menu = array();

    public function init()
    {
        if ('en_us' == Yii::app()->language)
        {
            Yii::app()->language = 'en';
        }
    }

    public function sendDebugMail($body, $subject = 'debug')
    {
        mail(!empty(Yii::app()->params['debug_mail']) ? Yii::app()->params['debug_mail'] : 'perov@maximagroup.com', $subject, print_r($body, true));
    }

    public function filterAccessControl($filter)
    {
        if (Yii::app()->user->isGuest && Yii::app()->user->loginUrl != Yii::app()->getRequest()->pathInfo)
        {
            $this->_denyUrl('/' . Yii::app()->user->loginUrl);
        }
        return parent::filterAccessControl($filter);
    }

    public function isUserHasAccess($action = '', $params = array())
    {
        $actions = $this->_getAccessActions();
        $action = empty($action) ? $this->action->id : $action;
        if (empty($actions) || in_array($action, array('login', 'logout', 'index')) || in_array(Yii::app()->user->id, array('admin', 'awards')))
        {
            return true;
        }
        if (!isset($actions[Yii::app()->user->id]) || !isset($actions[Yii::app()->user->id][$action]))
        {
            return false;
        }
        if (!is_array($actions[Yii::app()->user->id][$action]))
        {
            return true;
        }
        $params = empty($params) ? $this->getActionParams() : $params;
        if (empty($params) && !empty($_POST['type']))
        {
            $params['type'] = $_POST['type'];
        }
        if (empty($params))
        {
            return true;
        }
        foreach ($actions[Yii::app()->user->id][$action] as $param => $value)
        {
            if (!isset($params[$param]))
            {
                return false;
            }
            if (!is_array($value))
            {
                if ($params[$param] != $value)
                {
                    return false;
                }
            }
            else
            {
                if (!in_array($params[$param], $value))
                {
                    return false;
                }
            }
        }
        return true;
    }

    protected function _getAccessActions()
    {
        return array();
    }

    protected function _denyUrl($redirect)
    {
        if (!Yii::app()->request->isAjaxRequest)
        {
            $this->redirect($redirect);
        }
        echo json_encode(array('redirect' => $redirect));
        Yii::app()->end();
    }

    protected function getTicketByToken($token){
        $token_exp = 3600;

        $ticket = Ticket::model()->find('access_token = :access_token', array(':access_token'=>$token));

        if(!$ticket) return false;

        if((strtotime($ticket->last_login) + $token_exp) < time()) return false;

        return $ticket;
    }
}