<?php

class Core
{
    public $devgammHost = '//devgamm.com/';
    public $gameCategories;
    protected $_lang = 'en';

    public function init()
    {
    }

    public function getBaseUrl()
    {
        return preg_replace('/http[s]?:/', '', Yii::app()->getBaseUrl(true));
    }

    public function __get($field)
    {
        if (in_array($field, array('conferences', 'conf', 'lang', 'confDir', 'sponsorTypes', 'imgHost', 'expertCategory')))
        {
            $field = '_get' . ucfirst($field);
            return $this->$field();
        }
    }

    public function updateRating($model, $id, $direction)
    {
        if ('Game' == $model)
        {
            Yii::app()->cache->delete('games');
        }
        return Yii::app()->eCore->updateRating($id, $direction, $model);
    }

    public function addGame($data)
    {
        if (empty($data))
        {
            return false;
        }
        try
        {
            return Yii::app()->eCore->saveGame($data);
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    public function updateGame($id, $data)
    {
        Yii::app()->cache->delete('games');
        return Yii::app()->eCore->saveGame($data, $id);
    }

    public function deleteParticipant($model, $id)
    {
        return Yii::app()->eCore->deleteElement($id, $model);
    }

    public function deleteGame($id)
    {
        Yii::app()->cache->delete('games');
        return Yii::app()->eCore->deleteElement($id, 'Game');
    }

    public function deleteCategory($model, $id)
    {
        if (empty($model) || !is_string($model) || empty($id) || !is_numeric($id))
        {
            throw new CException(__METHOD__ . ' - empty or invalid incoming parameters', E_USER_ERROR);
        }

        return Yii::app()->eCore->deleteElement($id, $model);
    }

    public function approveGame($id, $approve)
    {
        Yii::app()->cache->delete('games');
        return Yii::app()->eCore->saveGame(array('approved' => $approve), $id);
    }

    public function getParticipantsForConsole($model)
    {
        return Yii::app()->eCore->getParticipants($model, '*', false);
    }

    public function saveParticipant($model, $data, $id = null)
    {
        return Yii::app()->eCore->saveParticipant($model, $data, $id);
    }

    public function saveCategory($model, $data, $id = null)
    {
        return Yii::app()->eCore->saveCategory($model, $data, $id);
    }

    public function getConversation($model)
    {
        return Yii::app()->eCore->getConversation($model);
    }

    public function getParticipants($model, $params = array())
    {
        if (false !== ($data = Yii::app()->cache->get('list' . $model . Yii::app()->language . Yii::app()->conf . serialize($params))))
        {
            return $data;
        }
        $select = array();
        switch ($model)
        {
            case 'Partner':
                $select = array(
                    'id', 'instr(email, "@") as receive_email', 'name_' . Yii::app()->language . ' as name',
                    'company_title', 'company_url', 'position', 'photo', 'lookingfor_' . Yii::app()->language . ' as lookingfor',
                    'propose_' . Yii::app()->language . ' as propose', 'rating'
                );
                break;
            case 'Sponsor':
                $select = array(
                    'id', 'instr(email, "@") as receive_email', 'title_' . Yii::app()->language . ' as title',
                    'logo', 'url', 'description_' . Yii::app()->language . ' as description',
                    'category_id', 'rating'
                );
                break;
            case 'Speaker':
                $select = array(
                    'id', 'instr(email, "@") as receive_email', 'photo', 'company_logo',
                    'category_id', 'name_' . Yii::app()->language . ' as name', 'description_' . Yii::app()->language . ' as description', 'rating',
                    'bio_' . Yii::app()->language . ' as bio', 'topic_' . Yii::app()->language . ' as topic', 'video', 'presentation', 'foreign_speaker',
                );
                break;
            case 'Expert':
            case 'DiscussionPanel':
                $select = array(
                    'id', 'instr(email, "@") as receive_email', 'photo', 'company_logo',
                    'category_id', 'name_' . Yii::app()->language . ' as name', 'rating',
                    'bio_' . Yii::app()->language . ' as bio',
                );
                if ('Expert' == $model)
                {
                    $select[] = 'foreign_speaker';
                }
                break;
            default:
                throw new CException('Unknown model');
                break;
        }
        $data = Yii::app()->eCore->getParticipants($model, $select, true, $params);
        Yii::app()->cache->set('list' . $model . Yii::app()->language . Yii::app()->conf . serialize($params), $data, 600);
        return $data;
    }

    public function getCategories($isForConsole = false, $model = 'SpeakerCategory')
    {
        if (!$isForConsole && (false !== $data = Yii::app()->cache->get(Yii::app()->conf . 'speaker_categories' . $model . Yii::app()->language)))
        {
            return $data;
        }
        $select = array('id', 'title_' . Yii::app()->language . ' as title');
        if ('SpeakerCategory' == $model)
        {
            $select[] = 'color';
        }
        elseif ('DiscussionCategory' == $model)
        {
            $select = array('id', 'title_' . Yii::app()->language . ' as title', 'description_' . Yii::app()->language . ' as description');
        }
        $data = Yii::app()->eCore->getCategories($isForConsole ? '*' : $select, $model);
        if ($isForConsole)
        {
            return $data;
        }
        if (empty($data) || !is_array($data))
        {
            return array();
        }
        $result = array();
        foreach($data as $row)
        {
            switch ($model)
            {
                case 'SpeakerCategory':
                    $result[$row['id']] = array(
                        'title' => $row['title'],
                        'color' => $row['color'],
                    );
                    break;
                case 'DiscussionCategory':
                    $result[$row['id']] = array(
                        'title' => $row['title'],
                        'description' => $row['description'],
                    );
                    break;
                default:
                    $result[$row['id']] = $row['title'];
                    break;
            }
        }
        Yii::app()->cache->set(Yii::app()->conf . 'speaker_categories' . $model . Yii::app()->language, $result, 600);
        return $result;
    }

    public function getCSV($model)
    {
        $headers = $data = array();
        switch ($model)
        {
            case 'Game':
                $games = Yii::app()->eCore->getGames(array(), 'date');
                if (empty($games) || !is_array($games))
                {
                    return false;
                }
                $headers = array_flip(array_keys(reset($games)));
                foreach ($games as &$row)
                {
                    $links = unserialize($row['link']);
                    $row['link'] = "";
                    if (!empty($links) && is_array($links))
                    {
                        foreach ($links as $type => $value)
                        {
                            $row['link'] .= $type .': '. $value . "\n";
                        }
                    }
                    $platforms = unserialize($row['platforms']);
                    $row['platforms'] = "";
                    if (!empty($platforms) || is_array($platforms)) {
                        foreach ($platforms as $platform) {
                            $row['platforms'] .= $platform['title'] . "\n";
                        }
                        if(!empty($platform['childs']) && is_array($platform['childs'])){
                            foreach ($platform['childs'] as $subplatform) {
                                $row['link'] .= $subplatform['title'] .": ". $subplatform['link'] . "\n";
                            }
                        }
                    }
                    $technologies = unserialize($row['technology']);
                    $row['technology'] = "";
                    if (!empty($technologies) || is_array($technologies)) {
                        foreach ($technologies as $technology) {
                            $row['technology'] .= $technology['title'] . "\n";
                        }
                    }
                }
                $headers = array_keys($headers);
                sort($headers);
                foreach ($games as $k => $row)
                {
                    foreach ($headers as $key)
                    {
                        $data[$k][$key] = isset($row[$key]) ? $row[$key] : '';
                    }
                }
                break;
            default:
                throw new CException('Unknown model');
                break;
        }
        if (empty($data) || !is_array($data))
        {
            return false;
        }

        $now = gmdate('Y-m-d_H:i:s');

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Type: application/octet-stream');

        header("Content-Disposition: attachment;filename={$model}_export_{$now}.csv");
        header('Content-Transfer-Encoding: binary');

        echo $this->_prepareCSVAttach($headers, $data);

        return true;
    }

    public function getGamesForConsole($params = array())
    {
        $conditions = array();
        if (isset($params['lynch']))
        {
            $conditions = array('lynch' => 1);
        }
        if (isset($params['gammplay']))
        {
            $conditions = array('gammplay' => 1);
        }
        if (isset($params['approved']))
        {
            $conditions['approved'] = 1;
        }
        if (isset($params['notapproved']))
        {
            $conditions = array('approved' => 0);
        }
        $games = Yii::app()->eCore->getGames($conditions);
        if (empty($games) || !is_array($games))
        {
            return array();
        }
        $gameCats = $this->getGamesCategories();
        foreach ($games as $id => $g)
        {
            if (!isset($gameCats[$g['category_id']]))
            {
                $games[$id]['_category'] = $gameCats[1];
            }
            else
            {
                $games[$id]['_category'] = $gameCats[$g['category_id']];
            }
        }
        return $games;
    }

    public function getGames()
    {
        $key = 'games' . Yii::app()->language . Yii::app()->conf;

        if (false !== Yii::app()->cache->get('games') && false !== $data = Yii::app()->cache->get($key))
        {
            return $data;
        }
        $data = Yii::app()->eCore->getGames(array('approved' => 1));
        if (empty($data) || !is_array($data))
        {
            return array();
        }
        $categories = $this->getGamesCategories();
        if (empty($categories))
        {
            throw new CException('Games categories not found');
        }
        $result = array();
        foreach ($categories as $id => $cat)
        {
            $result[$id]['category'] = $cat;
        }

        foreach ($data as $row)
        {
            $result[$row['category_id']]['games'][$row['id']] = $row;
        }
        Yii::app()->cache->set($key, $result, 3600);
        Yii::app()->cache->set('games', true, 86400);

        return $result;
    }

    public function isTicketExist($ticket)
    {
        if (empty($ticket) || !is_string($ticket) || strlen($ticket) != 16)
        {
            throw new CException(__METHOD__ . ' - empty or invalid ticket', E_USER_ERROR);
        }

        $data = Yii::app()->eCore->isTicketExist($ticket);
        if (empty($data))
        {
            return false;
        }
        return true;
    }

    public function getFlashGameDimension($id)
    {
        if (!$id)
        {
            throw new CHttpException('Page not found');
        }
        if (false !== $dim = Yii::app()->cache->get('flash-game-dimension' . $id . Yii::app()->conf))
        {
            return $dim;
        }
        $game = Yii::app()->eCore->getFlashGame($id);
        $game = explode('x', $game['dimension']);
        $game = array(
            'x' => $game[0],
            'y' => $game[1],
        );
        Yii::app()->cache->set('flash-game-dimension' . $id . Yii::app()->conf, $game, 3600);
        return $game;
    }

    public function getGameALink($game)
    {
        if (empty($game['link']))
        {
            return 1 == $game['category_id'] ? ('//devgamm.com/' . $this->confDir . '/flash-games/' . $game['id']) : '#';
        }
        $link = unserialize($game['link']);
        $url = '';
        if (!empty($link) && is_array($link))
        {
            foreach ($link as $l)
            {
                if (!empty($l))
                {
                    $url = $l;
                    break;
                }
            }
        }
        else
        {
            $url = $game['link'];
        }
        return empty($url) ? (1 == $game['category_id'] ? ('//devgamm.com/' . $this->confDir . '/flash-games/' . $game['id']) : '#') : $url;
    }

    public function getGameIMGLink($game)
    {
        return isset($game['icon_ext']) ? '//devgamm.com/' . Yii::app()->Core->confDir . '/contest/contestGameIcon_' . $game['id'] . '.' . $game['icon_ext'] : '';
    }

    public function getGamesCategories($isForConsole = false)
    {
        $key = Yii::app()->conf . 'game_categories_' . Yii::app()->language;
        if (!$isForConsole && (false !== $data = Yii::app()->cache->get($key)))
        {
            return $data;
        }
        $data = Yii::app()->eCore->getGamesCategories($isForConsole ? '*' : array('id', 'color', 'icons', 'title_' . $this->_lang . ' as title'));
        if ($isForConsole)
        {
            return $data;
        }
        if (empty($data) || !is_array($data))
        {
            return array();
        }
        $result = array();
        foreach($data as $row)
        {
            $result[$row['id']] = $row;
        }
        Yii::app()->cache->set($key, $result, 3600);
        return $result;
    }

    public function getGameAuthorTypes()
    {
        return array(
            'Independent developer',
            'Game studio',
            'Game company',
        );
    }

    public function getGameTypes()
    {
        return array(
            'Mobile',
            'Flash',
            'Social',
        );
    }

    public function getGameDevelopmentStages()
    {
        return array(
            1 => 'Prototype',
            2 => 'Alfa',
            3 => 'Beta',
            4 => 'Completed',
            5 => 'Released',
        );
    }

    public function setLang($lang)
    {
        $this->_lang = $lang;
    }

    protected function _getLang()
    {
        return $this->_lang;
    }

    protected function _getConfDir()
    {
        if (!isset(Yii::app()->params['confs'][Yii::app()->conf]))
        {
            throw new CException('Unkown conference ' . Yii::app()->conf);
        }
        return Yii::app()->params['confs'][Yii::app()->conf];
    }

    protected function _getImgHost()
    {
        return $this->devgammHost . 'i/';
    }

    protected function _prepareCSVAttach($headers, $data)
    {
        ob_start();
        $handler = fopen('php://output', 'w');

        fputcsv($handler, $headers);

        foreach ($data as $row)
        {
            fputcsv($handler, $row);
        }
        fclose($handler);

        return ob_get_clean();
    }

    private function _getSponsorTypes()
    {
        return array(1 => 'Diamond', 2 => 'Platinum', 3 => 'Organizer', 4 => 'Gold', 5 => 'Silver', 6 => 'Bronze', 7 => 'Party', 8 => 'Lounge');
    }

    private function _getExpertCategory()
    {
        return array(
            1 => 'Lynch',
            2 => 'Adviser',
        );
    }

    private function _getConferences()
    {
        return array_map(function ($name) {
            return ucfirst(preg_replace('/^([a-z]+)(\d+)$/', '$1 $2', $name));
        }, Yii::app()->params['confs']);
    }

    private function _getConf()
    {
        if (!isset($this->_getConferences()[Yii::app()->conf]))
        {
            throw new CException('Unkown conference ' . Yii::app()->conf);
        }
        return $this->_getConferences()[Yii::app()->conf];
    }
}