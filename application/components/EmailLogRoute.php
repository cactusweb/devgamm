<?php

class EmailLogRoute extends CEmailLogRoute
{
    protected function processLogs($logs)
    {
        $message = '';
        foreach ($logs as $log)
        {
            $message .= $this->formatLogMessage($log[0], $log[1], $log[2], $log[3]);
        }
        $subject = $this->getSubject();
        if ($subject === null)
        {
            $subject = 'Application Log';
        }
        foreach ($this->getEmails() as $email)
        {
            $this->sendEmail($email, $subject, $message);
        }
    }
}