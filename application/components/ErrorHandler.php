<?php

class ErrorHandler extends CErrorHandler
{
    public function render($view, $data)
    {
        if (YII_DEBUG)
        {
            return parent::render($view, $data);
        }
        if ($this->isAjaxRequest())
        {
            header('HTTP/1.0 404 Not Found');
            Yii::app()->end();
        }
        $data['version'] = $this->getVersionInfo();
        $data['time'] = time();
        $data['admin'] = $this->adminInfo;
        if (!empty($data) && !empty($data['code']))
        {
            switch ($data['code'])
            {
                case 400:
                    $data['message'] = 'Bad Request';
                    break;
                case 403:
                    $data['message'] = 'Unauthorized';
                    break;
                case 404:
                    $data['message'] = 'Page Not Found';
                    break;
                case 500:
                    $data['message'] = 'Internal Server Error';
                    break;
                case 503:
                    $data['message'] = 'Service Unavailable';
                    break;
                default:
                    $data['message'] = 'Error ' . $data['code'];
                    break;
            }
        }
        $file = Yii::getPathOfAlias('application.views') . DIRECTORY_SEPARATOR . 'error' . '.php';
        include($file);
        Yii::app()->end();
    }
}