<?php

class Extension extends CComponent
{
    public $entities;
    public $namespace = '';
    protected $_recordHandler = 'ActiveRecord';
    protected $config;
    protected $_connection = 'db';

    public function init()
    {
    }

    public function model($name)
    {
        $class = ((!empty($this->namespace)) ? $this->namespace . '\\' : '') . get_class($this) . $this->_recordHandler;
        return $class::model($name);
    }

    public function newModel($name)
    {
        $class = ((!empty($this->namespace)) ? $this->namespace . '\\' : '') . $name;
        return new $class();
    }

    public function getConnection()
    {
        ExtensionActiveRecord::setDb($this->config[$this->_connection], get_class($this));
        return $this->_connection;
    }

    public function setConnection($connection = '')
    {
        $this->_connection = empty($connection) ? 'db' : $connection;
        return $this;
    }

    protected function setConfig($config)
    {
        $this->config = $config;
        $this->init();
    }

    /**
     * _executeRelatedQuery
     * Provides executing query with related models
     *
     * $conditions = array(
     *      'select' => array(
     *          'count(1) as cnt',
     *      ),
     *      'from' => 'User',
     *      'conditions' => array(
     *          'User' => array(
     *              'country' => 'ua',
     *          ),
     *          'UserTraffic' => array(
     *              'install_time' => array('BETWEEN' => ('2013-05-01', '2013-05-05')),
     *              'app' => ('bp', 'bo', 'bc'),
     *          )
     *      ),
     *      'join' => array(
     *          array(
     *              'model' => 'UserTraffic',
     *              'conditions' => array(
     *                  'id' => 'id',
     *              ),
     *          ),
     *       ),
     * );
     * $importantConditions = array(
     *      'UserTraffic' => array(
     *          'install_time',
     *      ),
     * );
     * @param mixed $conditions
     * @access protected
     * @return mixed
     */
    protected function _executeRelatedQuery($type, $conditions, $importantConditions = array(), $connectionPostfix = '')
    {
        if (empty($type) || !isset($this->_getQueryHandlers()[$type]))
        {
            throw new CDbException(__METHOD__ . ' - incorrect type specified', E_USER_ERROR);
        }
        if (empty($conditions) || !is_array($conditions))
        {
            throw new CDbException(__METHOD__ . ' - incorrect conditions specified', E_USER_ERROR);
        }
        if (empty($conditions['select']) || !is_array($conditions['select']))
        {
            throw new CDbException(__METHOD__ . ' - conditions specified with empty "select" field', E_USER_ERROR);
        }
        if (!isset($conditions['from']))
        {
            throw new CDbException(__METHOD__ . ' - conditions specified with empty "from" field', E_USER_ERROR);
        }
        if (!($from = $this->model($conditions['from'])))
        {
            throw new CDbException(__METHOD__ . ' - incorrect "from" field: {' . $conditions['from'] . '}', E_USER_ERROR);
        }
        $conditions['from'] = $from->tableName() . ' as ' . $from->alias;
        if (isset($conditions['join']) && is_array($conditions['join']))
        {
            $j = array();
            foreach ($conditions['join'] as $joinData)
            {
                if (!isset($joinData['model']) || !isset($joinData['conditions']) || !is_array($joinData['conditions']))
                {
                    continue;
                }
                $c = array();
                foreach ($joinData['conditions'] as $f => $t)
                {
                    if ($f == '_' && is_array($t))
                    {
                        $c = array_merge($c, $t);
                    }
                    else
                    {
                        $c[] = $from->alias . '.' . $f . ' = ' . (isset($joinData['alias']) ? $joinData['alias'] : $this->model($joinData['model'])->alias) . '.' . $t;
                    }
                }
                $jType = isset($joinData['type']) ? $joinData['type'] : 'join';
                $j[] = array(
                    'type' => $jType,
                    'table' => $this->model($joinData['model'])->tableName() . ' as ' . (isset($joinData['alias']) ? $joinData['alias'] : $this->model($joinData['model'])->alias),
                    'where' => implode (' AND ', $c),
                );
            }
            $conditions['join'] = $j;
        }
        $filteredConds = array('where' => array(), 'ps' => array());
        if (!empty($conditions['conditions']) && is_array($conditions['conditions']))
        {
            foreach ($conditions['conditions'] as $model => $attributes)
            {
                if ('_' == $model)
                {
                    $filteredConds['where'][] = $attributes;
                    continue;
                }
                $obj = $this->model($model);
                if (!is_object($obj))
                {
                    throw new CDbException(__METHOD__ . ' - incorrect model specified: {' . $model . '}', E_USER_ERROR);
                }
                $att = $obj->filterAttributes($attributes);
                if (isset($importantConditions[$model]) && ($im = array_diff($importantConditions[$model], array_keys($att))))
                {
                    throw new CDbException(__METHOD__ . ' - filtered important conditions: {' . print_r($im, true) . '}', E_USER_ERROR);
                }
                $where = $obj->parseWhere($att, false);
                if (!isset($where['where']))
                {
                    throw new CException(__METHOD__ . ' - incomplete where specified', E_USER_ERROR);
                }
                $filteredConds['where'][] = $where['where'];
                $filteredConds['ps'] = array_merge($filteredConds['ps'], $where['ps']);
                unset($where);
            }
            $filteredConds['where'] = implode(' AND ', $filteredConds['where']);
            unset($conditions['conditions']);

            $conditions['where'] = $filteredConds['where']. " ";
        }
        return $from->$type(array('conditions' => $conditions, 'params' => $filteredConds['ps']));
    }

    private function _getQueryHandlers()
    {
        return array(
            'getAllByCriteria' => true,
            'getRowByCriteria' => true,
            'getColumnByCriteria' => true,
            'getScalarByCriteria' => true,
            'getAllByCriteriaViaCache' => true,
            'getRowByCriteriaViaCache' => true,
            'getColumnByCriteriaViaCache' => true,
            'getScalarByCriteriaViaCache' => true,
        );
    }
}