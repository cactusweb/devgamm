<?php

class ExtensionActiveRecord extends ActiveRecord
{
    private $_cacheKeyPrefix = 'cache';
    private $extensionClass = null;

    public function __construct($scenario = 'insert')
    {
        $this->extensionClass = null;

        $selfClass = get_class($this);

        $extensionClass = strstr($selfClass, "\\extensions\\");
        $extensionClass = explode("\\", $extensionClass);

        if (isset($extensionClass[2]))
        {
            $extensionClass = implode('', array_map('ucfirst', explode('_', $extensionClass[2])));
            $extensionClass = 'E' . $extensionClass;
            if (class_exists($extensionClass, false))
            {
                $this->extensionClass = $extensionClass;
            }
            else
            {
                throw new CException(__METHOD__ . ' Supposed extension class dose not exist <![CDATA['.$extensionClass.']]>.', E_USER_WARNING);
            }
        }
        else
        {
            throw new CException(__METHOD__ . ' can`t find extension name.', E_USER_ERROR);
        }

        parent::__construct($scenario);
    }

    public static function setDb($dbConfig, $extensionClass)
    {
        return ConnectionManager::setDb($dbConfig, $extensionClass);
    }

    public function getDbConnection()
    {
        return ConnectionManager::getDbConnection($this->extensionClass, Yii::app()->{'e' . substr($this->extensionClass, 1)}->connection);
    }

    public static function model($className = __CLASS__)
    {
        return parent::model(implode('\\', explode('\\', get_called_class(), -1)) . '\\' . $className);
    }
}