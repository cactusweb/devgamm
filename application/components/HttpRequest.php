<?php

class HttpRequest extends CHttpRequest
{
    private $_csrfToken;

    public function getCsrfToken()
    {
        if (is_null($this->_csrfToken))
        {
            $csrfToken = Yii::app()->session->itemAt($this->csrfTokenName);
            if (is_null($csrfToken))
            {
                $csrfToken = md5(sha1(sha1(uniqid(mt_rand(), true))));
                Yii::app()->session->add($this->csrfTokenName, $csrfToken);
            }
            $this->_csrfToken = $csrfToken;
        }
        return $this->_csrfToken;
    }

    public function parseQueryString()
    {
        $attributes = array();
        foreach (explode('&', urldecode($_SERVER['QUERY_STRING'])) as $part)
        {
            if ($param = explode('=', $part))
            {
                $val = quotemeta(strip_tags(trim($param[1])));
                if (empty($val))
                {
                    continue;
                }
                $attributes[$param[0]] = $val;
            }
        }
        unset($attributes['code']);
        unset($attributes['state']);
        return $attributes;
    }

    public function getCleanedRedirectUri()
    {
        $rq = (is_string($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'][0] != '/' ? ('/' . $_SERVER['REDIRECT_URL']) : $_SERVER['REDIRECT_URL']);
        if (Yii::app()->Brand->platform != 'web' && (0 == strpos($rq, '/m')))
        {
            $rq = substr($rq, 2);
        }
        $q = $this->parseQueryString();
        return Yii::app()->FB->getAppUrl()
            . $rq
            . (!empty($q) ? '?' . urlencode(http_build_query($q)) : '');
    }

    public function getCurrentAbsoluteUrlWithoutQuery()
    {
        return Yii::app()->FB->getAppUrl() . $_SERVER['REDIRECT_URL'];
    }

    public function validateCsrfToken($tokenName)
    {
        if ($this->getIsPostRequest())
        {
            if (Yii::app()->session->contains($this->csrfTokenName) && !empty($_POST[$this->csrfTokenName]))
            {
                if (Yii::app()->session->itemAt($this->csrfTokenName) == $_POST[$this->csrfTokenName])
                {
                    return true;
                }
            }
        }
        return false;
    }
}
