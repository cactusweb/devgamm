<?php

/**
 * MissingTranslation contains methods for handling onMissTranslation event
 */
class MissingTranslation extends CApplicationComponent
{
    /**
     * handle on miss translation actions. Track key and category
     * @todo create track. Send info into developer mailbox
     * @param  object $event event raised on translation is missed
     * @access public
     * @return void
     */
    public static function handler($event)
    {
        $message = $event->message;
        $category = $event->category;
        $language = $event->language;
        $pathToMessages = $event->sender->basePath;
        //mail to developer mailbox
    }
}