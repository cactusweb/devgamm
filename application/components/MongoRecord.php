<?php

/**
 * MongoRecord
 * Provides work with mongo collections throught vendors class
 *
 * @uses MongoDbRecord
 * @package FF
 * @version $id$
 * @copyright 2013 Renatus
 * @author Victor Perov <vic.gald@gmail.com>
 */
class MongoRecord extends MongoDbRecord
{
    protected $_cacheKeyPrefix = '';
    protected $_collectionPrefix = '';
    protected $_simpleInsertData = null;
    private static $__models = array();

    public function __call($name, $arguments)
    {
        $name = str_replace('ViaCache', '', $name, $isCache);
        if (isset($this->_getHandlers()[$name]))
        {
            $arguments = $arguments[0];
            return $isCache ? $this->_processViaCache($name, $arguments) : $this->_process($name, $arguments);
        }

        return parent::__call($name, $arguments);
    }

    public static function model($className=__CLASS__)
    {
        if (isset(self::$__models[$className]))
        {
            return self::$__models[$className];
        }
        else
        {
            $model = self::$__models[$className] = new $className(null);
            return $model;
        }
    }

    public function flushCacheKey($key)
    {
        Yii::app()->cache->delete($this->_cacheKeyPrefix . $key);
    }

    public function getHashId()
    {
        return isset($this->_document['_id'])? $this->_document['_id'] : null;
    }

    public function count($attributes = array())
    {
        $query = $this->_formQuery($attributes);
        $doc = $this->collection->count($query);
        return !is_null($doc) ? $this->instantiate($doc) : null;
    }

    public function findAll($criteria = array())
    {
        $docs = $this->collection->find(isset($criteria['query']) ? $criteria['query'] : array(), (isset($criteria['fields']) && is_array($criteria['fields']) ? $criteria['fields'] : array()));

        if (isset($criteria['skip']))
        {
            $docs = $docs->skip($criteria['skip']);
        }
        if (isset($criteria['limit']))
        {
            $docs = $docs->limit($criteria['limit']);
        }
        if (isset($criteria['sort']))
        {
            $docs = $docs->sort($criteria['sort']);
        }
        return $this->populateRecords($docs);
    }

    public function find($criteria = array())
    {
        if (!empty($criteria['query']))
        {
            $doc = $this->collection->findOne($criteria['query'], isset($criteria['fields']) && is_array($criteria['fields']) ? $criteria['fields'] : array());
        }
        else
        {
            $doc = $this->collection->findOne(array(), isset($criteria['fields']) && is_array($criteria['fields']) ? $criteria['fields'] : array());
        }

        return $doc? $this->instantiate($doc) : null;
    }

    public function deleteByAttributes($attributes)
    {
        if (empty($attributes) || !is_array($attributes))
        {
            throw new CException(__METHOD__ . ' - incorrect attributes specified', E_USER_WARNING);
        }
        return $this->collection->remove($attributes, array('fsync' => true));
    }

    public function deleteAll()
    {
        return $this->collection->remove(array(), array('fsync' => true));
    }

    public function group($keys, $reduceFunction, $initial = array(), $options = array())
    {
        if (empty($keys) || empty($initial) || !is_array($initial) || empty($reduceFunction) || !($reduceFunction instanceof MongoCode))
        {
            throw new InvalidArgumentException(__METHOD__ . ' - empty or invalid income parameters', E_USER_ERROR);
        }
        $parsedOptions = array();
        if (!empty($options))
        {
            foreach ($options as $key => $value)
            {
                if (!isset($this->_getAvailableGroupOptions()[$key]))
                {
                    throw new CException(__METHOD__ . " - operation $key isn't available in group functionality", E_USER_ERROR);
                }
                $parsedOptions[$key] = $value;
            }
        }
        $res = $this->collection->group($keys, $initial, $reduceFunction, $parsedOptions);
        return $res['retval'];
    }

    /**
     * reCreateCollection
     * Drops collection and ensures indexes if $indexes isn't empty
     * $index should be one of:
     * string, example: 'fb', where 'fb' - field of collection (index will be ascending)
     * array, example: array('fb' => 1), creates indexe ascending on field 'fb'
     * see http://www.php.net/manual/en/mongocollection.ensureindex.php for more info about supported indexes
     *
     * @param array $indexes
     * @access public
     * @return bool
     */
    public function reCreateCollection($indexes = array())
    {
        $this->collection->drop();
        return $this->ensureIndexes($indexes);
    }

    public function ensureIndexes($indexes)
    {
        if (!empty($indexes))
        {
            $this->collection->ensureIndex($indexes);
        }
        return true;
    }

    /**
     * getData
     * Main method for getting data from mongo storage
     * Supports caching results
     * $params used for setting additional params such as order, limit
     *
     * @param array $attributes
     * @param array $fields
     * @param array $params
     * @param bool $isUseCache
     * @access public
     * @return array
     */
    public function getData($attributes, $fields = array(), $params = array(), $isUseCache = false)
    {
        $query = array();

        $query['query'] = $this->_formQuery($attributes);

        if (isset($params['order']) && is_array($params['order']))
        {
            foreach ($params['order'] as $field => $direction)
            {
                $query['sort'][$field] = ($direction == 'DESC' ? -1 : 1);
            }
        }
        if (isset($params['duration']))
        {
            $query['duration'] = $params['duration'];
        }
        if (isset($params['cacheKey']))
        {
            $query['cacheKey'] = $params['cacheKey'];
        }
        if (isset($params['limit']))
        {
            $query['limit'] = $params['limit'];
        }
        if (isset($params['skip']))
        {
            $query['skip'] = $params['skip'];
        }
        $query['fields'] = empty($fields) || !is_array($fields) ? array() : $fields;
        $method = 'find' . (!isset($params['type']) || $params['type'] != 'one' ? 'All' : '') . ($isUseCache ? 'ViaCache' : '');
        return $this->$method($query);
    }

    /**
     * updateByCriteria
     * Update document in collection by specified $criteria
     * $set should be array of new document values (used as $set param)
     * $inc - rray of increment values (used as $inc param)
     *
     * @param array $criteria
     * @param array $set
     * @param bool $isUpsert
     * @param bool $isMulti
     * @param array $inc
     * @access public
     * @return void
     */
    public function updateByCriteria($criteria, $set, $isUpsert = true, $isMulti = false, $inc = array())
    {
        if (empty($set))
        {
            throw new CDbException('Can\'t update with empty object', E_USER_ERROR);
        }
        if ($this->beforeUpdate())
        {
            $params = array('fsync' => true);
            if ($isUpsert)
            {
                $params['upsert'] = true;
            }
            if ($isMulti)
            {
                $params['multiple'] = true;
            }
            $values = array('$set' => $set);
            if (!empty($inc))
            {
                $values['$inc'] = $inc;
            }
            return $this->collection->update($criteria, $values, $params);
        }
        return false;
    }

    public function multiplyInsert($data)
    {
        if (empty($data))
        {
            throw new CDbException('Can\'t insert empty objects', E_USER_ERROR);
        }
        return $this->collection->batchInsert($data, array('fsync' => true));
    }

    public function simpleInsert($data)
    {
        if (empty($data))
        {
            throw new CDbException('Can\'t insert empty objects', E_USER_ERROR);
        }
        $this->_simpleInsertData = $data;
        return $this->_beforeSimpleInsert() && $this->collection->insert($this->_simpleInsertData, array('fsync' => true));
    }

    public function filterAttribute($attr)
    {
        return strpos('$', $attr) === false;
    }

    protected function _beforeSimpleInsert()
    {
        return true;
    }

    protected function getCollectionName()
    {
        $app = Yii::app()->Core->app;
        $event = Yii::app()->Core->event;
        return $this->_collectionPrefix . '_' . (empty($app) ? '' : $app) . '_' . (empty($event) ? '' : $event);
    }

    protected function _parseWhere($where)
    {
        $query = array();
        if (empty($where) || !is_array($where))
        {
            return $query;
        }
        foreach ($where as $operator => $data)
        {
            if (!isset($this->_getAvailabaleOperators()[$operator]))
            {
                continue;
            }
            if ($operator == 'between' && is_array($data))
            {
                $query['$gte'] = $data[0];
                $query['$lte'] = $data[1];
            }
            else
            {
                $query[$operator] = $data;
            }
        }
        if (empty($query))
        {
            return array(
                '$in' => $where,
            );
        }
        return $query;
    }

    protected function beforeUpdate()
    {
        return true;
    }

    protected function instantiate($document)
    {
        return $document;
    }

    protected function afterSave()
    {
        $this->_flushNamedCache();
    }

    protected function afterDelete()
    {
        $this->_flushNamedCache();
    }

    private function _getAvailableGroupOptions()
    {
        return array(
            'condition' => 1,
            'finalize' => 1
        );
    }

    private function _formQuery($attributes)
    {
        $query = array();
        if (empty($attributes) || !is_array($attributes))
        {
            return $query;
        }
        foreach ($attributes as $key => $value)
        {
            if (is_array($value))
            {
                $query[$key] = $this->_parseWhere($value);
            }
            else
            {
                $query[$key] = $value;
            }
        }
        return $query;
    }

    private function _getAvailabaleOperators()
    {
        return array(
            '$ne' => 1,
            '$in' => 1,
            '$gt' => 1,
            '$gte' => 1,
            '$lt' => 1,
            '$lte' => 1,
            '$exists' => 1,
            'between' => 1,
        );
    }

    private function _flushNamedCache()
    {
        if (!empty($this->_document['id']))
        {
            foreach (array('Profile', 'User') as $item)
            {
                Yii::app()->cache->delete($item . 'getuserbyid' . $this->_document['id']);
            }
        }
    }

    private function _getHandlers()
    {
        return array(
            'find' => true,
            'findAll' => true,
        );
    }

    /**
     * _processViaCache
     * Processing request via cache
     * $name - one of _getCacheHandlers()
     * $arguments consists of
     * 'query' - required
     * 'duration' - default 3600,
     * 'cacheKey' - if empty, generating key as md5 of conditions
     *
     * @param string $name
     * @param array $arguments
     * @access private
     * @return object | array of objects
     */
    private function _processViaCache($name, $arguments)
    {
        if (empty($arguments['cacheKey']))
        {
            if (is_array($arguments['query']))
            {
                $arguments['cacheKey'] = serialize($arguments['query']);
            }
            if (is_array($arguments['fields']))
            {
                $arguments['cacheKey'] = (isset($arguments['cacheKey']) ? $arguments['cacheKey'] : '') . serialize($arguments['fields']);
            }
            $arguments['cacheKey'] = md5($arguments['cacheKey']);
        }

        $key = $this->_cacheKeyPrefix . $arguments['cacheKey'];
        $duration = !empty($arguments['duration']) ? $arguments['duration'] : 3600;

        if (false !== ($result = Yii::app()->cache->get($key)))
        {
            return $result;
        }

        $name = str_replace('ViaCache', '', $name);

        $result = $this->_process($name, $arguments);

        if ($result)
        {
            Yii::app()->cache->set($key, $result, $duration);
        }
        return $result;
    }

    private function _process($name, $arguments)
    {
        $result = null;

        switch ($name) {
            case 'find':
            case 'findAll':
                $result = $this->$name((isset($arguments['query']) ? $arguments['query'] : ''));
                break;

            default:
                throw new CException(__METHOD__ . ' - unknoun method : <![CDATA[' . $name . ']]>', E_USER_ERROR);
                break;
        }
        return $result;
    }
}