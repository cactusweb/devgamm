<?php

/**
 * QueueManager
 * Provides logic for working with queues throught eQueue
 *
 * @package
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
Yii::import('application.extensions.queue.EQueue');

class QueueManager
{
    protected $_queue = '';
    protected $_model = '';
    private $_handler = null;

    public function init()
    {
        $this->_handler = new EQueue();
    }

    public function setModel($model)
    {
        $this->_model = $model;
        return $this;
    }

    public function setQueue($queue)
    {
        if (empty($queue))
        {
            throw new CException(__METHOD__ . ' - empty queue specified', E_USER_ERROR);
        }
        $data = explode('_', $queue);
        if (count($data) != 3)
        {
            throw new CException(__METHOD__ . ' - incorrect queue specified: {"' . $queue . '"}', E_USER_ERROR);
        }
        $this->_queue = $queue;
        $this->_model = $data[0];

        return $this;
    }

    public function getAllQueues($appId = null, $model = 'Queue')
    {
        return $this->_handler->getAllQueues($appId, $model);
    }

    public function declareQueues($queues)
    {
        return $this->_handler->declareAllQueues($queues);
    }

    public function getOneMessage($appId)
    {
        if (empty($this->_queue) || empty($this->_model) || empty($appId))
        {
            throw new CException(__METHOD__ . ' - empty input params specified', E_USER_ERROR);
        }
        return $this->_handler->setQueue($this->_model, $this->_queue)->setPostfix($appId)->receive();
    }

    public function save($postfix, $message)
    {
        if (empty($this->_model) || empty($postfix))
        {
            throw new CException(__METHOD__ . ' - empty model specified', E_USER_ERROR);
        }
        return $this->_handler->setModel($this->_model)->setPostfix($postfix)->send($message);
    }
}