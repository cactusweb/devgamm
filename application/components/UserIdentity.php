<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    public $userName;
    public $password;
    public $reports;
    public $apps;

    const ERROR_ACCOUNT_DISABLED = 3;

    private $_id;

    public function __construct($userName = '', $password = '')
    {
        $this->userName = $userName;
        $this->password = $password;
    }

    /**
     * Authenticates a user with User model.
     */
    public function authenticate()
    {
        if (isset($this->_getPermanentUserAccessList()[$this->userName]))
        {
            if ($this->_getPermanentUserAccessList()[$this->userName] !== $this->password)
            {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            else
            {
                $this->_id = $this->userName;
                $this->errorCode = self::ERROR_NONE;
            }
        }
        else
        {
            $admin = Yii::app()->Core->getAdmin($this->userName);
            if (empty($admin) || !isset($admin['p']) || !isset($admin['r']) || !isset($admin['a']))
            {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            }
            elseif ($admin['p'] !== self::getPassword($this->password))
            {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            elseif (empty($admin['s']))
            {
                $this->errorCode = self::ERROR_ACCOUNT_DISABLED;
            }
            else
            {
                $this->_id = $this->userName;
                $this->errorCode = self::ERROR_NONE;
                $this->reports = json_encode($admin['r']);
                $this->apps = json_encode($admin['a']);
            }
        }

        return !$this->errorCode;
    }

    public function getErrorMessage()
    {
        if (empty($this->errorCode))
        {
            return '';
        }
        switch ($this->errorCode)
        {
            case 1:
            case 2:
                return 'Incorrect username or password';
                break;
            case 3:
                return 'Username is disabled';
                break;
            default:
                return 'Unknown identity';
                break;
        }
        return '';
    }

    public function getId()
    {
        return $this->_id;
    }

    public static function getPassword($password)
    {
        return md5(sha1($password));
    }

    private function _getPermanentUserAccessList()
    {
        $fileName = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'consoleAccess.php';
        if (!file_exists($fileName))
        {
            return array();
        }
        return require $fileName;
    }
}