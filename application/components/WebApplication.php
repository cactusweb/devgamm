<?php

class WebApplication extends CWebApplication
{
    public $conf = 'm14';
    public $lang = 'en';

    public function beforeControllerAction($controller, $action)
    {
        return parent::beforeControllerAction($controller, $action);
    }

    public function __construct($config = null)
    {
        Yii::setApplication($this);

        $this->preinit();
        $this->initSystemHandlers();
        $this->registerCoreComponents();
        $this->_preconfigure();
        if (is_string($config))
        {
            $config = require($config);
        }
        if (isset($config['basePath']))
        {
            $this->setBasePath($config['basePath']);
            unset($config['basePath']);
        }
        else
        {
            $this->setBasePath('protected');
        }
        Yii::setPathOfAlias('application', $this->getBasePath());
        Yii::setPathOfAlias('webroot', dirname($_SERVER['SCRIPT_FILENAME']));
        Yii::setPathOfAlias('ext', $this->getBasePath().  DIRECTORY_SEPARATOR . 'extensions');
        $this->configure($config);
        $this->attachBehaviors($this->behaviors);
        $this->preloadComponents();
        $this->init();
    }

    protected function _preconfigure()
    {
        if (!empty($_REQUEST['_conference']))
        {
            Yii::app()->session->add('_conference', $_REQUEST['_conference']);
            $this->conf = $_REQUEST['_conference'];
        }
        elseif ($val = Yii::app()->session->get('_conference', false))
        {
            $this->conf = $val;
        }
        return true;
    }

    public function init()
    {
        if (!empty($_REQUEST['lang']) && in_array($_REQUEST['lang'], array('en', 'ru')))
        {
            Yii::app()->session->add('_lang', $_REQUEST['lang']);
            Yii::app()->setLanguage($_REQUEST['lang']);
        }
        elseif ($val = Yii::app()->session->get('_lang', false))
        {
            Yii::app()->setLanguage($val);
        }
    }

    public function getClientScript()
    {
        if (isset(Yii::app()->controller->module->id))
        {
            if (Yii::app()->controller->module->hasComponent('clientScript'))
            {
                return Yii::app()->controller->module->getComponent('clientScript');
            }
        }
        return $this->getComponent('clientScript');
    }
}