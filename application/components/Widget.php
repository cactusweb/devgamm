<?php

class Widget extends CWidget
{
    public function getViewFile($viewName)
    {
        return parent::getViewFile('application.views.widgets.' . $viewName);
    }

    public function run()
    {
        return $this->render(get_class($this));
    }
}