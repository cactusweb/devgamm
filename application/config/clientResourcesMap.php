<?php
return array(
    'bootstrap3' => array(
        'basePath' => 'application.assets.awards.vendor.bootstrap.dist.*',
        'js' => array(
            'js/bootstrap.js',
        ),
        'css' => array(
            'css/bootstrap.css',
            //'css/bootstrap-theme.css',
        ),
    ),

    'ckeditor' => array(
        'basePath' => 'application.assets.ckeditor.*',
        'js' => array(
            'ckeditor.js',
        ),
        'css' => array(

        ),
    ),

    'bootstrap' => array(
        'basePath' => 'application.assets.bootstrap.*',
        'js' => array(
            'js/bootstrap.js',
        ),
        'css' => array(
            'css/bootstrap.css',
        ),
    ),

    'fancybox' => array(
        'basePath' => 'application.assets.fancybox.*',
        'js' => array(
            'fancybox.js',
        ),
        'css' => array(
            'fancybox.css',
        ),
    ),

    'angular' => array(
        'basePath' => 'application.assets.awards.vendor.angular.*',
        'js' => array(
            'angular.js',
        ),
        'css' => array(),
    ),

    'angular-animate' => array(
        'basePath' => 'application.assets.awards.vendor.angular-animate.*',
        'js' => array(
            'angular-animate.js',
        ),
        'css' => array(),
    ),

    'angular-resource' => array(
        'basePath' => 'application.assets.awards.vendor.angular-resource.*',
        'js' => array(
            'angular-resource.js',
        ),
        'css' => array(),
    ),

    'angular-ui-router' => array(
        'basePath' => 'application.assets.awards.vendor.angular-ui-router.release.*',
        'js' => array(
            'angular-ui-router.js',
        ),
        'css' => array(),
    ),

    'ng-file' => array(
        'basePath' => 'application.assets.awards.vendor.ng-file-upload-shim.*',
        'js' => array(
            'angular-file-upload.js',
        ),
        'css' => array(),
    ),

    'ngstorage' => array(
        'basePath' => 'application.assets.awards.vendor.ngstorage.*',
        'js' => array(
            'ngStorage.js',
        ),
        'css' => array(),
    ),

    'angular-sanitize' => array(
        'basePath' => 'application.assets.awards.vendor.angular-sanitize.*',
        'js' => array(
            'angular-sanitize.js',
        ),
        'css' => array(),
    ),

    'jquery213' => array(
        'basePath' => 'application.assets.awards.vendor.jquery.dist.*',
        'js' => array(
            'jquery.js',
        ),
        'css' => array(),
    ),

    'isotope' => array(
        'basePath' => 'application.assets.awards.vendor.isotope.dist.*',
        'js' => array(
            'isotope.pkgd.js',
        ),
        'css' => array(),
    ),

    'awards' => array(
        'basePath' => 'application.assets.awards.src.*',
        'js' => array(
            'js/common.js',
            'js/init.js',
            'js/step-1.ctrl.js',
            'js/step-2.ctrl.js',
            'js/step-3.ctrl.js',
            'js/step-4.ctrl.js',
            'js/step-5.ctrl.js',
            'js/step-6.ctrl.js',
            'js/success.ctrl.js',
            'js/edit-game.ctrl.js',
            'js/participants.js',
            'js/script.js'
        ),
        'css' => array(
            'css/style.css',
            'css/bootstrap-theme.css',
        ),
        'depends' => array(
            'jquery213',
            'bootstrap3',
            'isotope',
            'angular',
            'angular-animate',
            'angular-resource',
            'angular-sanitize',
            'angular-ui-router',
            'ngstorage',
            'ng-file'
        ),
    ),

);