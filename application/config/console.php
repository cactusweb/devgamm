<?php

$basePath = dirname(dirname(__FILE__));
$configPath = dirname(__FILE__);

return array(
    'basePath' => $basePath,

    'preload' => array('log'),

    'runtimePath' => $basePath . '/../runtime/local',

    'import' => array(
        'application.components.*',
        'application.vendors.MongoDb.*',
        'application.vendors.RabbitMQ.*',
    ),
    'components' => array(
        'errorHandler' => array(
            'errorAction' => 'api/error',
        ),
        'cache' => array(
//            'class' => 'system.caching.CApcCache',
            'class' => 'system.caching.CDummyCache',
        ),
        'MongoDb' => array(
            'class' => 'MongoDbConnection',
            'config' => array(),
        ),
        'RabbitMQ' => array(
            'class' => 'RabbitMQConnection',
            'config' => array(),
        ),
        'QueueManager' => array(
            'class' => 'application.components.QueueManager',
        ),
        'Rgc' => array(
            'class' => 'application.components.Rgc',
        ),
        'Rule' => array(
            'class' => 'application.components.RuleHandler',
        ),
        'Core' => array(
            'class' => 'application.components.Core',
            /**
             * TODO: Deploy this to mongo storage
             */
            'events' => require_once $configPath . DIRECTORY_SEPARATOR . 'events.php',
            'apps' => require_once $configPath . DIRECTORY_SEPARATOR . 'apps.php',
        ),
        'Analytics' => array(
            'class' => 'application.components.Analytics',
        ),
        'IpDetection' => array(
            'class' => 'application.vendors.IpDetection.IpDetection',
        ),
        'Import' => array(
            'class' => 'application.components.Import',
        ),
       //'eMurka' => array(
       //    'class' => 'application.extensions.murka.EMurka',
       //    'config' => array(
       //        'db' => array(),
       //    ),
       //),
        'eAnalytics' => array(
            'class' => 'application.extensions.analytics.EAnalytics',
        ),
        'eOpenX' => array(
            'class' => 'application.extensions.openX.EOpenX',
        ),
        'log' => array(
            'class' => 'CLogRouter',
        ),
    ),
);
