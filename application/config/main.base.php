<?php

$basePath = dirname(dirname(__FILE__));
$configPath = dirname(__FILE__);

if (!defined('CONFIG_PATH'))
{
    define('CONFIG_PATH', $configPath);
}

return array(
    'basePath' => $basePath,
    'params'=> array(
        'htdocsPath' => dirname($basePath).DIRECTORY_SEPARATOR.'htdocs',
        'confs' => array(
            'm14' => 'moscow2014',
            'i14' => 'minsk2014',
            'm15' => 'moscow2015',
            'h15' => 'hamburg2015',
            'i15' => 'minsk2015'
        )
    ),
    'defaultController' => 'Console',
    'charset' => 'utf-8',
    'name'  => 'DevGAMM Console',
    'sourceLanguage' => 'en_us',
    'language' => 'en_us',

    'preload' => array('log'),

    'import' => array(
        'application.controllers.*',
        'application.components.*',
        'application.models.*',
        'application.vendors.MongoDb.*',
        'application.vendors.RabbitMQ.*',
    ),
    'components' => array(
        'message' => array(
            'basePath'=>'application.messages.*',
        ),
        'messages' => array(
            'onMissingTranslation' => array('MissingTranslation','handler'),
        ),

        'urlManager' => array(
            'class' => 'CUrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                'show-games' => 'game/index',
                'add-game' => 'game/show',
                'play-game/<id:\d+>' => 'game/play',
                '<action:\w+>' => 'console/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'errorHandler' => array(
            'class' => 'ErrorHandler',
            'errorAction' => 'game/error',
            'adminInfo' => '<a href="mailto:fbportal@renatus.com">support</a>',
        ),
        'cache' => array(
            'class' => 'system.caching.CApcCache',
        ),
        'IpDetection' => array(
            'class' => 'application.vendors.IpDetection.IpDetection',
        ),
        'Core' => array(
            'class' => 'application.components.Core',
        ),
        'eCore' => array(
            'class' => 'application.extensions.core.ECore',
        ),
        'clientScript' => array(
            'class' => 'application.components.ClientScript',
            'combineCss' => true,
            'compressCss' => false, // true
            'combineJs' => true,
            'compressJs' => true,
            'packages' => require_once $configPath . DIRECTORY_SEPARATOR . 'clientResourcesMap.php',
            'excludeFiles' => array(
                'ckeditor.js' => 'ckeditor.js',
                'jquery-ui.min.js' => 'jquery-ui.min.js',
                'jquery.jcarousel.min.js' =>'jquery.jcarousel.min.js',
            ),
            'excludeCssFiles' => array(
                'jquery-ui.css' => 'jquery-ui.css',
                'bootstrap.css' => 'bootstrap.css',
                'fancybox.css' => 'fancybox.css',
            ),
        ),
        'log' => array(
            'class' => 'CLogRouter',
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'autoRenewCookie' => true,
            'authTimeout' => 3600,
            'loginUrl' => 'login',
        )
    ),
);