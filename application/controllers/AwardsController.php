<?php
use application\extensions\Translit\Translit;
/**
 * Created by PhpStorm.
 * User: burre
 * Date: 06.04.15
 * Time: 14:45
 */

class AwardsController extends Controller {
    public $layout = 'awards';

    public function init()
    {
        Yii::app()->request->enableCsrfValidation = false;

        return parent::init();
    }

    /**
     * Это нужно для того, чтобы AssetManager переписывал копии скриптов.
     * @param CAction $action
     * @return bool
     */
    protected function beforeAction($action){
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
            return;
        }
        if(defined('YII_DEBUG') && YII_DEBUG){
            Yii::app()->assetManager->forceCopy = true;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex() {

        $criteria = new CDbCriteria();
        $criteria->condition = 't.ticket_id IS NOT NULL';
        if(Yii::app()->user->isGuest){
            $criteria->addCondition('t.approved = 1');
        }
        $criteria->order = "id ASC";


        $dataProvider=new CActiveDataProvider('Game', array(
            'criteria'=>$criteria,
            'pagination'=>false,
        ));

        $this->render('index', array('dataProvider'=>$dataProvider));
    }

    public function actionGameview($id) {
        $game = Game::model()->findByPk($id);
        if($game){
            $settings = Settings::model()->findByPk(19);
            $pitch_match_url = ($settings)? $settings->value : '';

            $partner = Partner::model()->find('email=:email', array(':email'=>$game->email));
            $this->render('game', array('game'=>$game, 'partner'=>$partner, 'pitch_match_url'=>$pitch_match_url));
        }
    }

    public function actionGetConfParam() {
        echo Yii::app()->conf;
    }

    public function actionRegister() {
        $this->render('register');
    }

    public function actionFile() {
        if($_FILES && file_exists($_FILES['file']['tmp_name']) && $_POST['ticket']){
            $ticket = Ticket::model()->find('code=:code', array(':code'=>$_POST['ticket']));
            if($ticket){
                if($_FILES['file']['type'] != 'image/jpg' && $_FILES['file']['type'] != 'image/jpeg' && $_FILES['file']['type'] != 'image/png'){
                    http_response_code(400);
                    echo json_encode(array('error'=>'Wrong image format, please add JPG or PNG.'));
                    return;
                }

                /**
                 * checking 100x100 icon
                 */
//                if(isset($_POST['type']) && $_POST['type'] == 'icon'){
//                    $fileinfo = getimagesize($_FILES['file']['tmp_name']);
//                    if($fileinfo[0] != $fileinfo[1] || $fileinfo[0] != 100){
//                        echo json_encode($fileinfo);
//                        return;
//                    }
//                }

                if(!is_dir(Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf)){
                    mkdir(Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf);
                    mkdir(Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp');
                    mkdir(Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/ticket');
                }

                $path = Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp/'.$ticket->id;
                if(!is_dir($path)){
                    mkdir($path);
                }

                $fname = Translit::transliterate($_FILES['file']['name']);
                $fpath =  $path.'/'.$fname;
                if(move_uploaded_file($_FILES['file']['tmp_name'], $fpath)){
                    $result = array(
                        "ticket" => $_POST['ticket'],
                        "name" => $fname,
                        "path" => $fpath,
                        "created" => date('Y-m-d h:i:s', time())
                    );
                    echo json_encode($result);
                }else{
                    http_response_code(400);
                    echo json_encode(array('error'=>'Error. Unable to save file.'));
                }
            }else{
                http_response_code(400);
                echo json_encode(array('error'=>'Error. Ticket was not found.'));
            }
        }else{
            http_response_code(400);
            echo json_encode(array('error'=>'Unable to save file.'));
        }
    }

    public function actionCheckTicket(){
        $ticket_code = trim(Yii::app()->request->getParam('ticket'));
        if(!$ticket_code || strlen($ticket_code) != 16){
            http_response_code(400);
            echo json_encode(array('error'=>'wrong_data'));
            return;
        }
        $ticket = Ticket::model()->find('code=:code', array(':code'=>$ticket_code));

        if($ticket){
            $checkGame = Game::model()->find('ticket=:ticket', array(':ticket'=>$ticket->code));
            if($checkGame){
                echo json_encode(array('error'=>'taken', 'game_title'=>$checkGame->title));
                return;
            }

            $result['id'] = $ticket->id;
            $result['code'] = $ticket->code;
            $result['firstname'] = $ticket->firstname? $ticket->firstname : '';
            $result['lastname'] = $ticket->lastname? $ticket->lastname : '';
            $result['email'] = $ticket->email? $ticket->email : '';
            $result['company'] = $ticket->company? $ticket->company : '';
            $result['phone'] = $ticket->phone? $ticket->phone : '';
            $result['website'] = $ticket->website? $ticket->website : '';
            $result['country'] = $ticket->country? $ticket->country : '';
            $result['city'] = $ticket->city? $ticket->city : '';

            echo json_encode($result);
        }else{
            echo json_encode(array('error'=>'wrong_code'));
        }
    }

    public function actionAddGame(){

        $request = json_decode(file_get_contents('php://input'), true);

        #file_put_contents(Yii::app()->params['htdocsPath'].'/game_request.txt', print_r($request, true));

        $ticket = Ticket::model()->find('code=:code', array(':code'=>$request['step1']['ticket']['full']));

        if(!$ticket){
            echo json_encode(array('error'=>'wrong_code'));
            return;
        }

        $checkGame = Game::model()->find('ticket=:ticket', array(':ticket'=>$ticket->code));
        if($checkGame){
           echo json_encode(array('error'=>'taken', 'game_title'=>$checkGame->title));
           return;
        }

        $game = new Game();
        $game->category_id = 0;

        $game->ticket = $request['step1']['ticket']['full'];
        $game->ticket_id = $ticket->id;
        $game->title = $request['step2']['game']['title'];
        $game->description = $request['step2']['game']['description'];

        $game->author = @$request['step3']['dev']['author'];
        $game->author_type = $request['step3']['dev']['type'];

        $game->video = $request['step2']['game']['linkVideo'];

        $game->contact_person = $request['step3']['dev']['name'];
        $game->email = $request['step3']['dev']['email'];
        $game->phone = $request['step3']['dev']['phone'];
        $game->skype = @$request['step3']['dev']['skype'];
        $game->team_size = $request['step3']['dev']['team'];

        $game->lynch = @(int)$request['step5']['lynch']['add'];
        if($game->lynch){
            $game->lynch_drink = (int)$request['step5']['lynch']['drink'];
        }


        $game->gammplay = @(int)$request['step6']['play']['add'];
        if($game->gammplay){
            $game->showcase_device = implode(',', array_keys($request['step6']['play']['device']));
            $showcase_time = array();
            foreach($request['step6']['play']['showcase'] as $showcase){
                if(isset($showcase['checked']) && $showcase['checked']){
                    $showcase_time[] = $showcase['title'];
                }
            }
            $game->showcase_time = implode('|', $showcase_time);
        }

        $game->f2p = (isset($request['step2']['game']['freeToPlay']))? (int)$request['step2']['game']['freeToPlay'] : 0;

        $game->stage = $request['step2']['game']['devStage'];
        $game->location = $request['step3']['dev']['city'].', '.$request['step3']['dev']['country'];

        $links = array();
        $links['company'] = @$request['step3']['dev']['site'];
        $game->link = serialize($links);

        $platforms = array();
        foreach($request['step2']['game']['platform'] as $platform){
            if(isset($platform['checked']) && $platform['checked']){
                $platforms[$platform['id']] = array('id'=>$platform['id'], 'title'=>$platform['title']);
                if($platform['childs']){
                    foreach($platform['childs'] as $subplatform){
                        if(isset($subplatform['checked']) && $subplatform['checked']){
                            $platforms[$platform['id']]['childs'][$subplatform['id']] =array('id'=>$subplatform['id'], 'title'=>$subplatform['title'], 'link'=>$subplatform['link'], 'public_link'=>$subplatform['public_link']);
                        }
                    }
                }
            }
        }
        $game->platforms = serialize($platforms);

        $technologies = array();
        foreach($request['step2']['game']['tech'] as $technology){
            if(isset($technology['checked']) && $technology['checked']){
                $technologies[] = array('id'=>$technology['id'], 'title'=>$technology['title'], 'comment'=>$technology['comment']);
            }
        }
        $game->technology = serialize($technologies);

        $game->premium = (isset($request['step2']['game']['premium']))? (int)$request['step2']['game']['premium'] : 0;
        $game->multiplayer = (isset($request['step2']['game']['multiplayer']))? (int)$request['step2']['game']['multiplayer'] : 0;

        $game->awards = @(int)$request['step4']['awards']['add'];
        $awards_nominations = array();
        if($game->awards){
            foreach($request['step4']['awards']['nominations'] as $nomination){
                if(isset($nomination['checked']) && $nomination['checked']){
                    $awards_nominations[] = $nomination['title'];
                }
            }
        }
        $game->awards_nominations = implode(',', $awards_nominations);

        $screenshoots = array();
        $icon = @$request['step2']['game']['icon']['name'];
        $playLogo = @$request['step6']['play']['logo']['name'];

        $path = Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/ticket/'.$ticket->id;
        if(!is_dir($path)){
            mkdir($path);
        }

        foreach($request['step2']['game']['screenshots'] as $screenshot){
            if($screenshot['name']){
                $fname = Translit::transliterate($screenshot['name']);
                $fpath =  $path.'/'.$fname;
                $fpathTemp = Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp/'.$ticket->id.'/'.$fname;
                if(is_file($fpathTemp) && rename($fpathTemp, $fpath)){
                    $screenshoots[] = '/files/'.Yii::app()->conf.'/ticket/'.$ticket->id.'/'.$fname;
                }
            }
        }

        $game->screenshoots = @implode(',', $screenshoots);

        if($icon){
            $fname = Translit::transliterate($icon);
            $fpath =  $path.'/'.$fname;
            $fpathTemp =  Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp/'.$ticket->id.'/'.$fname;
            if(is_file($fpathTemp) && rename($fpathTemp, $fpath)){
                $game->icon = '/files/'.Yii::app()->conf.'/ticket/'.$ticket->id.'/'.$fname;
            }
        }
        if($playLogo){
            $fname = Translit::transliterate($playLogo);
            $fpath =  $path.'/'.$fname;
            $fpathTemp =  Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp/'.$ticket->id.'/'.$fname;
            if(is_file($fpathTemp) &&  rename($fpathTemp, $fpath)){
                $game->showcase_logo = '/files/'.Yii::app()->conf.'/ticket/'.$ticket->id.'/'.$fname;
            }else{
                $game->showcase_logo = $game->icon;
            }
        }

        $game->token = md5(sha1($ticket->code).sha1(time()));

        if(!$game->save()){
            http_response_code(400);
            echo json_encode($game->errors);
        }else{
            $title = Settings::model()->find(array('condition'=>'name = "game_submited_email_title"'));
            $body = Settings::model()->find(array('condition'=>'name = "game_submited_email_template"'));


            $curl = curl_init();
            $url = 'http://devgamm.com/';
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($curl);
            $base_url = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);


            if($http_status != 200){
                $url = 'http://'.$_SERVER['SERVER_NAME'].'/awards/register#/edit-game?token='.$game->token;
            }else{
                $url = $base_url.'games/?token='.$game->token;
            }

            $body->value .= '<p><a href="'.$url.'">edit my game</a></p>';

            $formData =  $this->renderPartial('email/_game', array('game'=>$game), true);

            $headers  = 'From: DevGAMM ' . Yii::app()->Core->conf . ' <contact@devgamm.com>' . "\r\n";
            $headers .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";
            $headers .= 'Content-type: text/html; charset="utf-8"' . "\r\n\r\n";

            mail('contact@devgamm.com', 'New game for contest! DevGAMM ' . Yii::app()->Core->conf, $formData, $headers);
            mail('contact@renatus.com', 'New game for contest! DevGAMM ' . Yii::app()->Core->conf, $formData, $headers);
            mail(trim($game->email),  strip_tags($title->value), $body->value, $headers);

            echo 'ok';
        }
    }

    public function actionUpdateGame(){

        $request = json_decode(file_get_contents('php://input'), true);

        if(!$request['token']){
            http_response_code(400);
            echo json_encode(array('error'=>'wrong token'));
            return;
        }

        $game = Game::model()->find('token=:token', array(':token'=>$request['token']));

        if(!$game){
            http_response_code(400);
            echo json_encode(array('error'=>'wrong token'));
            return;
        }

        $ticket = Ticket::model()->findByPk($game->ticket_id);

        $game->title = $request['step2']['game']['title'];
        $game->description = $request['step2']['game']['description'];

        $game->author = (isset($request['step3']['dev']['author']))? $request['step3']['dev']['author'] : '';
        $game->author_type = $request['step3']['dev']['type'];

        $game->video = $request['step2']['game']['linkVideo'];

        $game->contact_person = $request['step3']['dev']['name'];
        $game->email = $request['step3']['dev']['email'];
        $game->phone = $request['step3']['dev']['phone'];
        $game->skype = (isset($request['step3']['dev']['skype']))? $request['step3']['dev']['skype'] : '';
        $game->team_size = $request['step3']['dev']['team'];

        $game->f2p = (isset($request['step2']['game']['freeToPlay']))? (int)$request['step2']['game']['freeToPlay'] : 0;

        $game->stage = $request['step2']['game']['devStage'];
        $game->location = $request['step3']['dev']['city'].', '.$request['step3']['dev']['country'];

        $links = array();
        $links['company'] = (isset($request['step3']['dev']['site']))? $request['step3']['dev']['site'] : '';
        $game->link = serialize($links);

        $platforms = array();
        foreach($request['step2']['game']['platform'] as $platform){
            if(isset($platform['checked']) && $platform['checked']){
                $platforms[$platform['id']] = array('id'=>$platform['id'], 'title'=>$platform['title']);
                if($platform['childs']){
                    foreach($platform['childs'] as $subplatform){
                        if(isset($subplatform['checked']) && $subplatform['checked']){
                            $platforms[$platform['id']]['childs'][$subplatform['id']] =array('id'=>$subplatform['id'], 'title'=>$subplatform['title'], 'link'=>$subplatform['link'], 'public_link'=>$subplatform['public_link']);
                        }
                    }
                }
            }
        }
        $game->platforms = serialize($platforms);

        $technologies = array();
        foreach($request['step2']['game']['tech'] as $technology){
            if(isset($technology['checked']) && $technology['checked']){
                $technologies[] = array('id'=>$technology['id'], 'title'=>$technology['title'], 'comment'=>$technology['comment']);
            }
        }
        $game->technology = serialize($technologies);

        $game->premium = (isset($request['step2']['game']['premium']))? (int)$request['step2']['game']['premium'] : 0;
        $game->multiplayer = (isset($request['step2']['game']['multiplayer']))? (int)$request['step2']['game']['multiplayer'] : 0;

        $path = Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/ticket/'.$ticket->id;
        if(!is_dir($path)){
            mkdir($path);
        }

        if(isset($request['step2']['game']['screenshots']) && is_array($request['step2']['game']['screenshots']) && count($request['step2']['game']['screenshots'])){
            $screenshoots = array();
            foreach($request['step2']['game']['screenshots'] as $screenshot){
                if($screenshot['name']){
                    $fname = Translit::transliterate($screenshot['name']);
                    $fpath =  $path.'/'.$fname;
                    $fpathTemp = Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp/'.$ticket->id.'/'.$fname;
                    if(is_file($fpathTemp) && rename($fpathTemp, $fpath)){
                        $screenshoots[] = '/files/'.Yii::app()->conf.'/ticket/'.$ticket->id.'/'.$fname;
                    }
                }
            }

            $screenshootsOld = explode(',', $game->screenshoots);
            foreach($screenshootsOld as $screenshootOld){
                if(!in_array($screenshootOld, $screenshoots)){
                    @unlink(Yii::app()->params['htdocsPath'].$screenshootOld);
                }
            }

            $game->screenshoots = implode(',', $screenshoots);
        }

        $icon = (isset($request['step2']['game']['icon']['name']))? $request['step2']['game']['icon']['name'] : '';
        if($icon){
            $fname = Translit::transliterate($icon);
            $fpath =  $path.'/'.$fname;
            $fpathTemp =  Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp/'.$ticket->id.'/'.$fname;
            if(is_file($fpathTemp) && rename($fpathTemp, $fpath)){
                if($game->icon != '/files/'.Yii::app()->conf.'/ticket/'.$ticket->id.'/'.$fname){
                    @unlink(Yii::app()->params['htdocsPath'].$game->icon);
                }
                $game->icon = '/files/'.Yii::app()->conf.'/ticket/'.$ticket->id.'/'.$fname;
            }
        }

        if(!$game->save()){
            http_response_code(400);
            echo json_encode($game->errors);
        }else{
            echo 'ok';
        }
    }

    public function actionCheckToken(){
        $token= Yii::app()->request->getParam('token');
        if(!$token){
            echo json_encode(array('error'=>404));
            return;
        }

        $game = Game::model()->find(array('condition'=>'token = :token', 'params'=>array(':token'=>$token)));
        if(!$game){
            echo json_encode(array('error'=>404));
            return;
        }

        $deadline = Settings::model()->findByPk(4);
        $deadline_time = strtotime(strip_tags($deadline->value));
        if($deadline_time < time() && Yii::app()->user->isGuest){
            echo json_encode(array('error'=>403));
            return;
        }

        $game->link = unserialize($game->link);
        $game->platforms = unserialize($game->platforms);
        $game->technology = unserialize($game->technology);

        $game->showcase_device = explode(',w', $game->showcase_device);
        $game->showcase_time = explode('|', $game->showcase_time);
        $game->awards_nominations = explode(',', $game->awards_nominations);

        $location = explode(', ', $game->location);
        $game->location = array('country'=> $location[1], 'city'=>$location[0]);

        echo CJSON::encode($game);
    }

    public function actionOldGameParser(){

        if(Yii::app()->user->isGuest){
            $this->redirect(array('console/login'));
        }

        $mCategories = GameCategoryNew::model()->findAll();
        $categories = CHtml::listData($mCategories, 'id', 'title');

        $mTechnolgies = GameTechnology::model()->findAll();
        $technologies = CHtml::listData($mTechnolgies, 'id', 'title');


        $games = Game::model()->findAll(array('condition'=>'t.token = ""'));
        if($games){
            foreach($games as $game){
                $technologiesOld = explode(',', $game->technology);
                $links = unserialize($game->link);
                $platforms = array();
                $platformsOld = unserialize($game->platforms);

                if($platformsOld){
                    foreach($platformsOld as $platformKey => $platformVal){
                        switch($platformKey){
                            case 'mobile':
                                $categoryId = 1;
                                break;
                            case 'console':
                                $categoryId = 2;
                                break;
                            case 'social':
                                $categoryId = 3;
                                break;
                            case 'web':
                                $categoryId = 4;
                                break;
                            case 'desktop':
                                $categoryId = 5;
                                break;
                        }
                        $platforms[$categoryId] = array('id'=>$categoryId, 'title'=>$categories[$categoryId]);
                        if(is_array($platformVal)){
                            foreach($platformVal as $subplatformKey => $subplatformVal){
                                switch($subplatformKey){
                                    case 'iOS':
                                        $subcategoryId = 20;
                                        $linkKey = 'appstore';
                                        break;
                                    case 'android':
                                        $subcategoryId = 21;
                                        $linkKey = 'gplay';
                                        break;
                                    case 'winPhone':
                                        $subcategoryId = 22;
                                        $linkKey = 'winphone';
                                        break;

                                    case 'PS':
                                        $subcategoryId = 23;
                                        $linkKey = 'ps';
                                        break;
                                    case 'Xbox':
                                        $subcategoryId = 24;
                                        $linkKey = 'xbox';
                                        break;
                                    case 'WiiU':
                                        $subcategoryId = 25;
                                        $linkKey = 'wiiu';
                                        break;

                                    case 'facebook':
                                        $subcategoryId = 26;
                                        $linkKey = 'facebook';
                                        break;
                                    case 'vk':
                                        $subcategoryId = 27;
                                        $linkKey = 'vk';
                                        break;
                                    case 'odnoklassniki':
                                        $subcategoryId = 28;
                                        $linkKey = 'odnoklassniki';
                                        break;
                                    case 'moiMir':
                                        $subcategoryId = 29;
                                        $linkKey = 'moimir';
                                        break;

                                    case 'macOS':
                                        $subcategoryId = 30;
                                        $linkKey = 'macos';
                                        break;
                                    case 'linux':
                                        $subcategoryId = 31;
                                        $linkKey = 'linux';
                                        break;
                                    case 'steam':
                                        $subcategoryId = 32;
                                        $linkKey = 'steam';
                                        break;
                                    case 'windows':
                                        $subcategoryId = 34;
                                        $linkKey = 'windows';
                                        break;
                                    default:
                                        continue(2);
                                }
                                $link = (isset($links[$linkKey]))? $links[$linkKey] : '';
                                $platforms[$categoryId]['childs'][$subcategoryId] =array('id'=>$subcategoryId, 'title'=>$categories[$subcategoryId], 'link'=>$link);
                            }
                        }
                    }
                }

                $technologiesNew = array();
                if($technologiesOld){
                    foreach($technologiesOld as $technologyKey){
                        switch($technologyKey){
                            case 'unity':
                                $technologyId = 1;
                                break;
                            case 'unrealEngine':
                                $technologyId = 2;
                                break;
                            case 'cSharp':
                                $technologyId = 3;
                                break;
                            case 'html5':
                                $technologyId = 4;
                                break;
                            case 'java':
                                $technologyId = 5;
                                break;
                            case 'javaScript':
                                $technologyId = 6;
                                break;
                            case 'flashAir':
                                $technologyId = 7;
                                break;
                            case 'marmalade':
                                $technologyId = 8;
                                break;
                            case 'cocos':
                                $technologyId = 9;
                                break;
                            case 'customEngine':
                                $technologyId = 10;
                                break;
                            case 'other':
                                $technologyId = 11;
                                break;
                            default:
                                continue(2);
                        }
                        $technologiesNew[] = array('id'=>$technologyId, 'title'=>$technologies[$technologyId], 'comment'=>'');
                    }
                }
                $ticket = Ticket::model()->findByPk($game->ticket_id);
                $game->token = md5(sha1($ticket->code).sha1(time()));
                $game->link = serialize($links);
                $game->platforms = serialize($platforms);
                $game->technology = serialize($technologiesNew);
                $game->save(false);
            }
        }
    }

    public function actionOldGameTokenGenerate(){

        if(Yii::app()->user->isGuest){
            $this->redirect(array('console/login'));
        }
        $games = Game::model()->findAll(array('condition'=>'t.token = ""'));
        if($games){
            foreach($games as $game){
                $ticket = Ticket::model()->findByPk($game->ticket_id);
                $game->token = md5(sha1($ticket->code).sha1(time()));
                $game->save(false);
            }
        }
    }

    public function actionAddTicket(){
        $serviceIP = array('85.17.210.209', '85.17.210.210');
        $requestIP = @$_SERVER['HTTP_X_REAL_IP'];

        if(in_array($requestIP, $serviceIP)){
            if(isset($_POST['status']) && $_POST['status'] == 'ordered'){
                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/ticket_log.txt', print_r($_POST, true), FILE_APPEND);

                if(isset($_POST['tickets']) && count($_POST['tickets'])){
                    foreach($_POST['tickets'] as $ticketData){
                        if(Ticket::model()->find('code=:code', array(':code'=>$ticketData['id']))){
                            continue;
                        }

                        $ticket = new Ticket();
                        $ticket->event_id = $_POST['event_id'];
                        $ticket->event_title = $_POST['event_title'];

                        $ticket->code = $ticketData['id'];
                        $ticket->firstname = @$ticketData['fields_values']['first_name'];
                        $ticket->lastname = @$ticketData['fields_values']['last_name'];
                        $ticket->email = @$ticketData['fields_values']['email'];
                        $ticket->company = @$ticketData['fields_values']['companyName'];
                        $ticket->post = @$ticketData['fields_values']['position'];
                        $ticket->phone = @$ticketData['fields_values']['phone'];
                        $ticket->website = @$ticketData['fields_values']['site_url'];
                        $ticket->country = @$ticketData['fields_values']['country'];
                        $ticket->city = @$ticketData['fields_values']['city'];

                        if(!$ticket->save()){
                            file_put_contents(Yii::app()->params['htdocsPath'].'/assets/ticket_error_log.txt', print_r($ticket->errors, true), FILE_APPEND);
                            file_put_contents(Yii::app()->params['htdocsPath'].'/assets/ticket_error_log.txt', '=======================================================================', FILE_APPEND);
                        }

                        // add Partner
                        $partnerTicketType = array(
                            'business (early bird)',
                            'business',
                            'business_free',
                            'business + service showcase (early bird)',
                            'business + service showcase'
                        );
                        if(in_array(trim(strtolower($ticketData['ticket_type_name'])), $partnerTicketType)){
                            $partner = new Partner();
                            $partner->email = $ticket->email;
                            $partner->id = md5(sha1($ticket->code));
                            if(Partner::model()->find('id = :id', array(':id'=>$partner->id))){
                                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', print_r(array("partner $partner->id ($partner->email) already in the system"), true), FILE_APPEND);
                                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', print_r($_POST, true), FILE_APPEND);
                                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', '=======================================================================', FILE_APPEND);
                                continue;
                            }

                            $partner_rating = Partner::model()->find(array(
                                'select' => 'MAX(rating) as rating'
                            ));


                            $partner->rating = ($partner_rating)? $partner_rating->rating + 1 : 1;
                            $partner->ticket_id = $ticket->id;
                            $partner->name_en = $ticket->firstname . ' ' . $ticket->lastname;
                            $partner->email = $ticket->email;
                            $partner->company_title = $ticket->company;
                            $partner->position = $ticket->post;
                            $partner->company_url = $ticket->website;
                            $partner->lookingfor_en = @$ticketData['fields_values']['looking_for'];
                            $partner->propose_en = @$ticketData['fields_values']['i_suggest'];
                            $partner->photo = 'user_avatar.gif';

                            if(!$partner->save()){
                                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', print_r($partner->errors, true), FILE_APPEND);
                                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', '=======================================================================', FILE_APPEND);
                            }
                        }else{
                            file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', print_r(array('wrong ticket type: "'.$ticketData['ticket_type_name'].'"'), true), FILE_APPEND);
                            file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', print_r($_POST, true), FILE_APPEND);
                            file_put_contents(Yii::app()->params['htdocsPath'].'/assets/partner_error_log.txt', '=======================================================================', FILE_APPEND);
                        }

                    }
                }
                echo 'ok';
            }else{
                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/error_log.txt', print_r(array('!isset($_POST["status"]) || $_POST["status"] != "ordered"'), true), FILE_APPEND);
                file_put_contents(Yii::app()->params['htdocsPath'].'/assets/error_log.txt', '=======================================================================', FILE_APPEND);
            }
        }

    }

    public function actionClientAuth()
    {
        $request = json_decode(file_get_contents('php://input'), true);
        if(!isset($request['ticket']) || !isset($request['email']) ){
            http_response_code(400);
            return;
        }

        $ticket = Ticket::model()->find('code = :code AND email = :email', array(':code'=>$request['ticket'], ':email'=>$request['email']));
        if(!$ticket){
            http_response_code(404);
            $ticketNoEmail = Ticket::model()->find('code = :code', array(':code'=>$request['ticket']));
            if($ticketNoEmail){
                echo json_encode(array('message'=>'Ticket and email do not match in the system'));
            }else{
                echo json_encode(array('message'=>"This ticket don't exist in the system"));
            }

            return;
        }

        $access_token = md5(sha1($ticket->code).sha1(time()).sha1($ticket->email));

        $ticket->access_token = $access_token;
        $ticket->last_login = date('Y-m-d H:i:s', time());
        $ticket->save(false);

        echo json_encode(array('access_token'=>$access_token));
    }


    public function actionCheckTicketsForPartner()
    {
        $tickets = Ticket::model()->findAll();
        $counter = 0;

        foreach($tickets as $ticket){

            if(!Partner::model()->find('email = :email AND ticket_id = :ticket_id', array(':email'=>$ticket->email, ':ticket_id'=>$ticket->id))){
                $game = Game::model()->find('ticket = :ticket_code', array(':ticket_code'=>$ticket->code));
                if($game){
                    $partner = new Partner();
                    $partner->email = $ticket->email;
                    $partner->id = $partner->generateId();

                    if(Partner::model()->find('id = :id', array(':id'=>$partner->id)))
                        continue;

                    $partner->ticket_id = $game->ticket_id;
                    $partner->name_en = $game->author;
                    $partner->company_title = $game->author;
                    $partner->position = '';
                    $partner->company_url = '';
                    $partner->lookingfor_en ='';
                    $partner->propose_en = '';
                    $partner->photo = 'user_avatar.gif';
                    if(!$partner->save()){
                        echo json_encode($partner->errors);
                        continue;
                    }
                    $counter++;
                }else{
                    echo 'no game with ticket '.$ticket->code.'<br/>';
                }
            }else{
                echo 'Partner with email '.$ticket->email.' and ticket_id '.$ticket->id.' isset<br/>';
            }
        }

        echo $counter.' Partners was created';
    }
}