<?php

Yii::import('application.widgets.Participant');

class ConsoleController extends Controller
{
    /**
     * _isJustReturnValue
     * returns value flag, if true getstats actions returns data, otherwise echo data and exit
     *
     * @var bool
     * @access private
     */
    private $_isJustReturnValue = false;

    /**
     * _isEchoCSV
     * returns data flag, if true - returns data as csv file
     *
     * @var bool
     * @access private
     */
    private $_isEchoCSV = false;

    /**
     * _csvArray
     * csv data container
     *
     * @var array
     * @access private
     */
    private $_csvArray = array();

    private $_userReports = array();
    private $_userCharttypes = array();

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('login', 'error'),
                'users' => array('*'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    /**
     * actionIndex
     * default action
     * @access public
     * @return void
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * actionLogin
     * action provide login form and login user whether he has been authenticated
     * @access public
     * @return void
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        $model = new LoginForm();

        if (isset($_POST['LoginForm']))
        {
            $model->attributes = $_POST['LoginForm'];

            if ($model->validate() && $model->login())
            {
                $this->redirect('/');
            }
        }

        return $this->render('login', array(
            'model' => $model,
        ));
    }

    /**
     * actionLogout
     * log out user from the system
     * @access public
     * @return void
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect('login');
    }

    public function actionPartners($action = null)
    {
        $this->_processParticipantAction('partners', 'Partner', $action);
    }

    public function actionExperts($action = null)
    {
        $this->_processParticipantAction('experts', 'Expert', $action);
    }

    public function actionSponsors($action = null)
    {
        $this->_processParticipantAction('sponsors', 'Sponsor', $action);
    }

    public function actionSpeakers($action = null)
    {
        $this->_processParticipantAction('speakers', 'Speaker', $action);
    }

    public function actionParticipants($action = null)
    {
        $this->_processParticipantAction('participants', 'Participant', $action);
    }

    public function actionDiscussionpanels($action = null)
    {
        $this->_processParticipantAction('discussionPanels', 'DiscussionPanel', $action);
    }

    public function actionApprove($id, $approve)
    {
        if (empty($id))
        {
            throw new CHttpException('Incorrect id specified');
        }
        if (Yii::app()->Core->approveGame($id, isset($_GET['approve']) && $_GET['approve'] ? 1 : 0))
        {
            echo 1;
        }
        Yii::app()->end();
    }

    public function actionGames($action = null, $type = null)
    {
        if (empty($action))
        {
            $action = 'list';
        }
        switch ($action)
        {
            case 'list':
                $this->render('games', array('type' => $type, 'data' => Yii::app()->Core->getGamesForConsole(array($type => 1))));
                break;
            case 'save':
                if (empty($_POST) || empty($type))
                {
                    throw new CHttpException('Empty data for save');
                }
                if ('games' == $type && empty($_POST['id']))
                {
                    throw new CHttpException('Can\'t update game without id');
                }
                $id = null;
                if (isset($_POST['id']))
                {
                    $id = $_POST['id'];
                    unset($_POST['id']);
                }
                try
                {
                    switch ($type)
                    {
                        case 'games':
                            Yii::app()->Core->updateGame($id, $_POST);
                            break;
                        case 'categories':
                            Yii::app()->Core->saveGameCategory($_POST, $id);
                            break;
                        default:
                            throw new CHttpException('Unknown type: {' . $type . '}');
                            break;
                    }
                }
                catch (Exception $e)
                {
                    echo 'Some problem with saving data: ' . $e->getMessage();
                    Yii::app()->end();
                }
                break;
            case 'approve':
                if (empty($_POST['id']) || !isset($_POST['is_approved']))
                {
                    throw new CHttpException('Empty data for approve');
                }
                Yii::app()->Core->approveGame($_POST['id'], $_POST['is_approve']);
                break;
            case 'up':
            case 'down':
                if (empty($_POST['id']))
                {
                    throw new CHttpException('Incorrect data for updating rating');
                }
                try
                {
                    echo Yii::app()->Core->updateRating('Game', $_POST['id'], $action);
                }
                catch (Exception $e)
                {
                    echo 0;
                }
                break;
            case 'remove':
                if (empty($_POST['id']))
                {
                    throw new CHttpException('Incorrect data for delete');
                }
                try
                {
                    Yii::app()->Core->deleteGame($_POST['id']);
                }
                catch (Exception $e)
                {
                    echo $e->getMessage();
                    Yii::app()->end();
                }
                break;
            case 'getCSV':
                Yii::app()->Core->getCSV('Game');
                break;
            default:
                throw new CHttpException('Incorrect action specified');
                break;
        }
    }

    protected function _processParticipantAction($type, $model, $action)
    {
        switch ($action)
        {
            case 'list':
                $this->render('participants', array('type' => $type, 'data' => Yii::app()->Core->getParticipantsForConsole($model)));
                break;
            case 'save':
            case 'save_speaker_category':
            case 'save_discussion_category':
                if (empty($_POST))
                {
                    throw new CHttpException('Empty data for save');
                }
                $id = null;
                if (isset($_POST['id']))
                {
                    $id = $_POST['id'];
                    unset($_POST['id']);
                }
                try
                {
                    switch ($action)
                    {
                        case 'save':
                            Yii::app()->Core->saveParticipant($model, $_POST, $id);
                            break;
                        case 'save_speaker_category':
                        case 'save_discussion_category':
                            Yii::app()->Core->saveCategory('save_speaker_category' == $action ? 'SpeakerCategory' : 'DiscussionCategory', $_POST, $id);
                            break;
                    }
                }
                catch (Exception $e)
                {
                    echo 'Some problem with saving data: ' . $e->getMessage();
                    Yii::app()->end();
                }
                break;
            case 'up':
            case 'down':
                if (empty($_POST['id']))
                {
                    throw new CHttpException('Incorrect data for updating rating');
                }
                try
                {
                    echo Yii::app()->Core->updateRating($model, $_POST['id'], $action);
                }
                catch (Exception $e)
                {
                    echo 0;
                }
                break;
            case 'remove':
                if (empty($_POST['id']))
                {
                    throw new CHttpException('Empty data for delete');
                }
                if ('speakers' == $type)
                {
                    if (isset($_POST['participant']))
                    {
                        try
                        {
                            Yii::app()->Core->deleteParticipant($model, $_POST['id']);
                        }
                        catch (Exception $e)
                        {
                            echo $e->getMessage();
                            Yii::app()->end();
                        }
                    }
                    else
                    {
                        $data = Yii::app()->Core->getParticipants('Speaker', array('category_id' => $_POST['id']));
                        if (empty($data))
                        {
                            Yii::app()->Core->deleteCategory('SpeakerCategory', $_POST['id']);
                        }
                        else
                        {
                            echo 'You can\'t delete category where speakers are present!';
                            Yii::app()->end();
                        }
                    }
                }
                else
                {
                    try
                    {
                        Yii::app()->Core->deleteParticipant($model, $_POST['id']);
                    }
                    catch (Exception $e)
                    {
                        echo $e->getMessage();
                        Yii::app()->end();
                    }
                }
                break;
            case 'conversation':
                $this->render('conversation', array(
                    'data' => Yii::app()->Core->getConversation($model),
                    'type' => $type,
                ));
                break;
            case 'categories':
                switch ($type)
                {
                    case 'speakers':
                        $this->render('speaker_category', array(
                            'data' => Yii::app()->Core->getCategories(true),
                        ));
                        break;
                    case 'discussionPanels':
                        $this->render('discussion_category', array(
                            'data' => Yii::app()->Core->getCategories(true, 'DiscussionCategory'),
                        ));
                        break;
                    default:
                        throw new CHttpException('Incorrect URL specified');
                        break;
                }
                break;
            default:
                throw new CHttpException('Incorrect action specified');
                break;
        }
    }

    protected function _getAccessActions()
    {
        $result = array(
            'admin' => array(),
            'awards' => array(),
            'view' => array(
                'index' => 1,
                'getstats' => 1,
                'getapptoken' => 1,
                'stats' => 1,
            ),
        );
        if (!Yii::app()->user->id)
        {
            return $result;
        }
        if (!isset($result[Yii::app()->user->id]))
        {
            $this->_userReports = empty(Yii::app()->user->reports) ? 1 : json_decode(Yii::app()->user->reports);
            if (!empty($this->_userReports))
            {
                foreach ($this->_userReports as $r)
                {
                    if ('orders' == $r)
                    {
                        $this->_userCharttypes[] = 'orders_count';
                        $this->_userCharttypes[] = 'orders_amount';
                    }
                    elseif (isset($this->_getChartTypesTitles()[$r]))
                    {
                        $this->_userCharttypes[] = $r;
                    }
                }
            }
            $result[Yii::app()->user->id] = array(
                'index' => 1,
                'stats' => !empty($this->_userReports) && is_array($this->_userReports) ? array('type' => $this->_userReports) : 1,
                'getstats' => !empty($this->_userReports) && is_array($this->_userReports) ? array('type' => $this->_userReports) : 1,
                'getapptoken' => 1,
            );
            if (!empty($this->_userCharttypes))
            {
                $result[Yii::app()->user->id]['charts'] = 1;
                $result[Yii::app()->user->id]['getchart'] = !empty($this->_userCharttypes) ? array('type' => $this->_userCharttypes) : 1;
            }
        }
        return $result;
    }

    protected function beforeAction($action)
    {
        if ('admin' == Yii::app()->user->id)
        {
            return true;
        }

        if (!$this->isUserHasAccess())
        {
            $this->_denyUrl('/');
        }
        return true;
    }

    private function _end($message)
    {
        if (!$this->_isJustReturnValue)
        {
            echo (string)$message;
            Yii::app()->end();
        }
        return (string)$message;
    }

    private function _passVariableToJs($param, $data)
    {
        Yii::app()->clientScript->registerScript($param, 'var ' . $param . ' = eval(' . CJavaScript::jsonEncode($data) . ')', CClientScript::POS_HEAD);
    }

    private function _echoCSV($type)
    {
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$type}_console_renatus_com.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handler = fopen("php://output", 'w');
        if (empty($this->_csvArray))
        {
            fclose($handler);
            Yii::app()->end();
        }
        foreach ($this->_csvArray as $row)
        {
            fputcsv($handler, $row);
        }
        fclose($handler);
        Yii::app()->end();
    }

    private function _filterArray(&$value)
    {
        if (empty($value) && '0' !== $value)
        {
            return false;
        }
        if (is_array($value))
        {
            $value = array_filter($value, array($this, '_filterArray'));
            if (empty($value) && '0' !== $value)
            {
                return false;
            }
        }
        return true;
    }
}