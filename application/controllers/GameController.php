<?php

Yii::import('application.widgets.Participant');

class GameController extends Controller
{
    public $layout = 'game';

    public function actionShow()
    {
        $this->render('form');
    }

    public function actionIndex()
    {
        $this->render('games', array(
            'data' => Yii::app()->Core->getGames(),
        ));
    }

    public function actionError()
    {
        if (!($error = Yii::app()->errorHandler->error))
        {
            throw new CHttpException(404, 'Invalid request');
        }
        $this->renderPartial('http_error', array('data'=>$error));
    }

    public function actionPlay($id)
    {
        if (empty($id))
        {
            throw new CHttpException('Page not found');
        }
        $game = Yii::app()->Core->getFlashGameDimension($id);
        if (empty($game))
        {
            throw new CHttpException('Page not found');
        }
        echo '<object width="' . $game['x'] . '" height="' . $game['y'] . '" style="visibility: visible;" id="flashContent" data="' . Yii::app()->Core->devgammHost . '/' . Yii::app()->Core->confDir . '/contest/contestGame_' . $id . '.swf" type="application/x-shockwave-flash"><param value="sameDomain" name="allowscriptaccess"><param value="all" name="allownetworking"></object>';
        Yii::app()->end();
    }

    public function actionSave()
    {
        $formtypes = array('flash', 'social', 'mobile');
        $errors = array();
        /*
         * ported peace of ...
         */
        if (!isset($_POST['category_id'])) {
            $this->_end('form');
        }
            foreach ($_POST as $k => $v) {
                if (!is_array($v)) {
                  $fdata[$k] = stripcslashes($v);
                } else {
                  foreach ($v as $key => $value)
                    $fdata[$k][$key] = stripcslashes($value);
                }
            }
    if ( !isset($fdata['title']) || $fdata['title'] == '' ) $errors['title'] = 'Egamename';
    if ( !isset($fdata['description']) || $fdata['description'] == '' ) $errors['description'] = 'Egamedsc';
    else if (mb_strlen($fdata['description']) > 1000 ) $errors['description'] = 'Egamedscln';
    if ($fdata['category_id'] == 1) {
      if (!isset($fdata['dimension']) || $fdata['dimension'] == '') $errors['dimension'] = 'Egamedim';
      else {
        $fdata['dimension']=str_replace('х', 'x', $fdata['dimension']);
        $fdata['dimension']=str_replace('Х', 'x', $fdata['dimension']);
        if ( !preg_match ("/^[0-9]+[xX][0-9]+$/", trim ($fdata['dimension']) ) ) $errors['dimension'] = 'Egamedimf';
      }
    }
    if ( !isset($fdata['video']) || $fdata['video'] == '' ) { $errors['video'] = 'Egamevideo'; }
    if ( !isset($fdata['author']) || $fdata['author'] == '' ) { $errors['author'] = 'Egameauthor'; }
    if ( !isset($fdata['email']) || $fdata['email'] == '' ) { $errors['email'] = 'Eemail'; }
    else {
      if ( !preg_match ("/^[a-zA-Z0-9_\-]+(\.[a-zA-Z0-9_\-]+)*@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/", trim ($fdata['email']) ) ) {
        $errors['email'] = 'Eemailf';
      }
    }
    if (empty($fdata['location'])) {
      $errors['location'] = 'emptyError';
    }
    if (empty($fdata['stage'])) {
      $errors['stage'] = 'emptyError';
    }
    if (4 == $fdata['category_id']) {
      if (empty($fdata['platform'])) {
          $errors['platform'] = 'emptyError';
      }
      if (empty($fdata['link']) || !is_array($fdata['link'])) {
        $errors['link'] = 'emptyError';
      }
      $linkEmpty = true;
      foreach ($fdata['link'] as $v) {
        if (!empty($v)) {
          $linkEmpty = false;
          break;
        }
      }
      if ($linkEmpty) {
        $errors['link'] = 'emptyError';
      }
      if (empty($fdata['f2p'])) {
        $errors['f2p'] = 'emptyError';
      }
    }
    else
    {
        if (!empty($fdata['gameurl']))
        {
            $fdata['link'] = $fdata['gameurl'];
        }
        unset($fdata['gameurl']);
    }
    if ( !isset($fdata['phone']) || $fdata['phone'] == '' ) { $errors['phone'] = 'Ephone'; }
    if ($_FILES['icon_ext']['name'] == '') {
        $errors['icon_ext'] = 'Egameicon';
    }
    else {
      $itypes = array ('image/png', 'image/jpeg', 'image/png');
      if ( !in_array( $_FILES['icon_ext']['type'], $itypes ) )
      $errors['icon_ext'] = 'Egameicontype';
      $size=getimagesize($_FILES['icon_ext']['tmp_name']);
      $w=(int)$size[0];
      $h=(int)$size[1];
      if ($w != 100 || $h != 100) $errors['icon_ext'] = 'Egameicondim';
    }
    unset($fdata['send']);
    if (strlen($_FILES['file_ext']['name']) > 0) {
      $gext = array('swf','zip','rar');
      $fext = explode('.',$_FILES['file_ext']['name']);
      $fext = $fext[count($fext)-1];
      if (!in_array($fext,$gext)) { $errors['file_ext'] = 'Egamefiletype'; }
    }
    if (!isset($fdata['ticket']))
    {
        $errors['ticket'] = 'EticketEmpty';
    }
    else
    {
        if (empty($fdata['ticket']) || !is_array($fdata['ticket']))
        {
            $errors['ticket'] = 'EticketIncorrect';
        }
        $ticketChunks = $fdata['ticket'];
        $fdata['ticket'] = '';
        foreach ($ticketChunks as $codeChunk)
        {
            $fdata['ticket'] .= $codeChunk;
        }
        if (!is_numeric($fdata['ticket']) || strlen($fdata['ticket']) != 16)
        {
            $errors['ticket'] = 'EticketIncorrect';
        }
    }
    if (!empty($errors)) {
        $this->_end('form', array('errors' => $errors));
    }
    if (isset($fdata['lynch']) && 'on' == $fdata['lynch'])
    {
        $fdata['lynch'] = 1;
    }
    if (isset($fdata['gammplay']) && 'on' == $fdata['gammplay'])
    {
        $fdata['gammplay'] = 1;
    }
      $fExt = $iExt = '';
      if (isset($_FILES['file_ext']) and $_FILES['file_ext']['tmp_name']) {
        $fExt = explode('.', $_FILES['file_ext']['name']);
        $fExt = $fExt[count($fExt)-1];
        $fdata['file_ext'] = $fExt;
      }
      $iExt = explode('.', $_FILES['icon_ext']['name']);
      $iExt = $iExt[count($iExt)-1];
      $fdata['icon_ext'] = $iExt;
      Yii::app()->cache->flush();
      $recordId = Yii::app()->Core->addGame($fdata);

      if (empty($recordId))
      {
          $this->_end('form', array('errors' => array('global' => 'Eglobal')));
      }
      if (-1 == $recordId)
      {
          $this->_end('form', array('errors' => array('ticket' => 'EticketExists')));
      }

      if (!empty($_FILES['file_ext']['tmp_name']))
      {
          $fname = DEVGAMM_DIR . '/' . Yii::app()->Core->confDir . '/contest/contestGame_' . $recordId . '.' . $fExt;
          if( !move_uploaded_file ($_FILES['file_ext']['tmp_name'],$fname)) { echo "Error uploading file"; exit; }
      }

      $fname = DEVGAMM_DIR . '/' . Yii::app()->Core->confDir . '/contest/contestGameIcon_' . $recordId . '.' . $iExt;
      if (!move_uploaded_file ($_FILES['icon_ext']['tmp_name'],$fname)) { echo "Error uploading file"; exit; }

      $formData = '';
      foreach ($fdata as $title => $value)
      {
        switch ($title)
        {
          case 'category_id':
            $formData .= $title . ' => ' . Yii::app()->Core->gameCategories[$value]['title'] . '<br/>';
            break;
          case 'ticket':
            $formData .= $title . ' => ' . chunk_split($value, 4) . '<br/>';
            break;
          case 'link':
            if (empty($value) || !is_array($value))
            {
              break;
            }
            foreach ($value as $linkType => $link)
            {
                $formData .= $linkType . ' => ' . '<a href="' . $link . '">'. $link .'</a> <br/>';
            }
            break;
          case 'video':
            $formData .= '<a href="' . $value . '">'. $value .'</a> <br/>';
            break;
          default:
            $formData .= $title . ' => ' . $value . '<br/>';
            break;
        }
      }

      $msg= "Добавлена игра в каталог.
              Название игры: ".trim($_REQUEST['title'])."
              Описание игры: ".trim($_REQUEST['description'])."
              Автор игры: ".trim($_REQUEST['author'])."
              ";
      $headers  = 'From: DevGAMM ' . Yii::app()->Core->conf . ' <contact@devgamm.com>' . "\r\n";
      $headers .= 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";
      $headers .= 'Content-type: text/html; charset="utf-8"' . "\r\n\r\n";
      mail('contact@devgamm.com', 'New game for contest! DevGAMM ' . Yii::app()->Core->conf, $formData, $headers);
      mail('contact@renatus.com', 'New game for contest! DevGAMM ' . Yii::app()->Core->conf, $formData, $headers);

      $msgtitle = 'DevGAMM ' . Yii::app()->Core->conf;
      $umessage = 'umsg_' . Yii::app()->Core->confDir;
      mail(trim($fdata['email']), $msgtitle, Yii::t('gameform', $umessage), $headers);

      $this->render('complete', array('mess' => $umessage));
    }

    public function actionIsticketExist($ticket)
    {
         if (Yii::app()->Core->isTicketExist($ticket))
         {
            echo 'This ticket is already exists';
         }
    }

    private function _end($view = '', $params = array())
    {
        if (!empty($view))
        {
            $this->render($view, $params);
        }
        Yii::app()->end();
    }
}