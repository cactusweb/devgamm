<?php

class GameNominationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='main';

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        if(Yii::app()->user->isGuest){
            $this->redirect(array('console/login'));
        }

		$model=new GameNomination;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GameNomination']))
		{
			$model->attributes=$_POST['GameNomination'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        if(Yii::app()->user->isGuest){
            $this->redirect(array('console/login'));
        }

		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GameNomination']))
		{
			$model->attributes=$_POST['GameNomination'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        if(Yii::app()->user->isGuest){
            $this->redirect(array('console/login'));
        }

		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        $technologies = GameNomination::model()->findAll(array('order'=>'t.menu_index ASC, t.id ASC'));

        echo CJSON::encode($technologies);
    }
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        if(Yii::app()->user->isGuest){
            $this->redirect(array('console/login'));
        }

		$model=new GameNomination('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GameNomination']))
			$model->attributes=$_GET['GameNomination'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return GameNomination the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=GameNomination::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param GameNomination $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='game-nomination-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
