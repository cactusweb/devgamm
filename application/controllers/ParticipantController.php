<?php
use application\extensions\Translit\Translit;

Yii::import('application.widgets.Participant');

class ParticipantController extends Controller
{
    public $layout = 'participant';

    public function actionSponsor($partial = null)
    {
        $view = 'sponsor';
        if (isset($_GET['sidebar']))
        {
            $view = 'sponsor_sidebar';
        }
        $this->_renderParticipants($view, 'Sponsor', $partial, array('order' => 'category_id asc, rating asc'));
    }

    public function actionPartner($partial = null)
    {
        Yii::app()->cache->flush();
        $this->_renderParticipants('partner', 'Partner', $partial, array('order' => 'rating asc'));
    }

    public function actionSpeaker($partial = null)
    {
        $this->_renderParticipants('speaker', 'Speaker', $partial);
    }

    public function actionExpertlinch($partial = null)
    {
        $this->_renderParticipants('expert', 'Expert', $partial, array('category_id' => 1));
    }

    public function actionExpertadvicer($partial = null)
    {
        $this->_renderParticipants('expert', 'Expert', $partial, array('category_id' => 2));
    }

    public function actionDiscussionpanel($partial = null)
    {
        $this->_renderParticipants('discussion_panel', 'DiscussionPanel', $partial);
    }

    private function _renderParticipants($view, $model, $partial, $params = array())
    {
        $method = 'render';
        if ($partial)
        {
            $method = 'renderPartial';
        }

        $this->$method($view, array('data' => Yii::app()->Core->getParticipants($model, $params)));
    }


    public function actionPartnerUpdate(){
        $request = json_decode(file_get_contents('php://input'), true);

        if(!$request['token']){
            http_response_code(404);
            echo json_encode(array('error'=>'token is empty'));
            return;
        }

        $ticket = $this->getTicketByToken($request['token']);
        if(!$ticket){
            http_response_code(401);
            echo json_encode(array('error'=>'wrong token'));
            return;
        }

        $partner = Partner::model()->find('ticket_id=:ticket_id', array(':ticket_id'=>$ticket->id));
        if(!$partner){
            http_response_code(404);
            echo json_encode(array('error'=>'partner was not found'));
            return;
        }

        if(isset($request['photo'])){
            $photo = $request['photo'];
            $fname = Translit::transliterate($photo);
            if(!is_dir(DEVGAMM_DIR.Yii::app()->Core->confDir)){
                mkdir(DEVGAMM_DIR.Yii::app()->Core->confDir);
                mkdir(DEVGAMM_DIR.Yii::app()->Core->confDir.'/images');
            }

            $fpathTemp =  Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp/'.$fname;

            $path = DEVGAMM_DIR.Yii::app()->Core->confDir.'/images';
            $fname =  (is_file($path.'/'.$fname))? time().'_'.$fname : $fname;
            $fpath =  $path.'/'.$fname;

            if(is_file($fpathTemp) &&  rename($fpathTemp, $fpath)){
                $partner->photo = $fname;
            }else{
                http_response_code(404);
                echo "can't upload file to live server - $fpathTemp to $fpath";
            }
        }

        $partner->company_url = @$request['company_url'];
        $partner->lookingfor_en = @$request['lookingfor_en'];
        $partner->propose_en = @$request['propose_en'];

        if(!$partner->save()){
            http_response_code(404);
            echo json_encode($partner->errors);
        }else{
            Yii::app()->cache->flush();
            echo json_encode(array(
                'name'=>$partner->name_en,
                'email'=>$partner->email,
                'company_url'=>$partner->company_url,
                'lookingfor_en'=>$partner->lookingfor_en,
                'propose_en'=>$partner->propose_en,
                'photo'=>"//devgamm.com/".Yii::app()->Core->confDir."/images/".$partner->photo
            ));
        }

    }

    public function actionGetPartnerData(){
        $request = $_GET;

        if(!$request['token']){
            http_response_code(401);
            echo json_encode(array('error'=>'token is empty'));
            return;
        }

        $ticket = $this->getTicketByToken($request['token']);
        if(!$ticket){
            http_response_code(401);
            echo json_encode(array('error'=>'wrong token'));
            return;
        }

        $partner = Partner::model()->find('ticket_id=:ticket_id', array(':ticket_id'=>$ticket->id));
        if(!$partner){
            http_response_code(404);
            echo json_encode(array('error'=>'partner was not found'));
            return;
        }

        echo json_encode(array(
            'name'=>$partner->name_en,
            'email'=>$partner->email,
            'company_url'=>$partner->company_url,
            'lookingfor_en'=>$partner->lookingfor_en,
            'propose_en'=>$partner->propose_en,
            'photo'=>"//devgamm.com/".Yii::app()->Core->confDir."/images/".$partner->photo
            ));
    }

    public function actionGetMember(){
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
            return;
        }

        $request = json_decode(file_get_contents('php://input'), true);

        if(!$request['id'] || !$request['type']){
            http_response_code(401);
            echo json_encode(array('error'=>'bad request'));
            return;
        }

        if(!$request['token']){
            http_response_code(401);
            echo json_encode(array('error'=>'token is empty'));
            return;
        }

        $ticket = $this->getTicketByToken($request['token']);
        if(!$ticket){
            http_response_code(401);
            echo json_encode(array('error'=>'wrong token'));
            return;
        }

        switch($request['type']){
            case 'expert':
                $memberClass = 'Expert';
                break;
            case 'partner':
                $memberClass = 'Partner';
                break;
            case 'speaker':
                $memberClass = 'Speaker';
                break;
            case 'sponsor':
                $memberClass = 'Sponsor';
                break;
            default:
                http_response_code(400);
                echo json_encode(array('error'=>'wrong member type'));
                return;
        }

        $member = $memberClass::model()->find('id=:member_id', array(':member_id'=>$request['id']));
        if(!$member){
            echo json_encode(array('error'=>'member was not found'));
            return;
        }

        echo json_encode(array(
            'name'=>(isset($member->name_en))? $member->name_en : $member->title_en,
            'email'=>$member->email
            ));
    }

    public function actionPartnerPhoto() {
        if($_FILES && file_exists($_FILES['file']['tmp_name'])){

            $request = $_POST;

            if(!$request['token']){
                http_response_code(401);
                echo json_encode(array('error'=>'token is empty'));
                return;
            }

            $ticket = $this->getTicketByToken($request['token']);
            if(!$ticket){
                http_response_code(401);
                echo json_encode(array('error'=>'wrong token'));
                return;
            }

            $partner = Partner::model()->find('ticket_id=:ticket_id', array(':ticket_id'=>$ticket->id));
            if(!$partner){
                http_response_code(404);
                echo json_encode(array('error'=>'partner was not found'));
                return;
            }

            if($_FILES['file']['type'] != 'image/jpg' && $_FILES['file']['type'] != 'image/jpeg' && $_FILES['file']['type'] != 'image/png'){
                http_response_code(400);
                echo json_encode(array('error'=>'Wrong image format, please add JPG or PNG.'));
                return;
            }

            $fileinfo = getimagesize($_FILES['file']['tmp_name']);
            if($fileinfo[0] != $fileinfo[1] || $fileinfo[0] != 75){
                http_response_code(400);
                echo json_encode(array('error'=>'Size of photo should be 75x75 pixels.'));
                return;
            }


            if(!is_dir(Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf)){
                mkdir(Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf);
                mkdir(Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp');
            }
            $path = Yii::app()->params['htdocsPath'].'/files/'.Yii::app()->conf.'/temp';
            $fname = Translit::transliterate($_FILES['file']['name']);
            $fpath =  $path.'/'.$fname;
            if(move_uploaded_file($_FILES['file']['tmp_name'], $fpath)){
                $photo_url = 'http://'.$_SERVER['SERVER_NAME'].'/files/'.Yii::app()->conf.'/temp/'.$fname;
                echo json_encode(array('photo_url'=>$photo_url));
            }else{
                http_response_code(400);
                echo json_encode(array('error'=>'Error. Unable to save file.'));
            }

        }else{
            echo json_encode(array('error'=>'Unable to save file.'));
        }
    }

    public function actionSendMessage() {
        if($_SERVER['REQUEST_METHOD'] == 'OPTIONS'){
            return;
        }

        $request = json_decode(file_get_contents('php://input'), true);

        if(!$request['token']){
            http_response_code(401);
            echo json_encode(array('error'=>'token is empty'));
            return;
        }

        $ticket = $this->getTicketByToken($request['token']);
        if(!$ticket){
            http_response_code(401);
            echo json_encode(array('error'=>'wrong token'));
            return;
        }

        $member_type = array(
            'partner' => 1,
            'speaker' => 3,
            'sponsor' => 2,
            'participant' => 0,
        );

        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=utf-8";
        $headers[] = "From: DevGAMM Meeting Request System <admin@devgamm.com>";
        $headers[] = "Reply-To: {$request['data']['name']} <{$request['data']['email']}>";

        if(mail($request['data']['member']['email'], 'DevGAMM: meeting request', $request['data']['body'], implode("\r\n", $headers))){
            $conversation = new Conversation();
            $conversation->participant_id = $request['data']['member']['id'];
            $conversation->author = $request['data']['name'];
            $conversation->author_email = $request['data']['email'];
            $conversation->message = $request['data']['body'];
            $conversation->link = $request['data']['url'];
            $conversation->member_type = $member_type[$request['data']['member']['type']];
            $conversation->save();
        }else{
            http_response_code(400);
            echo print_r(error_get_last());
        }
    }

}