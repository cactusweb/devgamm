<?php

class ShowcaseController extends Controller
{

    public $layout = 'main';

	public function actionUpdate()
	{
        if(Yii::app()->user->isGuest){
            $this->redirect(array('console/login'));
        }

        $model = $this->loadModel();

        $model->schedule = unserialize($model->schedule);

		if(isset($_POST['Showcase']))
		{
            $model->days=$_POST['Showcase']['days'];
            $model->tables=$_POST['Showcase']['tables'];
            $model->timeslots=$_POST['Showcase']['timeslots'];

            $schedule = $this->generateSchedule($model->schedule, $model);
            $model->schedule = serialize($schedule);


			if($model->save()){
                Yii::app()->user->setFlash('msg', 'Your changes have been saved');
                $this->refresh();
            }
		}
        if(isset($_POST['Schedule']))
        {
            $model->schedule = serialize($_POST['Schedule']);
            if($model->save()){
                Yii::app()->user->setFlash('msg', 'Your changes have been saved');
                $this->refresh();
            }
        }

		$this->render('update',array(
			'model'=>$model,
		));
	}


	public function actionIndex()
	{
        $this->layout = 'awards';

        $model = $this->loadModel();
        $model->schedule = unserialize($model->schedule);

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionSchedule()
	{
        $showcase = Showcase::model()->findByPk(2);
        $showcase->schedule = unserialize($showcase->schedule);

        $response = array();
        
        $timeslotsCounter = 1;
        foreach($showcase->schedule as $day_k => $day){
            foreach($day["timeslot"] as $timeslot_k => $timeslot){
                $response[$timeslotsCounter] = array('title'=>$day["date"].', '.$timeslot);
                $timeslotsCounter++;
            }
        }

        echo json_encode($response);
	}

    public function loadModel(){
        $model = Showcase::model()->findByPk(2);

        if(!$model){
            $showcase = new Showcase();
            $showcase->id = 2; //v2
            $showcase->tables = 9;
            $showcase->days = 2;
            $showcase->timeslots = 2;

            $schedule = $this->generateSchedule(array(), $showcase);
            $showcase->schedule = serialize($schedule);

            if($showcase->save()){
                $model = $showcase;
            }else{
                echo '<pre>';
                echo print_r($showcase->errors);
                echo '</pre>';
            }
        }

        return $model;
    }

    public function generateSchedule(array $schedule, Showcase $showcase){
        $timeslotCounter = 1;
        $result = array();
        for($day=1; $day<=$showcase->days; $day++){
            $result["day_{$day}"]["date"] = (isset($schedule["day_{$day}"]["date"]))? $schedule["day_{$day}"]["date"] : "Day {$day} date";
            for($timeslot_header=1; $timeslot_header<=$showcase->timeslots; $timeslot_header++){
                $result["day_{$day}"]["timeslot"][$timeslot_header] =
                    (isset($schedule["day_{$day}"]["timeslot"][$timeslot_header]))?
                        $schedule["day_{$day}"]["timeslot"][$timeslot_header]: "Day {$day} - timeslot {$timeslot_header}";
            }
            for($table=1; $table<=$showcase->tables; $table++){
                $result["day_{$day}"]["table"]["table_{$table}"]['title'] =
                    (isset($schedule["day_{$day}"]["table"]["table_{$table}"]['title']))?
                        $schedule["day_{$day}"]["table"]["table_{$table}"]['title']: "A{$table}";
                for($timeslot=1; $timeslot<=$showcase->timeslots; $timeslot++){
                    $result["day_{$day}"]["table"]["table_{$table}"]["timeslot"]["timeslot_{$timeslot}"] =
                        (isset($schedule["day_{$day}"]["table"]["table_{$table}"]["timeslot"]["timeslot_{$timeslot}"]))?
                            $schedule["day_{$day}"]["table"]["table_{$table}"]["timeslot"]["timeslot_{$timeslot}"] : "free";
                    $timeslotCounter++;
                }
            }
        }
        return $result;
    }

}
