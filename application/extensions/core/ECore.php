<?php

Yii::import('application.extensions.core.models.*');

class ECore extends Extension
{
    public $namespace = 'application\extensions\core\models';

    public function updateRating($id, $direction, $model = 'Partner')
    {
        if (!isset($this->_getAvailableModels()[$model]))
        {
            throw new CException('Incorrect model specified: {' . $model . '}');
        }
        $row = $this->model($model)->findByPk($id);
        if (empty($row))
        {
            throw new CHttpException('Data not found for id: {' . $id . '}');
        }
        $swap = $this->model($model)->getRowByCriteria(array(
            'conditions' => array(
                'where' => 'rating ' . ('down' == $direction ? '<' : '>') . $row->rating,
                'select' => array('id', 'rating'),
                'from' => $this->model($model)->tableName(),
                'order' => 'rating ' . ('down' == $direction ? 'desc' : 'asc'),
                'limit' => 1,
            ),
        ));

        if (empty($swap['id']))
        {
            throw new CHttpException(404, 'Not found');
        }

        $this->model($model)->multiplyInsert(
            array(
                array('rating' => $row->rating, 'id' => $swap['id']),
                array('rating' => $swap['rating'], 'id' => $id),
            ),
            true,
            'rating'
        );
        return 0;
    }

    public function deleteElement($id, $model = 'Partner')
    {
        if (!isset($this->_getAvailableModels()[$model]))
        {
            throw new CException('Incorrect model specified: {' . $model . '}');
        }
        return (bool)$this->model($model)->deleteByPk($id);
    }

    public function getConversation($model)
    {
        $m = $this->model($model)->alias;
        $select = array(
            'id',
            "$m.email as member_email",
            'author',
            'author_email',
            'link',
            'message',
            'date',
        );
        switch ($model)
        {
            case 'Participant':
            case 'Speaker':
            case 'Partner':
            case 'Expert':
                $select[] = "$m.name_en as member";
                break;
            case 'Sponsor':
                $select[] = "$m.title_en as member";
                break;
            default:
               throw new CException('Incorrect model specified: {' . $model . '}');
               break;
        }
        $condition = array(
            'select' => $select,
            'from' => 'Conversation',
            'conditions' => array(
                'Conversation' => array(
                    'member_type' => $this->_getAvailableModels()[$model],
                ),
            ),
            'join' => array(
                array(
                    'type' => 'leftJoin',
                    'model' => $model,
                    'alias' => $m,
                    'conditions' => array(
                        'participant_id' => 'id',
                    ),
                ),
            ),
        );
        return $this->_executeRelatedQuery('getAllByCriteria', $condition);
    }

    public function getParticipants($model, $select = array(), $isGroupByCategory = false, $params = array())
    {
        $categories = array();
        switch ($model)
        {
            case 'Partner':
                if (empty($select))
                {
                    $select = '*';
                }
                break;
            case 'Participant';
                break;
            case 'Expert';
            case 'DiscussionPanel';
                if (empty($select))
                {
                    $select = '*';
                }
                $categories = ('Expert' == $model) ? Yii::app()->Core->expertCategory : Yii::app()->Core->getCategories(false, 'DiscussionCategory');
                if (!empty($params['category_id']))
                {
                    $criteria = array('where' => array('category_id = ' . $params['category_id']));
                }
                break;
            case 'Speaker':
                if (empty($select))
                {
                    $select = '*';
                }
                $categories = Yii::app()->Core->getCategories();
                if (!empty($params['category_id']))
                {
                    $criteria = array('where' => array('category_id = ' . $params['category_id']));
                }
                break;
            case 'Sponsor':
                if (empty($select))
                {
                    $select = '*';
                }
                $categories = Yii::app()->Core->sponsorTypes;
                break;
            default:
                throw new CException('Incorrect model specified: {' . $model . '}');
                break;
        }
        $condition = array(
            'conditions' => array(
                'select' => $select,
                'from' => $this->model($model)->tableName(),
                'order' => isset($params['order']) ? $params['order'] : 'rating desc',
            ),
        );
        if (isset($criteria['where']) && is_array($criteria['where']))
        {
            $condition['conditions']['where'] = '';
            foreach ($criteria['where'] as $criteriaValue)
            {
                $condition['conditions']['where'] .= $criteriaValue;
            }
        }
        $data = $this->model($model)->getAllByCriteria($condition);
        if (empty($data) || !is_array($data) || !$isGroupByCategory || empty($categories))
        {
            return $data;
        }
        $result = array();
        foreach ($data as $k => $row)
        {
            if (!isset($categories[$row['category_id']]))
            {
                throw new CException('Incorrect category specified: {' . $row['category_id'] . '}');
            }
            if ('Speaker' == $model)
            {
                $result[$row['category_id']]['data'][md5($row['topic'])][] = $row;
                $result[$row['category_id']]['category'] = $categories[$row['category_id']]['title'];
                $result[$row['category_id']]['category_color'] = $categories[$row['category_id']]['color'];
            }
            else
            {
                $result[$row['category_id']]['data'][] = $row;
                $result[$row['category_id']]['category'] = $categories[$row['category_id']];
            }
        }
        return $result;
    }

    public function saveParticipant($m, $data, $id = null)
    {
        if (!isset($this->_getAvailableModels()[$m]))
        {
            throw new CException('Incorrect model specified: {' . $m . '}');
        }
        if (empty($id))
        {
            $model = $this->newModel($m);
        }
        else
        {
            $model = $this->model($m)->findByPk($id);
        }
        if (empty($model))
        {
            throw new CException('Can\'t initialize model', E_USER_ERROR);
        }
        $model->attributes = $data;
        if (!$id)
        {
            $model->id = $model->generateId();

            if($model->findByPk($model->id)){
                throw new \CException('Email is already registered');
            }

            $criteria = array(
                'conditions' => array(
                    'select' => 'MAX(rating) as rating',
                    'from' => $this->model($m)->tableNameAsAlias(),
                ),
            );

            $rating = @$this->model($m)->getAllByCriteria($criteria)[0]['rating'];
            $r = ($rating)? $rating : 0;

            $model->rating = $r + 1;
        }
        if (!$model->validate())
        {
            throw new CException('Model can\'t be validated: ' . json_encode($model->getErrors()));
        }
        return $model->save();
    }

    public function saveCategory($m, $data, $id = null)
    {
        if (!in_array($m, array('SpeakerCategory', 'GameCategory', 'DiscussionCategory')))
        {
            throw new CException('Incorrect model specified: {' . $m . '}');
        }
        if (empty($id))
        {
            $model = $this->newModel($m);
        }
        else
        {
            $model = $this->model($m)->findByPk($id);
        }
        if (empty($model))
        {
            throw new CException('Can\'t initialize model');
        }
        $model->attributes = $data;
        if (!$model->validate())
        {
            throw new CException('Model can\'t be validated: ' . json_encode($model->getErrors()));
        }
        return $model->save();
    }

    public function getCategories($select = '*', $model)
    {
        if (empty($model) || !is_string($model))
        {
            throw new CException(__METHOD__ . ' - empty or invalid model', E_USER_ERROR);
        }
        return $this->model($model)->getAllByCriteria(array('conditions' => array('select' => $select, 'from' => $this->model($model)->tableName())));
    }

    public function getGames($conds = array(), $order = 'rating')
    {
        if (!$order || !in_array($order, array('rating', 'date')))
        {
            $order = 'rating';
        }
        $condition = array(
            'conditions' => array(
                'select' => '*',
                'from' => $this->model('Game')->tableNameAsAlias(),
                'order' => $order . ' desc',
            ),
        );
        if (!empty($conds))
        {
            $where = array();
            if (isset($conds['lynch']))
            {
                $where['lynch'] = 1;
            }
            elseif (isset($conds['gammplay']))
            {
                $where['gammplay'] = 1;
            }
            if (isset($conds['approved']))
            {
                $where['approved'] = $conds['approved'];
            }
            if (!empty($where))
            {
                $where = $this->model('Game')->parseWhere($where);
                $condition['conditions']['where'] = $where['where'];
                $condition['params'] = $where['ps'];
            }
        }
        return $this->model('Game')->getAllByCriteria($condition);
    }

    public function isTicketExist($ticket)
    {
        if (empty($ticket) || !is_string($ticket) || strlen($ticket) != 16)
        {
            throw new CException(__METHOD__ . ' - empty or invalid ticket', E_USER_ERROR);
        }

        return $this->model('Game')->findByAttributes(array('ticket' => $ticket));
    }

    public function getGamesCategories($select)
    {
        $conditons = array(
            'conditions' => array(
                'select' => !empty($select) ? $select : '*',
                'from' => $this->model('GameCategory')->tableNameAsAlias(),
                'order' => 'rating desc',
            ),
        );
        return $this->model('GameCategory')->getAllByCriteria($conditons);
    }

    public function saveGame($data, $id = null)
    {
        if (empty($id))
        {
            $model = $this->newModel('Game');
        }
        else
        {
            $model = $this->model('Game')->findByPk($id);
        }
        if (empty($model))
        {
            throw new CException('Can\'t initialize model');
        }
        $model->attributes = $data;
        if (!$model->validate())
        {
            throw new CException('Model can\'t be validated: ' . json_encode($model->getErrors()));
        }
        if (!$id)
        {
            $model->rating = $this->model('Game')->count() + 1;
        }
        try
        {
            return $model->save() ? $model->id : false;
        }
        catch (Exception $e)
        {
            if (isset($data['ticket']) && $this->model('Game')->findByAttributes(array('ticket' => $data['ticket'])))
            {
                return -1;
            }
            return false;
        }
    }

    public function getFlashGame($id)
    {
        if (!$game = $this->model('Game')->findByAttributes(array('id' => $id, 'category_id' => 1)))
        {
            throw new CHttpException('Page not found');
        }
        return $game;
    }

    /**
     * _getAvailableModels
     * Returns array of available models
     * key used for communication as member_type
     *
     * @access private
     * @return void
     */
    private function _getAvailableModels()
    {
        return array(
            'Participant' => 0,
            'Partner' => 1,
            'Sponsor' => 2,
            'Speaker' => 3,
            'Game' => 4,
            'Expert' => 5,
            'SpeakerCategory' => 6,
            'DiscussionPanel' => 7,
        );
    }
}