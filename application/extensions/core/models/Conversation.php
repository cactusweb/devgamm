<?php

namespace application\extensions\core\models;
/*
    create table conversation (
        participant_id char(32) not null,
        member_type tinyint unsigned not null default '0',
        author varchar(255) not null,
        author_email varchar(255) not null,
        link varchar(255) not null,
        message text,
        date timestamp not null default current_timestamp,
        key(participant_id)) Engine=InnoDB Default charset="utf8" Comment="Stores communication between visitors and participants";
 */
/**
 * Partner
 * Model for table conversation
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class Conversation extends ECoreActiveRecord
{
    protected $_alias = 'c';

    private $_cacheKeyPrefix = 'Conversation';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'conversation';
    }

    public function rules()
    {
        return array(
            array('id,email,photo,company,lookingfor_en,lookingfor_ru,propose_en,propose_ru,position,name_en,name_ru', 'required'),
            array('email', 'email'),
        );
    }

    public function generateId()
    {
        if (empty($this->email))
        {
            throw new CException('Email is empty');
        }
        return md5(sha1($this->email));
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
            'member_type' => 'int',
        );
    }
}