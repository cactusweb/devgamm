<?php

namespace application\extensions\core\models;
/*
    discussion_category
    Create Table: CREATE TABLE `discussion_category` (
      `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
      `title_en` varchar(255) NOT NULL,
      `title_ru` varchar(255) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Stores list of available discussion panels categories'
 */
/**
 * DiscussionCategory
 * Model for table discussion_category
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Oleynik Alexander
 */
class DiscussionCategory extends ECoreActiveRecord
{
    protected $_alias = 'dc';

    private $_cacheKeyPrefix = 'DiscussionCategory';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'discussion_category';
    }

    public function rules()
    {
        return array(
            array('title_en,title_ru', 'required'),
            array('description_en,description_ru', 'safe'),
        );
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
        );
    }
}