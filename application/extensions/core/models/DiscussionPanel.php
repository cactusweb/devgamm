<?php

namespace application\extensions\core\models;
/*
    CREATE TABLE `discussion_panel` (
      `id` char(32) NOT NULL,
      `email` varchar(255) NOT NULL,
      `photo` varchar(255) NOT NULL DEFAULT '',
      `company_logo` varchar(255) NOT NULL DEFAULT '',
      `category_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
      `name_en` varchar(255) NOT NULL,
      `name_ru` varchar(255) NOT NULL,
      `bio_en` text,
      `bio_ru` text,
      `rating` int(11) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores profile for discussion panels';

 */
/**
 * DiscussionPanel
 * Model for table discussion_panel
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Oleynik Alexander
 */
class DiscussionPanel extends ECoreActiveRecord
{
    protected $_alias = 'dp';

    private $_cacheKeyPrefix = 'DiscussionPanel';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'discussion_panel';
    }

    public function rules()
    {
        return array(
            array('id,email,photo,name_en,name_ru,bio_ru,bio_en,company_logo,category_id', 'required'),
            array('rating', 'safe'),
        );
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
        );
    }
}