<?php

namespace application\extensions\core\models;

class ECoreActiveRecord extends \ExtensionActiveRecord
{
    protected $_alias = '';

    public function tableName()
    {
        return '';
    }

    public function tableNameAsAlias()
    {
        return $this->tableName() . ' as ' . $this->alias;
    }

    public function generateId()
    {
        if (empty($this->email))
        {
            throw new \CException('Email is empty');
        }
        return md5(sha1($this->email));
    }
}