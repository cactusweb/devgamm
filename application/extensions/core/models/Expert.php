<?php

namespace application\extensions\core\models;
/*
    create table participant (
        id char(32) not null,
        email varchar(255) not null,
        photo varchar(255) not null default '',
        name_en varchar(255) not null,
        name_ru varchar(255) not null,
        rating int not null default '0',
        primary key(id)) Engine=InnoDB Default charset="utf8" Comment="Stores profile of other participants";
 */
/**
 * Partner
 * Model for table partner
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Oleynik Alexander
 */
class Expert extends ECoreActiveRecord
{
    protected $_alias = 'e';
    private $_cacheKeyPrefix = 'Expert';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'expert';
    }

    public function generateId()
    {
        if (empty($this->email))
        {
            throw new \CException('Email is empty');
        }
        return md5(sha1($this->email . $this->category_id));
    }

    public function rules()
    {
        return array(
            array('id,email,photo,name_en,name_ru,bio_ru,bio_en,company_logo,category_id', 'required'),
            array('foreign_speaker', 'safe'),
        );
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
        );
    }
}