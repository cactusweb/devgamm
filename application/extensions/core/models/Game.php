<?php

namespace application\extensions\core\models;

/*
CREATE TABLE `game` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` tinyint not null NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `author_type` tinyint unsigned not null default '0',
  `file_ext` varchar(3) NOT NULL default '',
  `dimension` varchar(255) NOT NULL default '',
  `icon_ext` varchar(3) NOT NULL default '',
  `link` text NOT NULL,
  `video` text NOT NULL,
  `email` tinytext NOT NULL,
  `phone` tinytext NOT NULL,
  `lynch` tinyint not null NOT NULL default '0',
  `gammplay` tinyint unsigned NOT NULL DEFAULT '0',
  `f2p` tinyint unsigned NOT NULL DEFAULT '0',
  `platform` varchar(255) NOT NULL DEFAULT '',
  `stage` tinyint unsigned DEFAULT '0',
  `location` varchar(255) NOT NULL DEFAULT '',
  `approved` tinyint unsigned NOT NULL DEFAULT '0',
  `rating` int NOT NULL DEFAULT '0',
  `ticket` int not null DEFAULT '0',
  `date` timestamp NOT NULL default current_timestamp,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`ticket`),
) Engine=InnoDB default charset="utf8" comment="Stores games"
 */
/**
 * Partner
 * Model for table game
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class Game extends ECoreActiveRecord
{
    protected $_alias = 'g';

    private $_cacheKeyPrefix = 'Game';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'game';
    }

    public function rules()
    {
        return array(
            array('title, category_id, description, author, author_type, video, email, phone, stage, location, ticket', 'required'),
            array('platforms, technology, premium, multiplayer, team_size, contact_person, skype, awards, awards_nominations, showcase_device, showcase_time', 'safe'),
            array('icon_ext', 'safe'),
            array('email', 'email'),
//            array('ticket', 'numerical'),
            array('file_ext, dimension, lynch, gammplay, f2p, platform, approved, rating, date, link, steam_code', 'safe'),
        );
    }

    protected function beforeValidate()
    {
        if (isset($this->link) && is_array($this->link))
        {
            $this->link = serialize($this->link);
        }
        return true;
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
            'lynch' => 'int',
            'gammplay' => 'int',
            'approved' => 'int',
        );
    }
}