<?php

namespace application\extensions\core\models;

/*
INSERT INTO game_category (title_en, title_ru, color, icons, rating) VALUES
('Flash games', 'Flash игры', 'red', '["flash.png"]', 300),
('Social games', 'Социальные игры', 'blue', '["social.png"]', 400),
('HTML5 games', 'HTML5 игры', 'orange', '["html5.png"]',200),
('Mobile games', 'Мобильные игры', 'green', '["android.png","apple.png","win.png","android.png","unity.png"]', 500);
*/
/*
CREATE TABLE `game_category` (
  `id` tinyint unsigned NOT NULL auto_increment,
  `title_en` varchar(255) NOT NULL,
  `title_ru` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `icons` text NOT NULL,
  `rating` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY (`rating`)
) Engine=InnoDB default charset="utf8" comment="Stores games categories"
 */
/**
 * GameCategory
 * Model for table game_category
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class GameCategory extends ECoreActiveRecord
{
    protected $_alias = 'gc';

    private $_cacheKeyPrefix = 'GameCategory';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'game_category';
    }

    public function rules()
    {
        return array(
            array('id, title_en, title_ru, color, icons', 'required'),
            array('rating', 'safe'),
        );
    }

    protected function beforeValidate()
    {
        if (isset($this->icons) && is_array($this->icons))
        {
            $this->icons = json_encode($this->icons);
        }
        return true;
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
            'lynch' => 'int',
            'gammplay' => 'int',
            'approved' => 'int',
        );
    }
}