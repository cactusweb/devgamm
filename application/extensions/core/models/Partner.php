<?php

namespace application\extensions\core\models;
/*
    CREATE TABLE `expert` (
      `id` char(32) NOT NULL,
      `email` varchar(255) NOT NULL,
      `photo` varchar(255) NOT NULL DEFAULT '',
      `company_logo` varchar(255) NOT NULL DEFAULT '',
      `category_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
      `name_en` varchar(255) NOT NULL,
      `name_ru` varchar(255) NOT NULL,
      `bio_en` text,
      `bio_ru` text,
      `rating` int(11) NOT NULL DEFAULT '0',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores profile of other participants'
 */
/**
 * Partner
 * Model for table partner
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class Partner extends ECoreActiveRecord
{
    protected $_alias = 'p';

    private $_cacheKeyPrefix = 'Partner';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'partner';
    }

    public function rules()
    {
        return array(
            array('id, email, name_en', 'required'),
            array('company_title, position, name_ru, lookingfor_en, lookingfor_ru, propose_en, propose_ru', 'safe'),
            array('rating, ticket_id', 'numerical', 'integerOnly'=>true),
            array('id', 'length', 'max'=>32),
            array('email, name_en, name_ru, company_title, company_url, position, photo', 'length', 'max'=>255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, email, name_en, name_ru, company_title, company_url, position, photo, lookingfor_en, lookingfor_ru, propose_en, propose_ru, rating, ticket_id', 'safe', 'on'=>'search'),
        );

    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
        );
    }
}