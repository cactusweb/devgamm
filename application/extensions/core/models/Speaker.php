<?php

namespace application\extensions\core\models;
/*
    create table speaker(
        id char(32) not null,
        email varchar(255) not null,
        name_en varchar(255) not null,
        name_ru varchar(255) not null,
        topic_en text,
        topic_ru text,
        photo varchar(255) not null default '',
        company_logo varchar(255) not null default '',
        description_en text,
        description_ru text,
        bio_en text,
        bio_ru text,
        video varchar(255) not null default '',
        presentation varchar(255) not null default '',
        category_id tinyint unsigned not null default '0',
        rating int not null default '0',
        primary key(id)) Engine=InnoDB Default charset="utf8" Comment="Stores profile of speaker";
 */
/**
 * Speaker
 * Model for table speaker
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class Speaker extends ECoreActiveRecord
{
    protected $_alias = 's';

    private $_cacheKeyPrefix = 'Speaker';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'speaker';
    }

    public function rules()
    {
        return array(
            array('id,photo,company_logo,name_en,name_ru,topic_en,topic_ru,bio_en,bio_ru,category_id, email', 'required'),
            array('description_en,description_ru,video,presentation,foreign_speaker', 'safe'),
        );
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
            'category_id' => 'int',
        );
    }
}