<?php

namespace application\extensions\core\models;
/*
    create table speaker_category (
        id tinyint unsigned not null auto_increment,
        title_en varchar(255) not null,
        title_ru varchar(255) not null,
        primary key(id)) Engine=InnoDB Default charset="utf8" Comment="Stores list of available speaker categories";
 */
/**
 * SpeakerCategory
 * Model for table speaker_category
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class SpeakerCategory extends ECoreActiveRecord
{
    protected $_alias = 'sc';

    private $_cacheKeyPrefix = 'SpeakerCategory';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'speaker_category';
    }

    public function rules()
    {
        return array(
            array('title_en,title_ru', 'required'),
            array('color', 'safe'),
        );
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
        );
    }
}