<?php

namespace application\extensions\core\models;
/*
create table sponsor (
        id char(32) not null,
        email varchar(255) not null,
        url varchar(255) not null default '',
        logo varchar(255) not null default '',
        title_en varchar(255) not null,
        title_ru varchar(255) not null,
        description_en text,
        description_ru text,
        type_id tinyint not null default '0',
        rating int not null default '0',
        primary key(id)) Engine=InnoDB Default charset="utf8" Comment="Stores profile of sponsors";
 */
/**
 * Sponsor
 * Model for table sponsor
 *
 * @uses ECoreActiveRecord
 * @package devgamm
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class Sponsor extends ECoreActiveRecord
{
    protected $_alias = 'sp';

    private $_cacheKeyPrefix = 'Sponsor';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'sponsor';
    }

    public function rules()
    {
        return array(
            array('id,email,url,logo,title_en,title_ru,description_en,description_ru,category_id', 'required'),
        );
    }

    protected function _getAvailableAttributes()
    {
        return array(
            'id' => 'string',
            'category_id' => 'int',
        );
    }
}