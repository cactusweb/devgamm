<?php

return array(
    'Diamond' => 'Diamond sponsor',
    'Platinum' => 'Platinum sponsors',
    'Gold' => 'Gold sponsors',
    'Silver' => 'Silver sponsors',
    'Bronze' => 'Bronze sponsors',
    'Party' => 'Party sponsor',
    'Lounge' => 'Sponsor of Business Lounge',
);
