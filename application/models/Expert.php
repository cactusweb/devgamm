<?php

/**
 * This is the model class for table "expert".
 *
 * The followings are the available columns in table 'expert':
 * @property string $id
 * @property string $email
 * @property string $photo
 * @property string $company_logo
 * @property integer $category_id
 * @property string $name_en
 * @property string $name_ru
 * @property string $bio_en
 * @property string $bio_ru
 * @property integer $rating
 * @property integer $foreign_speaker
 */
class Expert extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Expert the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'expert';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, email, name_en, name_ru', 'required'),
			array('category_id, rating, foreign_speaker', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>32),
			array('email, photo, company_logo, name_en, name_ru', 'length', 'max'=>255),
			array('bio_en, bio_ru', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, photo, company_logo, category_id, name_en, name_ru, bio_en, bio_ru, rating, foreign_speaker', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'photo' => 'Photo',
			'company_logo' => 'Company Logo',
			'category_id' => 'Category',
			'name_en' => 'Name En',
			'name_ru' => 'Name Ru',
			'bio_en' => 'Bio En',
			'bio_ru' => 'Bio Ru',
			'rating' => 'Rating',
			'foreign_speaker' => 'Foreign Speaker',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('company_logo',$this->company_logo,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('name_en',$this->name_en,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('bio_en',$this->bio_en,true);
		$criteria->compare('bio_ru',$this->bio_ru,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('foreign_speaker',$this->foreign_speaker);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}