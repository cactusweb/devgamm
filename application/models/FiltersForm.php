<?php
class FiltersForm extends CFormModel
{
    public $filters = array();

    /**
     * Override magic getter for filters
     * @access public
     * @return array
     */
    public function __get($name)
    {
        if (!isset($this->filters[$name]))
        {
            $this->filters[$name] = null;
        }

        return $this->filters[$name];
    }

    /**
     * Filter input array by key value pairs
     * @param array $data rawData
     * @access public
     * @throws InvalidArgumentException wrong income parameter
     * @return array filtered data array
     */
    public function filter($data)
    {
        if (!is_array($data))
        {
            throw new InvalidArgumentException(__METHOD__ . " - invalid type of parameter: ".gettype($data)."", E_USER_WARNING);
        }

        foreach ($data as $rowIndex => $row)
        {
            foreach ($this->filters as $key => $value)
            {
                if (isset($row[$key]) && !empty($value) && false === stripos($row[$key], $value))
                {
                    unset($data[$rowIndex]);
                }
            }
        }
        return $data;
    }
}