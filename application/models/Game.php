<?php

/**
 * This is the model class for table "game".
 *
 * The followings are the available columns in table 'game':
 * @property integer $id
 * @property string $ticket
 * @property integer $category_id
 * @property string $title
 * @property string $description
 * @property string $author
 * @property integer $author_type = [0=>'indi', 1=>'studio', 2=>'company']
 * @property string $file_ext
 * @property string $dimension
 * @property string $icon_ext
 * @property string $link = Array ( 'appstore' => '', 'gplay' => '', 'web' => '', 'other' => '', 'win' => '', 'mac' => '', 'steam' => '')
 * @property string $video
 * @property string $email
 * @property string $phone
 * @property integer $lynch
 * @property integer $gammplay
 * @property integer $f2p
 * @property string $platform
 * @property integer $stage = [1 => 'Prototype', 2 => 'Alpha', 3 => 'Beta', 4 => 'Completed']
 * @property string $location
 * @property string $steam_code
 * @property integer $approved
 * @property integer $rating
 * @property string $date
 * @property integer $ticket_id
 * @property string $platforms
 * @property string $screenshoots
 * @property string $technology
 * @property integer $premium
 * @property integer $multiplayer
 * @property string $icon
 * @property string $team_size
 * @property string $contact_person
 * @property string $skype
 * @property integer $awards
 * @property string $awards_nominations
 * @property string $showcase_device
 * @property string $showcase_time
 * @property string $showcase_logo
 * @property string $token
 */
class Game extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Game the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'game';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, title, description, author, link, video, email, phone, date, platforms, screenshoots, technology, premium, multiplayer, icon, team_size, contact_person, skype, awards, awards_nominations, showcase_device, showcase_time, showcase_logo, token', 'safe'),
			array('category_id, author_type, lynch, gammplay, f2p, stage, approved, rating, ticket_id, premium, multiplayer, awards', 'numerical', 'integerOnly'=>true),
			array('ticket', 'length', 'max'=>16),
			array('title, author, dimension, platform, location, steam_code, awards_nominations, showcase_device', 'length', 'max'=>255),
			array('file_ext, icon_ext', 'length', 'max'=>3),
			array('icon, team_size, contact_person, skype, showcase_logo', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ticket, category_id, title, description, author, author_type, file_ext, dimension, icon_ext, link, video, email, phone, lynch, gammplay, f2p, platform, stage, location, steam_code, approved, rating, date, ticket_id, platforms, screenshoots, technology, premium, multiplayer, icon, team_size, contact_person, skype, awards, awards_nominations, showcase_device, showcase_time, showcase_logo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ticket' => 'Ticket',
			'category_id' => 'Category',
			'title' => 'Title',
			'description' => 'Description',
			'author' => 'Author',
			'author_type' => 'Author Type',
			'file_ext' => 'File Ext',
			'dimension' => 'Dimension',
			'icon_ext' => 'Icon Ext',
			'link' => 'Link',
			'video' => 'Video',
			'email' => 'Email',
			'phone' => 'Phone',
			'lynch' => 'Lynch',
			'gammplay' => 'Gammplay',
			'f2p' => 'F2p',
			'platform' => 'Platform',
			'stage' => 'Stage',
			'location' => 'Location',
			'steam_code' => 'Steam Code',
			'approved' => 'Approved',
			'rating' => 'Rating',
			'date' => 'Date',
			'ticket_id' => 'Ticket',
			'platforms' => 'Platforms',
			'screenshoots' => 'Screenshoots',
			'technology' => 'Technology',
			'premium' => 'Premium',
			'multiplayer' => 'Multiplayer',
			'icon' => 'Icon',
			'team_size' => 'Team Size',
			'contact_person' => 'Contact Person',
			'skype' => 'Skype',
			'awards' => 'Awards',
			'awards_nominations' => 'Awards Nominations',
			'showcase_device' => 'Showcase Device',
			'showcase_time' => 'Showcase Time',
			'showcase_logo' => 'Showcase Logo',
			'token' => 'Token',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ticket',$this->ticket,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('author_type',$this->author_type);
		$criteria->compare('file_ext',$this->file_ext,true);
		$criteria->compare('dimension',$this->dimension,true);
		$criteria->compare('icon_ext',$this->icon_ext,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('video',$this->video,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('lynch',$this->lynch);
		$criteria->compare('gammplay',$this->gammplay);
		$criteria->compare('f2p',$this->f2p);
		$criteria->compare('platform',$this->platform,true);
		$criteria->compare('stage',$this->stage);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('steam_code',$this->steam_code,true);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('ticket_id',$this->ticket_id);
		$criteria->compare('platforms',$this->platforms,true);
		$criteria->compare('screenshoots',$this->screenshoots,true);
		$criteria->compare('technology',$this->technology,true);
		$criteria->compare('premium',$this->premium);
		$criteria->compare('multiplayer',$this->multiplayer);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('team_size',$this->team_size,true);
		$criteria->compare('contact_person',$this->contact_person,true);
		$criteria->compare('skype',$this->skype,true);
		$criteria->compare('awards',$this->awards);
		$criteria->compare('awards_nominations',$this->awards_nominations,true);
		$criteria->compare('showcase_device',$this->showcase_device,true);
		$criteria->compare('showcase_time',$this->showcase_time,true);
		$criteria->compare('showcase_logo',$this->showcase_logo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}