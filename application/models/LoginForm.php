<?php

class LoginForm extends CFormModel
{
    public $userName;
    public $password;
    public $rememberMe = false;

    private $_identity;

    public function init()
    {
        if (isset($_POST[__CLASS__]) && !empty($_POST[__CLASS__]))
        {
            $this->attributes = $_POST[__CLASS__];
        }
    }

    public function rules()
    {
        return array(
            array('userName, password', 'required'),
            array('password', 'authenticateValidator'),
            array('rememberMe', 'boolean'),
        );
    }

    public function authenticateValidator($attribute, $params)
    {
        $this->_identity = new UserIdentity($this->userName, $this->password);

        if(!$this->_identity->authenticate())
        {
            $this->addError('password', $this->_identity->getErrorMessage());
        }
    }

    public function login()
    {
        if (null === $this->_identity)
        {
            $this->_identity = new UserIdentity($this->userName, $this->password);
            $this->_identity->authenticate();
        }
        if (UserIdentity::ERROR_NONE === $this->_identity->errorCode)
        {
            $duration = $this->rememberMe ? 3600*24*30 : 0;
            $apps = $reports = null;
            if ($this->_identity->apps)
            {
                $apps = $this->_identity->apps;
            }
            if ($this->_identity->reports)
            {
                $reports = $this->_identity->reports;
            }
            $this->_identity->setPersistentStates(array('apps' => $apps, 'reports' => $reports));
            Yii::app()->user->login($this->_identity, $duration);

            return true;
        }
        else
        {
            return false;
        }
    }
}