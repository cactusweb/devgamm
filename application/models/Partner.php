<?php

/**
 * This is the model class for table "partner".
 *
 * The followings are the available columns in table 'partner':
 * @property string $id
 * @property string $email
 * @property string $name_en
 * @property string $name_ru
 * @property string $company_title
 * @property string $company_url
 * @property string $position
 * @property string $photo
 * @property string $lookingfor_en
 * @property string $lookingfor_ru
 * @property string $propose_en
 * @property string $propose_ru
 * @property integer $rating
 * @property integer $ticket_id
 */
class Partner extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Partner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, email, name_en', 'required'),
			array('company_title, position, name_ru', 'safe'),
			array('rating, ticket_id', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>32),
			array('email, name_en, name_ru, company_title, company_url, position, photo', 'length', 'max'=>255),
			array('lookingfor_en, lookingfor_ru, propose_en, propose_ru', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, name_en, name_ru, company_title, company_url, position, photo, lookingfor_en, lookingfor_ru, propose_en, propose_ru, rating, ticket_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'name_en' => 'Name En',
			'name_ru' => 'Name Ru',
			'company_title' => 'Company Title',
			'company_url' => 'Company Url',
			'position' => 'Position',
			'photo' => 'Photo',
			'lookingfor_en' => 'Lookingfor En',
			'lookingfor_ru' => 'Lookingfor Ru',
			'propose_en' => 'Propose En',
			'propose_ru' => 'Propose Ru',
			'rating' => 'Rating',
			'ticket_id' => 'Ticket',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name_en',$this->name_en,true);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('company_title',$this->company_title,true);
		$criteria->compare('company_url',$this->company_url,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('lookingfor_en',$this->lookingfor_en,true);
		$criteria->compare('lookingfor_ru',$this->lookingfor_ru,true);
		$criteria->compare('propose_en',$this->propose_en,true);
		$criteria->compare('propose_ru',$this->propose_ru,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('ticket_id',$this->ticket_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function generateId()
	{
		if (empty($this->email))
		{
			throw new \CException('Email is empty');
		}
		return md5(sha1($this->email));
	}

}