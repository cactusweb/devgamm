<?php

/**
 * This is the model class for table "sponsor".
 *
 * The followings are the available columns in table 'sponsor':
 * @property string $id
 * @property string $email
 * @property string $url
 * @property string $logo
 * @property string $title_en
 * @property string $title_ru
 * @property string $description_en
 * @property string $description_ru
 * @property integer $category_id
 * @property integer $rating
 */
class Sponsor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sponsor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sponsor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, email, title_en, title_ru', 'required'),
			array('category_id, rating', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>32),
			array('email, url, logo, title_en, title_ru', 'length', 'max'=>255),
			array('description_en, description_ru', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, email, url, logo, title_en, title_ru, description_en, description_ru, category_id, rating', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'url' => 'Url',
			'logo' => 'Logo',
			'title_en' => 'Title En',
			'title_ru' => 'Title Ru',
			'description_en' => 'Description En',
			'description_ru' => 'Description Ru',
			'category_id' => 'Category',
			'rating' => 'Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('title_en',$this->title_en,true);
		$criteria->compare('title_ru',$this->title_ru,true);
		$criteria->compare('description_en',$this->description_en,true);
		$criteria->compare('description_ru',$this->description_ru,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('rating',$this->rating);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}