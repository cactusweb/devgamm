<?php

class UserForm extends CFormModel
{
    public $l; // login
    public $p;
    public $password;
    public $passwordRepeat;
    public $r; // reports list
    public $a; // apps list
    public $s; // status

    public function init()
    {
    }

    public function __toString()
    {
        $result = array();
        foreach ($this as $k => $v)
        {
            $result[] = $k;
        }
        return implode(',', $result);
    }

    public function __get($field)
    {
        $method = '_get' . ucfirst($field);
        if (method_exists($this, $method))
        {
            return $this->$method();
        }
        return parent::__get($field);
    }

    public function rules()
    {
        return array(
            array((string)$this, 'safe'),
            array('l', 'required'),
            array('s', 'numerical'),
            array('password', 'passwordValidator', 'message' => 'Passwords doesn\'t match', 'compare' => 'passwordRepeat'),
            array('a', 'listValidator', 'message' => 'App is incorrect', 'type' => 'apps'),
            array('r', 'listValidator', 'message' => 'Reports is incorrect', 'type' => 'reports'),
        );
    }

    public function listValidator($attribute, $params)
    {
        if (empty($this->attributes[$attribute]) || !is_array($this->attributes[$attribute]))
        {
            if ('a' == $attribute)
            {
                return true;
            }
            $this->addError('a', $params['message']);
            return false;
        }
        $field = $params['type'];
        if (!$this->$field)
        {
            throw new CException(__METHOD__ . ' - unsupported type specified: {' . $params['type'] . '}', E_USER_ERROR);
        }
        $field = $this->$field;
        foreach ($this->attributes[$attribute] as $v)
        {
            if (!isset($field[$v]))
            {
                $this->addError('a', $params['message']);
                return false;
            }
        }
        return true;
    }

    public function passwordValidator($attribute, $params)
    {
        if (!isset($params['compare']))
        {
            $this->addError('a', $params['message']);
            return false;
        }
        if (empty($this->attributes[$attribute]) && empty($this->attributes[$params['compare']]))
        {
            return true;
        }
        if ($this->attributes[$attribute] != $this->attributes[$params['compare']])
        {
            $this->addError('a', $params['message']);
            return false;
        }
        $this->p = UserIdentity::getPassword($this->attributes[$attribute]);
        return true;
    }

    public function setAttributes($values, $safeOnly = true)
    {
        if (isset($values['r']))
        {
            $r = array();
            foreach ($values['r'] as $v)
            {
                if (isset($this->reports[$v]))
                {
                    $r[$v] = $v;
                }
            }
            $values['r'] = array_values($r);
        }
        if (isset($values['a']))
        {
            $r = array();
            foreach ($values['a'] as $v)
            {
                $k = array_flip($this->apps)[$v];
                $r[$k] = $k;
            }
            $values['a'] = array_values($r);
        }
        return parent::setAttributes($values, $safeOnly);
    }

    protected function _getApps()
    {
        $result = array();
        foreach (Yii::app()->Core->apps as $app => $data)
        {
            $result[$app] = $data['title'];
        }
        return $result;
    }

    protected function _getReports()
    {
        return Yii::app()->Core->getReports();
    }
}