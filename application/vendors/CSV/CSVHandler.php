<?php

/**
 * CSVHandler
 * Provides parsing data from CSV file for import into system
 *
 * Important: getAvailableFieldValues should be overloaded
 * This method contains data providing for each type of CSV file.
 * Such as sexsearchcom, that has some fields and values types, etc
 *
 * So, for usage you should extend this base class
 *
 *
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class CSVHandler
{
    public $rowsInPack = 100;

    protected $_fileHandler = null;
    protected $_fileName = null;
    protected $_headers = array();
    protected $_ignoredHeaders = array();

    private $_delimiter = '';

    public function init()
    {
    }

    /**
     * setFile
     * Gets headers from $filename and delimiter, if $delimiter is empty
     *
     * @param string $fileName
     * @param string $delimiter
     * @access public
     * @return void
     */
    public function setFile($fileName)
    {
        if (empty($fileName) || !is_string($fileName) || !file_exists($fileName))
        {
            throw new CException('Can\'t find file ' . $fileName);
        }
        $this->_fileName = $fileName;
        $this->_fileHandler = fopen($fileName, 'r');
        if (!$this->_fileHandler)
        {
            throw new CException('Can\'t open file ' . $fileName);
        }
        $this->_getHeaders();

        return $this;
    }

    public function __destruct()
    {
        fclose($this->_fileHandler);
    }

    /**
     * getAvailableFieldValues
     * Data provider method
     * Returns map of CSV fields with our data structure
     *
     * Format:
     * array(
     *      '__field__'     => array(
     *          'storage'   => '__storage__',
     *          'field'     => '__our_field__',
     *          'validator' => '__validator__',
     *          'map'       => array('__value__' => '__our_value__'),
     *      ),
     * ),
     * __field__       = field name in CSV file
     * __storage__     = one of tables, that stores user attributes, such as user, profile, user_attribute, user_text_attribute
     * __our_field__   = field name in our system
     * __validator__   = callable method of DataProvider class
     * __value__       = one of available value in CSV file for current __field__
     *
     *
     * @access public
     * @return array
     */
    public function getAvailableFieldValues()
    {
        return array();
    }

    /**
     * getDataRow
     * Returns next parsed row as array of key-values of current CSV file
     *
     * @access public
     * @return array
     */
    public function getDataRow()
    {
        set_time_limit(0);
        $dataRow = array();
        $row = fgets($this->_fileHandler);
        $row = rtrim($row);
        if (empty($row))
        {
            return $dataRow;
        }
        if (empty($this->_delimiter))
        {
            $this->_getDelimiter($row);
        }
        $dataRow = explode($this->_delimiter, $row);
        if (empty($dataRow) || !is_array($dataRow))
        {
            throw new CException('Can\'t parse headers');
        }
        if (!empty($this->_headers))
        {
            $dataRow = $this->_parseDataRow($dataRow);
        }
        return $dataRow;
    }

    /**
     * getRowsPack
     * Returns array of parsed rows
     *
     * @access public
     * @return array
     */
    public function getRowsPack()
    {
        $data = array();
        $countRows = 0;
        while ($row = $this->getDataRow())
        {
            if ($countRows >= $this->rowsInPack)
            {
                return $this->_prepareRows($data);
            }
            $data[] = $row;
            $countRows++;
        }
        if (!empty($data))
        {
            return $this->_prepareRows($data);
        }
    }

    protected function _prepareRows($rows)
    {
        if (empty($rows) || !is_array($rows))
        {
            throw new CException('Empty dataset for import');
        }
        $availableFields = $this->getAvailableFieldValues();
        $data = array();
        foreach ($rows as $key => $row)
        {
            foreach ($row as $field => $value)
            {
                if (!isset($availableFields[$field]['storage']))
                {
                    continue;
                }
                $fieldName = $field;
                if (isset($availableFields[$field]['field']) && is_string($availableFields[$field]['field']))
                {
                    $fieldName = $availableFields[$field]['field'];
                }
                $data[$availableFields[$field]['storage']][$key][$fieldName] = $value;
            }
        }
        return $data;
    }

    /**
     * _afterParseDataRow
     * Method used for additional parsing data row after working by getAvailableFieldValues data map
     * Can be overloaded in inheritable classes
     *
     * @param mixed $row
     * @access protected
     * @return array
     */
    protected function _afterParseDataRow($row)
    {
        return $row;
    }

    /**
     * _randomizeFields
     * Method used for filling empty fields by random values
     * Can be overloaded in inheritable classes
     *
     * @param array $row
     * @access protected
     * @return array
     */
    protected function _randomizeFields($row)
    {
        return $row;
    }

    private function _parseDataRow($row)
    {
        $availableFields = $this->getAvailableFieldValues();
        if (empty($availableFields) || !is_array($availableFields))
        {
            throw new CException('Incorrect map for rows in csv dataset');
        }
        $parsedRow = array();
        foreach ($this->_headers as $key => $field)
        {
            if (!isset($row[$key]))
            {
                continue;
            }
            $value = $row[$key];
            if (isset($availableFields[$field]['validator']))
            {
                $value = $this->_validateField($row[$key], isset($availableFields[$field]['validator']) ? $availableFields[$field]['validator'] : null);
            }
            if (isset($availableFields[$field]['map']))
            {
                $map_key = strtolower($value);
                if (!isset($availableFields[$field]['map'][$map_key]))
                {
                    continue;
                }
                $value = $availableFields[$field]['map'][$map_key];
            }
            $parsedRow[$field] = $value;
        }
        $parsedRow = $this->_randomizeFields($parsedRow);
        return $this->_afterParseDataRow($parsedRow);
    }

    private function _validateField($value, $validator = 'string')
    {
        if (!isset($value))
        {
            return null;
        }
        if (empty($validator))
        {
            $validator = 'string';
        }
        if (method_exists($this, $validator))
        {
            return $this->$validator($value);
        }
        switch ($validator)
        {
            case 'int':
                return is_numeric($value) ? $value : null;
                break;
            case 'string':
                return is_string($value) ? $value : null;
                break;
            default:
                throw new CException('Invalid validator specified: ' . $validator);
                break;
        }
        return null;
    }

    private function _getHeaders()
    {
        $rows = $this->getDataRow();
        $this->_headers = array();
        $availableHeaders = $this->getAvailableFieldValues();
        foreach ($rows as $key => $field)
        {
            if (isset($availableHeaders[$field]))
            {
                $this->_headers[$key] = $field;
            }
            else
            {
                $this->_ignoredHeaders[$key] = $field;
            }
        }
    }

    private function _getDelimiter($row)
    {
        if (empty($row) || !is_string($row))
        {
            throw new CException('Invalid input param specified');
        }
        if (!preg_match('/^[a-zA-Z0-9_]+(.{1}).*/i', $row, $matches) || !isset($matches[1]) || !is_string($matches[1]))
        {
            throw new CException('Can\'t find delimiter in csv file headers. Available symbols in header are a-zA-Z0-9_ than should be present delimiter');
        }
        $this->_delimiter = $matches[1];
    }
}
