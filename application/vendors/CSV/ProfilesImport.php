<?php

Yii::import('admin.vendors.csv.CSVHandler');

class ProfilesImport extends CSVHandler
{
    protected static $_coords = array();

    public function getStoragePriority()
    {
        return array(
            'user' => 'importUsers',
            'profile' => 'importUserProfiles',
            'attribute' => 'importUserAttributes',
            'text_attribute' => 'importUserTextAttributes',
            'profile_photo' => 'importUserPhotos',
        );
    }

    public function import($userPhotosParams = array())
    {
        $result = array();
        $storagePriority = $this->getStoragePriority();
        foreach ($storagePriority as $storage => $handler)
        {
            $result['storage_' . $storage] = 0;
        }
        while ($rowsPack = $this->getRowsPack())
        {
            $ids = array();
            foreach ($storagePriority as $storage => $handler)
            {
                if (!isset($rowsPack[$storage]) || !method_exists(get_class(Yii::app()->eUser), $handler))
                {
                    continue;
                }
                $result['storage_' . $storage] += count($rowsPack[$storage]);
                $ids = Yii::app()->eUser->$handler($rowsPack[$storage], $ids, $userPhotosParams);
            }
        }
        //$this->_sendApproveRequest($ids);
        return $result;
    }

    protected function _sendApproveRequest(array $userIds)
    {
        if (!empty($userIds))
        {
            foreach ($userIds as $id)
            {
                Approve::sendApproveRequest(
                    array(
                        'user_id' => $id,
                        'data' => array(),
                    ),
                    'content_approve'
                );
            }
        }
    }

    protected function _getCoordsByCity($city, $country = '', $province = '')
    {
        if (isset(self::$_coords[$country . $city]))
        {
            return self::$_coords[$country . $city];
        }

        $geo_info = Yii::app()->eGeo->suggestLocation($city, array(
            'limit'     => 1,
            'format'    => 'array',
            'city'      => $city,
            'country'   => $country,
        ));
        self::$_coords[$country . $city] = array(
            'latitude'  => $geo_info['latitude'],
            'longitude' => $geo_info['longitude'],
        );
        
        return self::$_coords[$country . $city];
    }
}
