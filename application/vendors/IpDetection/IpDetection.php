<?php

/**
 * IpDetection
 * Provides detection user ip address and country (based on detected ip address)
 *
 * @depends php5-geoip package
 * @package
 * @version $id$
 * @author Victor Perov <vic.gald@gmail.com>
 */
class IpDetection
{
    public function init()
    {
    }

    public function checkIp($ip)
    {
        if (empty($ip) || ip2long($ip) == -1 || ip2long($ip) == false)
        {
            return false;
        }
        $private_ips = array (
            array('0.0.0.0','2.255.255.255'),
            array('10.0.0.0','10.255.255.255'),
            array('127.0.0.0','127.255.255.255'),
            array('169.254.0.0','169.254.255.255'),
            array('172.16.0.0','172.31.255.255'),
            array('192.0.2.0','192.0.2.255'),
            array('192.168.0.0','192.168.255.255'),
            array('255.255.255.0','255.255.255.255')
        );
        foreach ($private_ips as $r)
        {
            $min = ip2long($r[0]);
            $max = ip2long($r[1]);
            if ((ip2long($ip) >= $min) && (ip2long($ip) <= $max))
            {
                return false;
            }
        }
        return true;
    }

    public function getIp()
    {
        if ($this->checkIp($_SERVER['HTTP_CLIENT_IP']))
        {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        if ($this->checkIp($_SERVER['HTTP_X_REAL_IP']))
        {
            return $_SERVER['HTTP_X_REAL_IP'];
        }
        foreach (explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']) as $ip)
        {
            if ($this->checkIp(trim($ip)))
            {
                return $ip;
            }
        }
        if ($this->checkIp($_SERVER['HTTP_X_FORWARDED']))
        {
            return $_SERVER['HTTP_X_FORWARDED'];
        }
        if ($this->checkIp($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        {
            return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        }
        if ($this->checkIp($_SERVER['HTTP_FORWARDED_FOR']))
        {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        }
        if ($this->checkIp($_SERVER['HTTP_FORWARDED']))
        {
            return $_SERVER['HTTP_FORWARDED'];
        }
        return $_SERVER['REMOTE_ADDR'];
    }

    public function getCountry($ip = '')
    {
        if (empty($ip))
        {
            $ip = $this->getIp();
        }
        elseif (!$this->checkIp($ip))
        {
            return false;
        }
        try
        {
            return @geoip_country_code_by_name($ip);
        }
        catch (Exception $e)
        {
            return false;
        }
    }
}
