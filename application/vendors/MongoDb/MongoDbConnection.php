<?php

class MongoDbConnection extends CApplicationComponent
{
    private $_db;
    private $_user;
    private $_password;
    private $_host='localhost';
    private $_model='';
    private $_replSet = null;
    private static $_connections = array();
    public $config;
    public $active = true;
    

    public function init()
    {
        parent::init();
    }

    protected function getConnection()
    {
        if (!isset(self::$_connections[$this->_model]))
        {
            try
            {
                $params = array('username' => $this->_user, 'password' => $this->_password, 'db' => $this->_db);
                if (isset($this->_replSet))
                {
                    $params['replicaSet'] = $this->_replSet;
                }
                self::$_connections[$this->_model] = new MongoClient("mongodb://$this->_host", $params);
            }
            catch(MongoConnectionException $e)
            {
                throw new CDbException('Can\'t connect to Mongo DB [' . $this->_host . ', u:'. $this->_user  . '] with message {' . $e->getMessage() . '}');
            }
        }
        return self::$_connections[$this->_model];
    }

    protected function setDb($config)
    {
        $this->_db = $config;
    }
    protected function getDb()
    {
        $db = $this->_db;
        return $this->connection->$db;
    }

    protected function setUser($config)
    {
        $this->_user = $config;
    }
    protected function getUser()
    {
        return $this->_user;
    }
    protected function setPassword($config)
    {
        $this->_password = $config;
    }
    protected function getPassword()
    {
        return $this->_password;
    }

    protected function setHost($config)
    {
        $this->_host = $config;
    }
    protected function getHost()
    {
        return $this->_host;
    }
    protected function setModel($name)
    {
        $this->_model = $name;
        if(!empty($this->config[$this->_model]))
        {
            $this->_db = $this->config[$this->_model]['db'];
            $this->_user = $this->config[$this->_model]['user'];
            $this->_password = $this->config[$this->_model]['password'];
            $this->_host = $this->config[$this->_model]['host'];
            $this->_replSet = isset($this->config[$this->_model]['replSet']) ? $this->config[$this->_model]['replSet'] : null;
        }
    }
    protected function getModel()
    {
        return $this->_model;
    }
}
