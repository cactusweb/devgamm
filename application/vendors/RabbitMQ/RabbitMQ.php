<?php

class RabbitMQ extends CApplicationComponent
{
    public static $exchange = null;

    private $_conn = null;
    private $_queues = array();
    private static $_models = array();

    public function init()
    {
        $model = get_called_class();
        Yii::app()->getComponent('RabbitMQ')->model = $model;
        $this->_conn = Yii::app()->getComponent('RabbitMQ')->exchange;
    }

    public function __construct($scenario = 'insert')
    {
        $this->init();
    }

    public static function model()
    {
        $className = get_called_class();
        if (isset(self::$_models[$className]))
        {
            return self::$_models[$className];
        }
        else
        {
            $model = self::$_models[$className] = new $className(null);
            return $model;
        }
    }

    public function getQueues()
    {
        return array();
    }

    public function getQueueTitle()
    {
        return '';
    }

    public function send($queueTitle, $message, $flags = AMQP_NOPARAM, $options = array())
    {
        if (empty($message))
        {
            return true;
        }
        $type = 'text/plain';
        if (!is_scalar($message))
        {
            $message = json_encode($message);
            $type = 'application/json';
        }
        if (!isset($options['content_type']))
        {
            $options['content_type'] = $type;
        }

        try
        {
            $this->_declareQueue($queueTitle);
            $result = $this->exchange->publish($message, $queueTitle, $flags, $options);
        }
        catch (Exception $e)
        {
            return false;
        }
        return $result;
    }

    public function get($queueTitle)
    {
        $queue = $this->_declareQueue($queueTitle);
        return $queue->get(AMQP_AUTOACK);
    }

    public function consume($queueTitle)
    {
    }

    public function declareAllQueues($queues = array())
    {
        if (empty($queues) || !is_array($queues))
        {
            $queues = $this->getQueues();
        }
        if (empty($queues) || !is_array($queues))
        {
            throw new CException(__METHOD__ . ' - can\'t work with empty queues', E_USER_ERROR);
        }
        foreach ($queues as $title => $data)
        {
            $this->_declareQueue($title);
        }
    }

    protected function getExchange()
    {
        return isset($this->_conn['exchange']) ? $this->_conn['exchange'] : null;
    }

    protected function getChannel()
    {
        return isset($this->_conn['channel']) ? $this->_conn['channel'] : null;
    }

    private function _declareQueue($queueTitle)
    {
        if (empty($queueTitle) || !isset($this->queues[$queueTitle]))
        {
            throw new CDbException('Incorrect queue specified: {"' . $queueTitle . '"}');
        }
        if (isset($this->_queues[$queueTitle]))
        {
            return $this->_queues[$queueTitle];
        }
        try
        {
            $queue = new AMQPQueue($this->channel);
            $queue->setName($queueTitle);
            $queue->declare();
            $queue->bind($this->exchange->getName(), $queueTitle);
        }
        catch (Exception $e)
        {
            throw new CDbException('Can\'t declare queue');
        }
        $this->_queues[$queueTitle] = $queue;
        return $queue;
    }
}