<?php

class RabbitMQConnection extends CApplicationComponent
{
    private $_connection = '';

    private $_user;
    private $_password;
    private $_host = 'localhost';
    private $_port = '5672';
    private $_model = '';
    private $_vHost;
    private $_excType = AMQP_EX_TYPE_DIRECT;
    private $_excFlags = AMQP_DURABLE;
    private $_connections = array();
    private $_exchanges = array();

    public $config;

    public function init()
    {
    }

    protected function getExchange()
    {
        if (isset($this->_exchanges[$this->_model]))
        {
            return $this->_exchanges[$this->_model];
        }
        if (isset($this->_connections[$this->_connection]))
        {
            $conn = $this->_connections[$this->_connection];
        }
        else
        {
            try
            {
                $conn = new AMQPConnection(array(
                    'host' => $this->_host,
                    'port' => $this->_port,
                    'vhost' => $this->_vHost,
                    'login' => $this->_user,
                    'password' => $this->_password,
                ));
                $conn->connect();
                if (!$conn->isConnected())
                {
                    throw new CDbException('Can\'t establish connection to rabbitMQ broker');
                }
                $this->_connections[$this->_connection] = $conn;
            }
            catch(AMQPConnectionException $e)
            {
                throw new CDbException('Can\'t establish connection to rabbitMQ broker');
            }
        }
        $channel = new AMQPChannel($conn);
        $this->_exchanges[$this->_model]['channel'] = $channel;
        $exchange = new AMQPExchange($channel);
        $exchange->setType($this->_excType);
        if (isset($this->_excFlags))
        {
            $exchange->setFlags($this->_excFlags);
        }
        $exchange->setName($this->_model);
        $exchange->declare();
        $this->_exchanges[$this->_model]['exchange'] = $exchange;
        return array(
            'exchange' => $exchange,
            'channel' => $channel,
        );
    }

    protected function setModel($name)
    {
        if (empty($this->config[$name]))
        {
            throw new CDbException('Can\'t set config');
        }
        $this->_model = $name;
        $this->_user = $this->config[$this->_model]['user'];
        $this->_connection = $this->config[$this->_model]['connection'];
        $this->_password = $this->config[$this->_model]['password'];
        $this->_host = $this->config[$this->_model]['host'];
        $this->_port = isset($this->config[$this->_model]['port']) ? $this->config[$this->_model]['port'] : $this->_port;
        $this->_vHost = $this->config[$this->_model]['vHost'];
        $this->_excType = isset($this->config[$this->_model]['exchangeType']) ? $this->config[$this->_model]['exchangeType'] : AMQP_EX_TYPE_DIRECT;
        $this->_excFlags = isset($this->config[$this->_model]['exchangeFlags']) ? $this->config[$this->_model]['exchangeFlags'] : null;
    }

    protected function getModel()
    {
        return $this->_model;
    }
}
