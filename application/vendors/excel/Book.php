<?php

Yii::import('application.vendors.excel.core.*');

class Book
{
    private $_title = '';
    private $_sheets = '';
    private $_tmpDir = '/tmp/';
    private $_viaMail = false;
    private $_formulaList = array();
    private $_filename = '';
    private $_reportData = array();

    public function init()
    {
    }

    public function __set($fieldname, $value)
    {
        if (!isset($this->_getPublicFields()[$fieldname]))
        {
            throw new CException(__METHOD__ . ' - can\'t set incorrect field', E_USER_ERROR);
        }
        $validator = $this->_getPublicFields()[$fieldname] . 'Validator';
        if (!empty($validator) && (!method_exists($this, $validator) || !$this->$validator($value)))
        {
            throw new CException(__METHOD__ . ' - $value is incorrect for current field', E_USER_ERROR);
        }
        $this->{"_$fieldname"} = $value;
        return $this;
    }

    public function __get($field)
    {
        if (isset($this->_getPublicFields()[$field]))
        {
            return $this->{"_$field"};
        }
    }

    public function createBook()
    {
        if (empty($this->_title) || empty($this->_sheets))
        {
            throw new CException(__METHOD__ . ' - can\'t process with empty book fields', E_USER_ERROR);
        }
        if (empty($this->_reportData) || !is_array($this->_reportData))
        {
            throw new CException(__METHOD__ . ' - can\'t process with empty data fields', E_USER_ERROR);
        }
        if ($this->_viaMail)
        {
            if (!isset($this->_tmpDir))
            {
                throw new CException(__METHOD__ . ' - tmp dir should be set before sending xls via mail', E_USER_ERROR);
            }
            $book = new Writer($this->_tmpDir . DIRECTORY_SEPARATOR . $this->_title . '.xls');
        }
        else
        {
            $book = new Writer();
            $book->send($this->_title . '.xls');
        }

        $book->setVersion(8);
        foreach ($this->_reportData as $key => $rows)
        {
            $c = $r = 0;
            if (!isset($this->_sheets[$key]['headers']))
            {
                continue;
            }
            if (empty($rows) || !is_array($rows))
            {
                continue;
            }
            $sheet = &$book->addWorksheet(str_replace(':', ' -', $this->_sheets[$key]['title']));
            if ('Worksheet' != get_class($sheet))
            {
                continue;
            }
            $headersMatch = array();
            foreach ($this->_sheets[$key]['headers'] as $k => $v)
            {
                $a = $sheet->write($r, $c, $v);
                $headersMatch[$k] = $c++;
            }
            $r++;
            foreach ($rows as $row)
            {
                foreach ($headersMatch as $f => $i)
                {
                    $sheet->write($r, $i, $this->_processFormula($f, $row));
                }
                $r++;
            }
        }
        $book->close();
        $this->_filename = (isset($this->_tmpDir) ? $this->_tmpDir . DIRECTORY_SEPARATOR : '') . $this->_title . '.xls';
        return $this->_filename;
    }

    protected function _getPublicFields()
    {
        return array(
            'title' => 'string',
            'sheets' => 'sheet',
            'viaMail' => 'bool',
            'tmpDir' => 'dir',
            'formulaList' => 'array',
            'reportData' => 'array',
        );
    }

    protected function _processFormula($field, $value)
    {
        if (!isset($this->_formulaList))
        {
            return $value;
        }
        foreach ($this->_formulaList as $method => $obj)
        {
            if (!is_object($obj) || !method_exists($obj, $method))
            {
                continue;
            }
            $value = $obj->$method($field, $value);
        }
        return $value;
    }

    private function stringValidator($data)
    {
        return isset($data) && is_string($data);
    }

    private function dirValidator($data)
    {
        return !empty($data) && is_dir($data);
    }

    private function arrayValidator($data)
    {
        return !empty($data) && is_array($data);
    }

    private function sheetValidator($data)
    {
        if (empty($data) || !is_array($data))
        {
            return false;
        }
        foreach ($data as $value)
        {
            if (!isset($value['title']) || !isset($value['headers']))
            {
                throw new CException(__METHOD__ . ' - incorrect sheet format: title or headers are missing', E_USER_ERROR);
            }
        }
        return true;
    }

    private function boolValidator($data)
    {
        return isset($data) && is_bool($data);
    }
}