<?php
$classes = array();

if($data->awards) $classes[] = 'dg-awards';
if($data->lynch) $classes[] = 'dg-game-lynch';
if($data->gammplay) $classes[] = 'dg-gamm-play';

$platforms = unserialize($data->platforms);
foreach($platforms as $platform) {
    $classes[] = 'dg-platform-'.$platform['id'];
}

$technologies = unserialize($data->technology);
foreach($technologies as $technology){
    if($technology['title'] == 'Other'){
        continue;
    }
    $classes[] = 'dg-technology-'.$technology['id'];
}
?>
<div class="dg-game <?= implode($classes, ' '); ?>" <?php if(!$data->approved) echo 'style="opacity: 0.7;"' ?>>
    <div class="dg-game-ico">
        <?php
            if($data->icon){
                echo CHtml::link('<img src="http://'.$_SERVER['SERVER_NAME'].$data->icon.'" />', array('awards/gameview', 'id'=>$data->id, '_conference'=>Yii::app()->conf), array('class'=>'game-url', 'data-id'=>$data->id));
            }else{
                echo CHtml::link('<img src="http://'.$_SERVER['SERVER_NAME'].'/i/awards/ico/game_icon.png" />', array('awards/gameview', 'id'=>$data->id, '_conference'=>Yii::app()->conf), array('class'=>'game-url', 'data-id'=>$data->id));
            }
        ?>
    </div>
    <div class="dg-game-info">
        <div class="dg-game-title"><?= $data->title; ?></div>
        <div class="dg-game-description"><?= $data->description; ?></div>
        <div>
            <?= CHtml::link('game', array('awards/gameview', 'id'=>$data->id, '_conference'=>Yii::app()->conf), array('class'=>'dg-button game-url', 'data-id'=>$data->id)); ?>
            <ul class="dg-game-submitted">
                <?= ($data->awards)? '<li class="dg-awards"><a title="DevGAMM Awards Nominee"></a></li>' : '' ?>
                <?= ($data->lynch)? '<li class="dg-game-lynch"><a title="Game Lynch Candidate"></a></li>' : '' ?>
                <?= ($data->gammplay)? '<li class="dg-gamm-play"><a title="GAMM:Play Showcase"></a></li>' : '' ?>
            </ul>
        </div>
    </div>
</div>