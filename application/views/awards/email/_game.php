<div><b>Ticket:</b> <?php echo chunk_split($game->ticket, 4); ?></div>
<div><b>Game title:</b> <?php echo $game->title; ?></div>

<div><b>Game description:</b></div>
<div><?php echo $game->description; ?></div>

<div><b>Author:</b> <?php echo $game->author; ?></div>
<div><b>Email:</b> <?php echo $game->email; ?></div>
<div><b>Phone:</b> <?php echo $game->phone; ?></div>
<div><b>Location:</b> <?php echo $game->location; ?></div>
<div><b>Links:</b></div>
<?php
$link = unserialize($game->link);
foreach ($link as $title=>$value) {
    if($value){
        echo "<div>$title: <a href='$value'>$value</a></div>";
    }
}
?>
<div><b>Video:</b> <?php echo $game->video; ?></div>

<div><b>Lynch:</b> <?php echo ($game->lynch)? 'yes' : 'no' ?></div>
<div><b>Gammplay:</b> <?php echo ($game->gammplay)? 'yes' : 'no' ?></div>
<div><b>Awards:</b> <?php echo ($game->awards)? 'yes' : 'no' ?></div>

<div><b>Platforms:</b> <?php echo $game->platform; ?></div>

<p>For more info go to console.</p>
