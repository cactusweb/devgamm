<?php
$screenshoots = explode(',', $game->screenshoots);
$links = unserialize($game->link);
$gamelinks = array();

$platforms = unserialize($game->platforms);
foreach($platforms as $platform){
    if(isset($platform['childs']) && is_array($platform['childs']) && count($platform['childs'])){
        foreach($platform['childs'] as $subplatform){
            if(trim($subplatform['link'])){
                $platformModel = GameCategoryNew::model()->findByPk($subplatform['id']);
                if($platformModel && $platformModel->icon){
                    $subplatform['icon'] = $platformModel->icon;
                }
                $gamelinks[] = $subplatform;
            }
        }
    }
}

if(strpos($game->video, 'youtu')){
    if(strpos($game->video, '/embed/')){// /embed/video_id
        if(preg_match("|^.+/embed/(.*)|i", $game->video, $matches)){
            $yt_video = $matches[1];
        }
    }elseif(strpos($game->video, '/watch')){// /watch?v=video_id
        if(preg_match("|^.+v=(.*)|i", $game->video, $matches)){
            $yt_video = $matches[1];
        }
    }else{// youtu.be/video_id
        if(preg_match("|^.*youtu.be/(.*)|i", $game->video, $matches)){
            $yt_video = $matches[1];
        }
    }
}
?>
<div class="dg-content-wrapper" style="padding: 0">
    <h3><?= $game->title; ?></h3>
    <div class="clearfix">
        <div class="dg-col_1">
            <div class="dg-media_active">
                <?php if(isset($yt_video)):  ?>
                    <iframe src="http://www.youtube.com/embed/<?=$yt_video;?>" frameborder="0"></iframe>
                <?php else: ?>
                    <img src="<?= 'http://'.$_SERVER['SERVER_NAME'].$screenshoots[0]; ?>" />
                <?php endif; ?>
            </div>
            <div class="dg-medialist">
                <?php if(isset($yt_video)): ?>
                    <div class="dg-media">
                        <div class="dg-overlay"></div>
                        <iframe src="http://www.youtube.com/embed/<?=$yt_video;?>" frameborder="0"></iframe>
                    </div>
                <?php endif; ?>
                <?php
                $screenshoots = explode(',', $game->screenshoots);
                foreach($screenshoots as $screenshot):
                    ?>
                    <div class="dg-media">
                        <div class="dg-overlay"></div>
                        <img src="<?= 'http://'.$_SERVER['SERVER_NAME'].$screenshot; ?>" />
                    </div>
                <?php endforeach ?>
            </div>
            <div>
                <h3>Summary</h3>
                <p>
                    <b>Title:</b> <?= $game->title; ?>
                </p>
                <p>
                    <b>Description:</b> <?= $game->description; ?>
                </p>
                <?= (!isset($yt_video))? '<p><b>Gameplay video:</b> <a href="'.$game->video.'" target="_blank">'.$game->video.'</a></p>': ''; ?>
                <p>
                    <b>Author:</b> <?= $game->author; ?><br/>
                    <b>Website:</b> <a href="<?= $links['company']; ?>" target="_blank"><?= $links['company']; ?></a><br/>
                    <b>Location:</b> <?= $game->location; ?><br/>
                </p>
                <p>
                    <div>
                    <b>Stage of development:</b>
                        <?php
                            switch($game->stage){
                                case 1:
                                    echo 'prototype';
                                    break;
                                case 2:
                                    echo 'alpha';
                                    break;
                                case 3:
                                    echo 'beta';
                                    break;
                                case 4:
                                    echo 'completed';
                                    break;
                                case 5:
                                    echo 'released';
                                    break;
                            }
                        ?>
                    </div>
                    <div>
                        <b>Technology:</b>
                        <?php
                        $technologies = unserialize($game->technology);
                        $technologiesArray = array();
                        foreach($technologies as $technology){
                            $technologiesArray[] = (!empty($technology['comment']))? $technology['title'].': '.$technology['comment'] : $technology['title'];
                        }
                        echo implode(', ', $technologiesArray);
                        ?>
                    </div>
                </p>
                <p>
                    <div><b>Free-to-play:</b> <?= ($game->f2p)? 'yes' : 'no'; ?></div>
                    <div><b>Multiplayer:</b> <?= ($game->multiplayer)? 'yes' : 'no'; ?></div>
                </p>

                <?php $this->renderPartial('../participant/_dgbp_start', array('participant'=>'partner')); ?>

                <p ng-init="edit_own_data=false">
                    <img align="left" style="padding-right:5px;" alt="" src="<?= 'http://'.$_SERVER['SERVER_NAME']; ?>/i/mail_icon.png"/>
                    <a title="<?= $partner->name_en; ?>" ng-click="makeAppointment('<?= $partner->id; ?>', 'partner')">
                        <?= Yii::t('app', 'Email developer'); ?></a>
                </p>
                <?php $this->renderPartial('../participant/_dgbp_end'); ?>

            </div>
        </div>
        <div class="dg-col_2">
            <div class="dg-gameinfo-block text-center">
                <p>
                    <ul class="dg-game-submitted">
                        <?= ($game->awards)? '<li class="dg-awards"><a title="DevGAMM Awards Nominee"></a></li>' : ''; ?>
                        <?= ($game->lynch)? '<li class="dg-game-lynch"><a title="Game Lynch Candidate"></a></li>' : ''; ?>
                        <?= ($game->gammplay)? '<li class="dg-gamm-play"><a title="GAMM:Play Showcase"></a></li>' : ''; ?>
                    </ul>
                </p>
                <p>
                    <?php if($game->icon): ?>
                        <img src="<?= 'http://'.$_SERVER['SERVER_NAME'].$game->icon; ?>" />
                    <?php else: ?>
                        <img src="<?= 'http://'.$_SERVER['SERVER_NAME']; ?>/i/awards/ico/game_icon.png" />
                    <?php endif; ?>
                </p>
                <p><b><?= $game->title; ?></b></p>
                <div class="dg-socials">
                    <a onclick="Share.vkontakte('<?=$game->title;?>')"><img src="http://w.sharethis.com/images/vkontakte_32.png" /></a>
                    <a onclick="Share.facebook('<?=$game->title;?>')"><img src="http://w.sharethis.com/images/facebook_32.png" /></a>
                    <a onclick="Share.twitter('<?=$game->title;?>')"><img src="http://w.sharethis.com/images/twitter_32.png" /></a>
                    <a onclick="Share.google('<?=$game->title;?>')"><img src="http://w.sharethis.com/images/googleplus_32.png" /></a>
                </div>
            </div>
            <?php foreach($gamelinks as $link): ?>
                <?php if(isset($link['public_link']) && $link['public_link'] == 0) continue; ?>
                <div class="dg-gameinfo-block text-center">
                    <p>
                        <b><?= $link['title'] ?> version</b>
                    </p>
                    <p style="position: relative;">
                        <?php if(isset($link['icon']) && $link['icon']): ?>
                            <img src="<?= 'http://'.$_SERVER['SERVER_NAME'].$link['icon'] ?>" class="version-ico" />
                        <?php endif; ?>
                        <a href="<?= $link['link'] ?>" target="_blank" class="dg-button">play</a>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <br/>
    <div class="clearfix">
        <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'devgamm';

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    </div>
</div>
<script>
    $('document').ready(function () {
        /** change main photo */
        $('.dg-media>.dg-overlay').click(function (){
            $('.dg-media_active').html($(this).parent().html());
            $('.dg-media').removeClass('dg-dim');
            $(this).parent().addClass('dg-dim');
        });

        $('.dg-medialist>div:first').addClass('dg-dim');

        var shareUrl = (window.location != window.parent.location) ? document.referrer: document.location;
        Share = {
            vkontakte: function(ptitle, pimg, text) {
                url  = 'http://vkontakte.ru/share.php?';
                url += 'url='          + encodeURIComponent(shareUrl);
                url += '&title='       + encodeURIComponent(ptitle);
/**                url += '&description=' + encodeURIComponent(text);
               url += '&image='       + encodeURIComponent(pimg);  */
                url += '&noparse=true';
                Share.popup(url);
            },
            odnoklassniki: function(text) {
                url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
                url += '&st.comments=' + encodeURIComponent(text);
                url += '&st._surl='    + encodeURIComponent(shareUrl);
                Share.popup(url);
            },
            facebook: function(purl, ptitle, pimg, text) {
                url  = 'http://www.facebook.com/sharer.php?s=100';
                url += '&p[title]='     + encodeURIComponent(ptitle);
/**                url += '&p[summary]='   + encodeURIComponent(text); */
                url += '&p[url]='       + encodeURIComponent(shareUrl);
/**                url += '&p[images][0]=' + encodeURIComponent(pimg); */
                Share.popup(url);
            },
            twitter: function(ptitle) {
                url  = 'http://twitter.com/share?';
                url += 'text='      + encodeURIComponent(ptitle);
                url += '&url='      + encodeURIComponent(shareUrl);
                url += '&counturl=' + encodeURIComponent(shareUrl);
                Share.popup(url);
            },
            mailru: function(ptitle, pimg, text) {
                url  = 'http://connect.mail.ru/share?';
                url += 'url='          + encodeURIComponent(shareUrl);
                url += '&title='       + encodeURIComponent(ptitle);
/**                url += '&description=' + encodeURIComponent(text); */
/**                url += '&imageurl='    + encodeURIComponent(pimg); */
                Share.popup(url)
            },
            google: function() {
                url  = 'https://plus.google.com/share?';
                url += 'url='          + encodeURIComponent(shareUrl);
                Share.popup(url)
            },

            popup: function(url) {
                window.open(url,'','toolbar=0,status=0,width=626,height=436');
            }
        };

    });
</script>