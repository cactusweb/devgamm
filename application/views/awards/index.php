<?php
$classesBlue = array();
$classesRed = array();
$games = $dataProvider->getData();

foreach($games as $game){
    if($game->awards) $classesRed['dg-awards'] = 'awards';
    if($game->lynch) $classesRed['dg-game-lynch'] = 'game lynch';
    if($game->gammplay) $classesRed['dg-gamm-play'] = 'gamm:play';

    $platforms = unserialize($game->platforms);

    if(is_array($platforms)){
        foreach($platforms as $platform) {
            $classesBlue['platform-'.$platform['id']] = $platform['title'];
        }
    }

    $technologies = unserialize($game->technology);

    foreach($technologies as $technology){
        if($technology['title'] == 'Other'){
            continue;
        }
        $classesBlue['technology-'.$technology['id']] = $technology['title'];
    }
}
//ksort($classesBlue);
?>
<div class="dg-content-wrapper dg-page">
<div class="dg-tags filter option-set clearfix " data-filter-group="blue">
    <a href="#" class="dg-active" data-filter="*" >All</a>
    <?php
    foreach($classesBlue as $class => $label) {
        echo '<a href="#" data-filter-value=".dg-'.$class.'">'.$label.'</a>';
    }
    ?>
</div>
<div class="dg-tags dg-tag-type filter option-set clearfix " data-filter-group="red">
    <a href="#" class="dg-active" data-filter="*" >All</a>
    <?php foreach($classesRed as $class => $label) echo '<a href="#" data-filter-value=".'.$class.'">'.$label.'</a>'; ?>
</div>
<div class="dg-sepline" style="clear: none;"></div>

<div class="dg-games" id="dg-games" style="width: 100%; display: inline-block;">
    <?php
    $data = $dataProvider->getData();
    foreach($data as $i => $item)
        Yii::app()->controller->renderPartial('_game',
            array('index' => $i, 'data' => $item, 'widget' => $this));
    ?>
</div>

<div style="height: 100px;"></div>

<script>
    $(document).ready(function(){
        var $container = $('#dg-games');
        var filters = {};

        $container.isotope({
            itemSelector: '.dg-game',
            masonry: {
                columnWidth: 380
            }
        });

        /** filter buttons */
        $('.filter a').click(function(){
            var $this = $(this);
            /** don't proceed if already selected */
            if ( $this.hasClass('dg-active') ) {
                return false;
            }

            var $optionSet = $this.parents('.option-set');
            /** change selected class */
            $optionSet.find('.dg-active').removeClass('dg-active');
            $this.addClass('dg-active');

            /** store filter value in object
             i.e. filters.color = 'red' */
            var group = $optionSet.attr('data-filter-group');
            filters[ group ] = $this.attr('data-filter-value');
            /** convert object into array */
            var isoFilters = [];
            for ( var prop in filters ) {
                isoFilters.push( filters[ prop ] )
            }
            var selector = isoFilters.join('');
            console.log(selector);
            $container.isotope({ filter: selector });

            return false;
        });


        $('.game-url').each(function(){
            var gameId = $(this).data('id');
            var url = (window.location != window.parent.location) ? document.referrer: document.location;

            if(url.toString().indexOf('?') > 0){
                url += '&gameId='+gameId;
            }else{
                url += '?gameId='+gameId;
            }
            var d = document.createElement('div');
            url = $(d).html(url).text();
            $(this).attr('href', url);
        });

    });
</script>