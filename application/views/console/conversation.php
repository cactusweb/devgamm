<div class="container-fluid" id="communication">
    <div class="hero-unit">
        <h3><?= ucfirst($type); ?> communication</h3>
    </div>
    <div class="row-fluid">
        <table class="table table-striped table-hover">
            <thead>
                <tr><th></th><th>Member</th><th>Member email</th><th>Author</th><th>Author email</th><th>Link</th><th>Message</th><th>Date</th></tr>
            </thead>
            <tbody>
                <?php if (!empty($data) && is_array($data)) foreach ($data as $v) { ?>
                    <tr id="c<?= $v['id']; ?>">
                        <td><a href="" data-type="remove" onclick="return false;"><span class="icon-trash"> </span></a></td>
                        <td><?= $v['member']; ?></td>
                        <td><?= $v['member_email']; ?></td>
                        <td><?= $v['author']; ?></td>
                        <td><?= $v['author_email']; ?></td>
                        <td><?= $v['link']; ?></td>
                        <td><?= $v['message']; ?></td>
                        <td><?= $v['date']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
