<?php Yii::app()->clientScript
    ->registerScriptFile('activity.js')
    ->registerScriptFile('spectrum.js')
    ->registerCssFile('spectrum.css')
    ->registerScriptFile('categories.js'); ?>

<div class="container-fluid">
    <div class="hero-unit">
    <h3 data-type="discussionpanels" data-action="save_discussion_category">Discussion categories</h3>
    </div>
    <?php if (!empty($data)) foreach ($data as $row) { ?>
        <?php $this->widget('Participant', array('type' => 'discussion_category', 'data' => $row)); ?>
    <?php } ?>
    <?php $this->widget('Participant', array('type' => 'discussion_category')); ?>
</div>