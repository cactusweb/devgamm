<?php Yii::app()->clientScript
    ->registerScriptFile('activity.js')
    ->registerScriptFile('games.js'); ?>

<div class="container-fluid">
    <div class="hero-unit" data-image-url="//devgamm.com/<?= Yii::app()->Core->confDir . '/images/'; ?>">
        <h3>Games: <?= $type; ?></h3>
    </div>
    <?php if (!empty($data)) foreach ($data as $row) { ?>
        <?php
        if(!$row['ticket_id']){
            $this->widget('Participant', array('data' => $row, 'type' => 'game'));
        }else{
            $this->widget('Participant', array('data' => $row, 'type' => 'game_v2'));
        }
        ?>
    <?php } ?>
</div>