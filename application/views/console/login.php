<div>
    <?= CHtml::beginForm('', 'post', array('class' => 'form-signin')); ?>
    <h2 class="form-signin-heading">Please sign in</h2>
    <?= CHtml::activeTextField($model, 'userName', array('class' => 'input-block-level', 'placeholder' => 'Login')); ?>
    <?= CHtml::error($model, 'userName'); ?>
    <?= CHtml::activePasswordField($model, 'password', array('class' => 'input-block-level','placeholder' => 'Password')); ?>
    <?= CHtml::error($model, 'password'); ?>
    <label class="checkbox">
        <?= CHtml::activeCheckBox($model, 'rememberMe'); ?>
        Remember me
    </label>
    <?= CHtml::submitButton('Sign in', array('class' => 'btn btn-middle btn-primary')); ?>
    <?= CHtml::endForm(); ?>
</div>