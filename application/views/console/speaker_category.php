<?php Yii::app()->clientScript
    ->registerScriptFile('activity.js')
    ->registerScriptFile('spectrum.js')
    ->registerCssFile('spectrum.css')
    ->registerScriptFile('categories.js')
    ->registerScriptFile('speaker.js');?>

<div class="container-fluid">
    <div class="hero-unit">
    <h3 data-type="speakers" data-action="save_speaker_category">Speaker categories</h3>
    </div>
    <?php if (!empty($data)) foreach ($data as $row) { ?>
        <?php $this->widget('Participant', array('type' => 'speaker_category', 'data' => $row)); ?>
    <?php } ?>
    <?php $this->widget('Participant', array('type' => 'speaker_category')); ?>
</div>