<?php Yii::app()->clientScript
    ->registerScriptFile('activity.js')
    ->registerScriptFile('participants.js'); ?>

<div class="container-fluid">
    <div class="hero-unit" data-image-url="//devgamm.com/<?= Yii::app()->Core->confDir . '/images/'; ?>">
    <h3><?= $type; ?></h3>
    </div>
    <?php if (!empty($data)) foreach ($data as $row) { ?>
        <?php $this->widget(ucfirst($type), array('data' => $row)); ?>
    <?php } ?>
    <?php $this->widget(ucfirst($type)); ?>
</div>
