<?php Yii::app()->clientScript->registerCssFile('game.form.css')
                              ->registerCoreScript('jquery')
                              ->registerPackage('fancybox')
                              ->registerScriptFile('game.form.js'); ?>
<div class="wrap">
    <div class="selectForms<?= !empty($_POST) ? ' loaded' : ''; ?>">
        <?php
            foreach (Yii::app()->Core->gameCategories as $categoryId => $categoryData) { ?>
                <p><input class="form-type" type="radio" name="SF_contest_selectForm" id="SF_contest_selectForm_<?=$categoryData['name'];?>" value="<?=$categoryId;?>" <?= (isset($_POST['category_id']) && $_POST['category_id'] == $categoryId) ? 'checked="checked"' : ''; ?> /> <label for="SF_contest_selectForm_<?=$categoryData['name'];?>"><?=$categoryData['title'];?></label></p>
           <?php } ?>
        <div class="clear"></div>
    </div>
    <form action="<?= Yii::app()->createUrl('game/save'); ?>" method="post" enctype="multipart/form-data" id="SF_sform_flash">
        <input type="hidden" name="category_id" value="" />
        <div class="formRow">
            <div class="leftTitle">
                <p class="fieldReq">* <?= Yii::t('gameform', 'ticket'); ?></p>
                <span class="subtext"><a href="<?= Yii::app()->Core->devgammHost . Yii::app()->Core->confDir . '/images/ticket_number.png'; ?>" class="faq"><?= Yii::t('gameform', 'faq'); ?></a></span>
            </div>
            <div class="leftField">
                <input class="ticket-code" type="text" maxlength="4" name="ticket[]" value="" placeholder="0517"> &#151;
                <input class="ticket-code" type="text" maxlength="4" name="ticket[]" value="" placeholder="5877"> &#151;
                <input class="ticket-code" type="text" maxlength="4" name="ticket[]" value="" placeholder="9140"> &#151;
                <input class="ticket-code" type="text" maxlength="4" name="ticket[]" value="" placeholder="0792">
                <label id="error" style="display:none; color:red;">Error text</label>
                <?= isset($errors['ticket']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['ticket']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'gameName'); ?></p></div>
            <div class="leftField">
                <input type="text" name="title" value="<?= (isset($_POST['title']))?htmlentities($_POST['title'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['title']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['title']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'shortDesc'); ?></p><span class="subtext"><?= Yii::t('gameform', 'to250'); ?></span></div>
            <div class="leftField">
                <textarea name="description" id="flash_gamesdsc" onkeyup="txtcounter('flash_gamesdsc', 250);"><?= (isset($_POST['description']))?htmlentities($_POST['description'], ENT_COMPAT, 'UTF-8'):''; ?></textarea>
                <div id="flash_gamesdsc_count">250</div>
                <?= isset($errors['description']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['description']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>

        <div class="formRow flash-type">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'gameFile'); ?></p><span class="subtext"><?= Yii::t('gameform', 'gameFileTypes'); ?></span></div>
            <div class="leftField"><input type="file" name="file_ext" />
                <?= isset($errors['file_ext']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['file_ext']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow social-type">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'gameURL'); ?></p></div>
            <div class="leftField"><input type="text" name="gameurl" value="<?= (isset($_POST['gameurl']))?htmlentities($_POST['gameurl'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['gameurl']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['gameurl']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow mobile-type">
            <div class="leftTitle">
                <p class="fieldReq">* <?= Yii::t('gameform', 'gameURL'); ?></p>
                <?= isset($errors['link']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['link']) . '</div>' : ''; ?>
            </div>
        </div>
        <div class="formRow mobile-type">
            <div class="leftTitle">App Store</div>
            <div class="leftField">
                 <input type="text" name="link[appstore]" value="<?= (isset($_POST['link']['appstore'])) ? htmlentities($_POST['link']['appstore'], ENT_COMPAT, 'UTF-8'):''; ?>" />
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow mobile-type">
            <div class="leftTitle">Google Play</div>
            <div class="leftField">
                 <input type="text" name="link[gplay]" value="<?= (isset($_POST['link']['gplay'])) ? htmlentities($_POST['link']['gplay'], ENT_COMPAT, 'UTF-8'):''; ?>" />
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow mobile-type">
            <div class="leftTitle">Web</div>
            <div class="leftField">
                 <input type="text" name="link[web]" value="<?= (isset($_POST['link']['web'])) ? htmlentities($_POST['link']['web'], ENT_COMPAT, 'UTF-8'):''; ?>" />
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow mobile-type">
            <div class="leftTitle"><?= Yii::t('gameform', 'other'); ?></div>
            <div class="leftField">
                 <input type="text" name="link[other]" value="<?= (isset($_POST['link']['other'])) ? htmlentities($_POST['link']['other'], ENT_COMPAT, 'UTF-8'):''; ?>" />
            </div>
            <div class="sclear"></div>
        </div>

        <div class="formRow flash-type">
            <p class="label"><?= Yii::t('gameform', 'gameFileInfo'); ?></p>
        </div>

        <div class="formRow steam-type">
            <div class="leftTitle"><p class="fieldReq"> <?= Yii::t('gameform', 'gameURL'); ?> (Win)</p></div>
            <div class="leftField"><input type="text" name="link[win]" value="<?= (isset($_POST['link']['win']))?htmlentities($_POST['link']['win'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['link']['win']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['link']['win']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>

        <div class="formRow steam-type">
            <div class="leftTitle"><p class="fieldReq"> <?= Yii::t('gameform', 'gameURL'); ?> (Mac)</p></div>
            <div class="leftField"><input type="text" name="link[mac]" value="<?= (isset($_POST['link']['mac']))?htmlentities($_POST['link']['mac'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['link']['mac']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['link']['mac']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>

        <div class="formRow steam-type">
            <div class="leftTitle"><p class="fieldReq"> <?= Yii::t('gameform', 'gameURL'); ?> (Steam)</p></div>
            <div class="leftField"><input type="text" name="link[steam]" value="<?= (isset($_POST['link']['steam']))?htmlentities($_POST['link']['steam'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['link']['steam']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['link']['steam']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>

        <div class="formRow steam-type">
            <div class="leftTitle"><p class="fieldReq"> <?= Yii::t('gameform', 'steam code'); ?></p></div>
            <div class="leftField"><input type="text" name="steam_code" value="<?= (isset($_POST['steam_code']))?htmlentities($_POST['steam_code'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['steam_code']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['steam_code']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>

        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'gameVideo'); ?></p><span class="subtext"><?= Yii::t('gameform', 'gameVideoFrom'); ?></span></div>
            <div class="leftField"><input type="text" name="video" value="<?= (isset($_POST['video']))?htmlentities($_POST['video'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['video']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['video']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow flash-type">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'gameSize'); ?></p><span class="subtext"><?= Yii::t('gameform', 'gameSizeExample'); ?></span></div>
            <div class="leftField"><input type="text" name="dimension" value="<?= (isset($_POST['dimension']))?htmlentities($_POST['dimension'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['dimension']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['dimension']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'gameIcon'); ?></p><span class="subtext"><?= Yii::t('gameform', 'gameIconSize'); ?></span></div>
            <div class="leftField"><input type="file" name="icon_ext" />
                <?= isset($errors['icon_ext']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['icon_ext']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'gameAutor'); ?></p><span class="subtext"><?= Yii::t('gameform', 'gameAutorDesc'); ?></span></div>
            <div class="leftField"><input type="text" name="author" value="<?= (isset($_POST['author']))?htmlentities($_POST['author'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['author']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['author']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq"></div>
            <div class="leftField">
                <input type="radio" name="author_type" value="0" id="ind" <?= isset($_POST['author_type']) && $_POST['author_type'] == '0' ? 'checked="checked"' : ''; ?> /> <label for="ind"><?= Yii::t('gameform', 'indi'); ?></label><br>
                <input type="radio" name="author_type" value="1" id="stu" <?= isset($_POST['author_type']) && $_POST['author_type'] == '1' ? 'checked="checked"' : ''; ?>> <label for="stu" /><?= Yii::t('gameform', 'studio'); ?></label><br>
                <input type="radio" name="author_type" value="2" id="com" <?= isset($_POST['author_type']) && $_POST['author_type'] == '2' ? 'checked="checked"' : ''; ?>> <label for="com" /><?= Yii::t('gameform', 'company'); ?></label><br>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'stage'); ?></p></div>
            <div class="leftField">
                <input type="radio" name="stage" value="1" id="stage-f-1"<?= isset($_POST['stage']) && 1 == $_POST['stage'] ? ' checked="checked"' : ''; ?> />
                <label for="stage-f-1">Prototype</label><br>
                <input type="radio" name="stage" value="2" id="stage-f-2"<?= isset($_POST['stage']) && 2 == $_POST['stage'] ? ' checked="checked"' : ''; ?> />
                <label for="stage-f-2">Alfa</label><br>
                <input type="radio" name="stage" value="3" id="stage-f-3"<?= isset($_POST['stage']) && 3 == $_POST['stage'] ? ' checked="checked"' : ''; ?> />
                <label for="stage-f-3">Beta</label><br>
                <input type="radio" name="stage" value="4" id="stage-f-4"<?= isset($_POST['stage']) && 4 == $_POST['stage'] ? ' checked="checked"' : ''; ?> />
                <label for="stage-f-4">Completed</label><br>
                <?= isset($errors['stage']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['stage']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow mobile-type social-type">
            <div class="leftTitle">
                <p class="fieldReq">* <?= Yii::t('gameform', 'platform'); ?></p>
                <span class="subtext">Unity, Marmalade, Air, C++, Java, ...</span>
            </div>
            <div class="leftField">
                <input type="text" name="platform" value="<?= isset($_POST['platform']) ? htmlentities($_POST['platform'], ENT_COMPAT, 'UTF-8') : ''; ?>" />
                <?= isset($errors['platform']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['platform']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow mobile-type">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'f2p'); ?></p></div>
            <div class="leftField">
                <input type="radio" name="f2p" value="1" id="f2p-1"<?= isset($_POST['f2p']) && 1 == $_POST['f2p'] ? ' checked="checked"' : ''; ?> />
                <label for="f2p-1"><?= Yii::t('gameform', 'yes'); ?></label><br>
                <input type="radio" name="f2p" value="2" id="f2p-2"<?= isset($_POST['f2p']) && 2 == $_POST['f2p'] ? ' checked="checked"' : ''; ?> />
                <label for="f2p-2"><?= Yii::t('gameform', 'no'); ?></label>
                <?= isset($errors['f2p']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['f2p']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'email'); ?></p></div>
            <div class="leftField"><input type="text" name="email" value="<?= (isset($_POST['email']))?htmlentities($_POST['email'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['email']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['email']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'phone'); ?></p></div>
            <div class="leftField"><input type="text" name="phone" value="<?= (isset($_POST['phone']))?htmlentities($_POST['phone'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['phone']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['phone']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <div class="leftTitle"><p class="fieldReq">* <?= Yii::t('gameform', 'location'); ?></p></div>
            <div class="leftField">
                <input type="text" name="location" value="<?= (isset($_POST['location'])) ? htmlentities($_POST['location'], ENT_COMPAT, 'UTF-8'):''; ?>" />
                <?= isset($errors['location']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['location']) . '</div>' : ''; ?>
            </div>
            <div class="sclear"></div>
        </div>
        <div class="formRow">
            <p class="label"><?= Yii::t('gameform', 'contestRange_' . Yii::app()->Core->confDir); ?></p>
        </div>
        <div class="formRow">
            <input type="checkbox" name="lynch" id="lynch1"    <?= (isset($_POST['lynch']))?'checked="checked"' : ''; ?>/> <label for="lynch1"><?= Yii::t('gameform', 'addToLynch_' . Yii::app()->Core->confDir); ?></label>
        </div>
        <div class="formRow">
            <input type="checkbox" name="gammplay" id="gammplay-f"<?= isset($_POST['gammplay']) ? ' checked="checked"' : ''; ?> />
            <label for="gammplay-f"><?= Yii::t('gameform', 'gammplay_' . Yii::app()->Core->confDir); ?></label>
        </div>
        <div class="formRow">
            <input type="submit" name="send" value="<?= Yii::t('gameform', 'send'); ?>" />
            <div class="leftField">
                <?= isset($errors['global']) ? '<br/><div style="color:red;">' . Yii::t('gameform', $errors['global']) . '</div>' : ''; ?>
            </div>
        </div>
    </form>
</div>
