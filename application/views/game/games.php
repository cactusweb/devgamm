<?php Yii::app()->clientScript->registerCssFile('games.list.css'); ?>
<?php if (!empty($data) && is_array($data)) foreach ($data as $row) { if(empty($row['games'])) continue; ?>
    <div class="category <?= $row['category']['color']; ?>">
        <div class="category-title">
            <h3><?= $row['category']['title']; ?></h3>
            <div class="category-icons">
                <div class="icons">
                    <?php $icons = json_decode($row['category']['icons']); if (!empty($icons) && is_array($icons)) foreach ($icons as $i) { ?><img src="/i/<?= $i; ?>"><?php } ?>
                </div>
            </div>
        </div>
        <?php if (empty($row['games'])) { ?>
            </div></div>
        <?php continue; } ?>
        <div class="duo-games-block">
            <?php $i = 0; foreach ($row['games'] as $game) { ?>
                <?php if ($i && !($i % 2)) { ?>
                    </div><div class="duo-games-block">
                    <?php $i = 0; ?>
                <?php } $i++; ?>
                <div class="game-block">
                    <div class="left-side">
                        <div class="game-icon">
                            <a href="<?= Yii::app()->Core->getGameALink($game); ?>" target="_blank"><img border="0" src="<?= Yii::app()->Core->getGameIMGLink($game); ?>"/></a>
                        </div>
                        <?php if (isset($game['lynch']) && $game['lynch']) { ?>
                            <img class="game-lynch" src="/i/cg.png"/>
                        <?php } ?>
                        <?php if (isset($game['gammplay']) && $game['gammplay']) { ?>
                            <a href="/<?= Yii::app()->Core->confDir; ?>/games/gammplay"><img class="game-gammplay" src="/i/gammplay_logo.png"/></a>
                        <?php } ?>
                    </div>
                    <div class="right-side">
                        <p class="padded"><strong><?= Yii::t('app', 'Title'); ?></strong>: <a href="<?= Yii::app()->Core->getGameALink($game); ?>" target="_blank"><?= $game['title']; ?></a></p>
                        <p class="padded"><strong><?= Yii::t('app', 'Description'); ?></strong>: <?= $game['description']; ?></p>
                        <?php if (!empty($game['video'])) ?> <p><strong><?= Yii::t('app', 'Video'); ?></strong>: <a href="<?= $game['video']; ?>" target="_blank"><?= $game['title']; ?></a></p>
                        <p><strong><?= Yii::t('app', 'Author'); ?></strong>: <?= $game['author']; ?></p>
                        <p class="padded"><strong><?= Yii::t('app', 'Category'); ?></strong>: <?= Yii::t('app', isset(Yii::app()->Core->getGameAuthorTypes()[$game['author_type']]) ? Yii::app()->Core->getGameAuthorTypes()[$game['author_type']] : ''); ?></p>
                        <p><strong><?= Yii::t('gameform', 'stage'); ?></strong>: <?= Yii::t('app', isset(Yii::app()->Core->getGameDevelopmentStages()[$game['stage']]) ? Yii::app()->Core->getGameDevelopmentStages()[$game['stage']] : 'Alfa'); ?></p>
                        <?php if (in_array($game['category_id'], array(2, 4))) { ?>
                            <p><strong><?= Yii::t('gameform', 'platform'); ?></strong>: <?= isset($game['platform']) ? $game['platform'] : ''; ?></p>
                            <p class="padded"><strong><?= Yii::t('app', 'Free-2-play'); ?></strong>: <?= Yii::t('gameform', (!$game['f2p'] || 1 != $game['f2p'] ? 'no' : 'yes')); ?></p>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
