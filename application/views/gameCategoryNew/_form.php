<?php
/* @var $this GameCategoryNewController */
/* @var $model GameCategoryNew */
/* @var $form CActiveForm */
?>

<div class="form">

<?php

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'game-category-new-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data',
    ),
));

$models = GameCategoryNew::model()->findAll(array('condition'=>'parent_id IS NULL'));
$list = CHtml::listData($models, 'id', 'title');

?>

	<?php echo $form->errorSummary($model); ?>

	<div class="">
		<?php echo $form->labelEx($model,'parent_id'); ?>
        <?php echo CHtml::activeDropDownList($model, 'parent_id', $list, array('empty' => 'without parent')); ?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'icon'); ?>
		<?php echo $form->fileField($model,'icon'); ?>
		<?php echo $form->error($model,'icon'); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->