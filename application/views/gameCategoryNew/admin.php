<div class="container-fluid">
    <div class="container">
        <h1>Manage Game Platforms</h1>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'game-category-new-grid',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'id',
                array(
                    'name'=>'parent_id',
                    'value'=>'@$data->parent->title',
                ),
                'title',
                array(
                    'name'=>'icon',
                    'value'=>'$data->getIcon()',
                    'type'=>'html',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}'
                ),
            ),
        )); ?>
        <div>
            <?= CHtml::link('New Platform',array('gameCategoryNew/create'), array('class'=>'btn btn-default')); ?>
        </div>
    </div>
</div>


