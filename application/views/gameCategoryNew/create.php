<div class="container-fluid">
    <div class="container">
        <h1>Create Game Platform</h1>

        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>