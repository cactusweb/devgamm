<div class="container-fluid">
    <div class="container">
        <h1>Manage Game Nominations</h1>
        <div style="color: red;">*Sorting by "menu index"</div>
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'game-nomination-grid',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'id',
                'title',
                'menu_index',
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}'
                ),
            ),
        )); ?>

        <div>
            <?= CHtml::link('New nomination',array('gameNomination/create'), array('class'=>'btn btn-default')); ?>
        </div>
    </div>
</div>