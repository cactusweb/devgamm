<?php
/* @var $this GameTechnologyController */
/* @var $model GameTechnology */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'game-technology-form',
	'enableAjaxValidation'=>false,
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row" style="display: inline-block; width: 100%; margin-bottom: 10px;">
        <?php echo $form->checkBox($model,'need_comment',  array('style'=>'float: left; margin-right: 5px;')); ?>
		<?php echo $form->labelEx($model,'need_comment'); ?>
		<?php echo $form->error($model,'need_comment'); ?>
	</div>

    <?php if(!$model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'comment'); ?>
            <?php echo $form->textField($model,'comment',array('size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'comment'); ?>
        </div>
    <?php endif; ?>

	<div class="row">
		<?php echo $form->labelEx($model,'icon'); ?>
		<?php echo $form->fileField($model,'icon',array()); ?>
		<?php echo $form->error($model,'icon'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->