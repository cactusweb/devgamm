<div class="container-fluid">
    <div class="container">
        <h1>Manage Game Technologies</h1>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'game-technology-grid',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'id',
                'title',
                'need_comment',
                'comment',
                array(
                    'name'=>'icon',
                    'value'=>'$data->getIcon()',
                    'type'=>'html',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}'
                ),
            ),
        )); ?>
        <div>
            <?= CHtml::link('New technology',array('gameTechnology/create'), array('class'=>'btn btn-default')); ?>
        </div>
    </div>
</div>
