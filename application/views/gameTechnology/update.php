<div class="container-fluid">
    <div class="container">
        <h1>Update GameTechnology <?php echo $model->id; ?></h1>

        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>