<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-us" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="https://www.facebook.com/2008/fbml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::app()->charset; ?>" />

        <title><?= CHtml::encode($this->pageTitle); ?></title>

        <?php
        Yii::app()->clientScript->registerCoreScript('jquery')
                                ->registerPackage('bootstrap')
                                ->registerCssFile('login.css');
        ?>
    </head>
    <body>

    <div>
      <?=$content?>
    </div>

  </body>

</html>