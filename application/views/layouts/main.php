<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-us">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::app()->charset; ?>" />
        <title><?= CHtml::encode($this->pageTitle); ?></title>
        <?php Yii::app()->clientScript->registerCoreScript('jquery')
                                      ->registerCoreScript('ckeditor')
                                      ->registerPackage('bootstrap')
                                      ->registerCssFile('admin.css'); ?>
    </head>
    <body>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="/"><?= Yii::app()->name; ?>, <?= Yii::app()->Core->conf; ?></a>
            <ul class="nav">
                <?php if ($this->isUserHasAccess('games')) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Games <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::app()->createUrl('console/games', array('action' => 'list', 'type' => 'all')); ?>">All</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/games', array('action' => 'list', 'type' => 'approved')); ?>">Approved</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/games', array('action' => 'list', 'type' => 'notapproved')); ?>">Not approved</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/games', array('action' => 'list', 'type' => 'lynch')); ?>">Lynch</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/games', array('action' => 'list', 'type' => 'gammplay')); ?>">GaMM:Play</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/games', array('action' => 'list', 'type' => 'categories')); ?>">Game categories</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/games', array('action' => 'getCSV')); ?>">Export games as csv</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($this->isUserHasAccess('participants')) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Experts <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::app()->createUrl('console/experts', array('action' => 'list')); ?>">List</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/experts', array('action' => 'conversation')); ?>">Conversations</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($this->isUserHasAccess('sponsors')) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sponsors <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::app()->createUrl('console/sponsors', array('action' => 'list')); ?>">List</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/sponsors', array('action' => 'conversation')); ?>">Conversations</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($this->isUserHasAccess('partners')) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Partners <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::app()->createUrl('console/partners', array('action' => 'list')); ?>">List</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/partners', array('action' => 'conversation')); ?>">Conversations</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($this->isUserHasAccess('speakers')) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Speakers <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::app()->createUrl('console/speakers', array('action' => 'list')); ?>">List</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/speakers', array('action' => 'conversation')); ?>">Conversations</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/speakers', array('action' => 'categories')); ?>">Categories</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($this->isUserHasAccess('discussion_panels')) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Discussion Panels <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= Yii::app()->createUrl('console/discussionpanels', array('action' => 'list')); ?>">Participants</a></li>
                            <li><a href="<?= Yii::app()->createUrl('console/discussionpanels', array('action' => 'categories')); ?>">Categories</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Showcase <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= Yii::app()->createUrl('showcase/update'); ?>">Udate</a></li>
                        <li><a href="<?= Yii::app()->createUrl('showcase/index'); ?>">View</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Game info <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= Yii::app()->createUrl('gameCategoryNew/admin'); ?>">Platforms</a></li>
                        <li><a href="<?= Yii::app()->createUrl('gameTechnology/admin'); ?>">Technology</a></li>
                        <li><a href="<?= Yii::app()->createUrl('gameNomination/admin'); ?>">Nomination</a></li>
                    </ul>
                </li>
                <li><a href="<?= Yii::app()->createUrl('settings/admin'); ?>">Settings</a></li>
            </ul>
            <ul class="nav pull-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Conferences <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php foreach (Yii::app()->Core->conferences as $k => $v) { ?>
                            <li><a href="/?_conference=<?= $k; ?>"><?= $v; ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="divider-vertical"></li>
                <li><a href="<?=Yii::app()->createUrl('console/logout')?>">Logout</a></li>
                <li class="divider-vertical"></li>
                <li>
                    <p class="navbar-text">working as: <b><?=Yii::app()->user->id;?></b> </p>
                </li>
            </ul>
        </div>
    </div>
    <div>
      <?=$content?>
    </div>
  </body>
</html>