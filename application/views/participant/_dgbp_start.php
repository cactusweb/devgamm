<div ng-app="DGBP" ng-controller="ParticipantsCtrl">
    <?php if($participant == 'partner') echo '<p><a ng-show="edit_own_data" ng-click="formEdit.edit()">Edit your data</a></p>'; ?>
    <div id="identity" class="identify-form-wrapper modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Log in, please</h4>
                </div>
                <form class="form-horizontal" name="formIdentity" ng-submit="doLogin()">
                    <div class="modal-body">
                        <div class="form-group required">
                            <label class="col-sm-4 control-label">Your ticket number</label>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="text" name="ticketNumber" class="form-control"
                                               ng-required="true" ng-model="identity.ticket"
                                               ng-pattern="/^\d+$/">
                                    </div>
                                </div>
                                <div ng-show="formIdentity.ticketNumber.$touched && formIdentity.ticketNumber.$invalid"
                                     class="error">Please specify ticket number
                                </div>
                                <div
                                    ng-show="formIdentity.ticketNumber.$touched && formIdentity.ticketNumber.$error.pattern"
                                    class="error">Only digits please
                                </div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-4 control-label">Your email</label>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="email" name="email" class="form-control"
                                               ng-required="true" ng-model="identity.email"
                                               ng-pattern="/^[^А-Яа-яіє]*$/">
                                    </div>
                                </div>
                                <div ng-show="formIdentity.email.$touched && formIdentity.email.$invalid" class="error">
                                    Please specify your email
                                </div>
                                <div ng-show="formIdentity.email.$error.pattern" class="error">Only latin symbols and
                                    digits
                                    please
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <div class="error" ng-show="formIdentity.showErrorMessage">
                                    {{formIdentity.message}}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                </button>
                                <button ng-disabled="formIdentity.$invalid" type="submit" class="btn btn-success">
                                    Continue
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="bp-data" class="bp-form-wrapper" ng-show="formEdit.open">
        <form class="form-horizontal" name="formBPData" ng-submit="formEdit.submit()">
            <div class="form-group">
                <label class="col-sm-4 control-label">
                    Photo
                    <span class="hint">75x75px, jpg/png</span>
                </label>
                <div class="col-sm-4">
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <p>
                                <img ng-src="{{BPData.photo_url}}">
                            </p>
                            <button type="button" valid-file type="file" name="photo"
                                    ng-model="BPData.photo"
                                    ng-file-select
                                    multiple="false"
                                    accept="'image/jpeg,image/png'"
                                    ng-accept="'image/jpeg,image/png'"
                                    resetOnClick="false"
                                >Change photo...
                            </button>
                            <div ng-show="formEdit.photo_error" class="error">Size of photo should be 75x75 pixels.</div>
                            <div class="upload_status" ng-show="formEdit.upload && formEdit.filePreloader"></div>
                        </div>
                    </div>
                    <div ng-show="game.error.screenshots" class="error">Wrong image format, please add JPG or PNG</div>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-4 control-label">
                    I'm looking for
                    <span class="hint">max 140 symbols</span>
                    <span class="hint">{{140-BPData.looking_for.length}}</span>
                </label>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="looking_for" class="form-control" required="1" rows="4" maxlength="140"
                                      ng-model="BPData.looking_for" ng-pattern="/^[^А-Яа-яіє]*$/"></textarea>
                        </div>
                    </div>
                    <div ng-show="formBPData.looking_for.$touched && formBPData.looking_for.$invalid" class="error">
                        Please
                        specify what are you looking for?
                    </div>
                    <div ng-show="formBPData.looking_for.$error.pattern" class="error">Only latin symbols and digits
                        please
                    </div>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-4 control-label">
                    I offer
                    <span class="hint">max 140 symbols</span>
                    <span class="hint">{{140-BPData.offer.length}}</span>
                </label>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="offer" class="form-control" required="1" rows="4" maxlength="140"
                                      ng-model="BPData.offer" ng-pattern="/^[^А-Яа-яіє]*$/"></textarea>
                        </div>
                    </div>
                    <div ng-show="formBPData.offer.$touched && formBPData.offer.$invalid" class="error">Please
                        specity what do you offer?
                    </div>
                    <div ng-show="formBPData.offer.$error.pattern" class="error">Only latin symbols and digits
                        please
                    </div>
                </div>
            </div>
            <div class="form-group required">
                <label class="col-sm-4 control-label">
                    Website
                    <span class="hint"></span>
                </label>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="url" class="form-control" name="website"
                                   ng-required="true" ng-model="BPData.website" valid-link>
                        </div>
                    </div>
                    <div ng-show="formBPData.website.$touched && formBPData.website.$invalid" class="error">Please add
                        valid
                        link to your website
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" class="btn btn-default" ng-click="formEdit.hide()">Cancel</button>
                    <button ng-disabled="formBPData.$invalid" type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </form>
        <div style=" width: 740px; margin: 10px auto 50px auto; ">
            If you want to edit <b>Name</b>, <b>Company</b> or <b>Job Title</b>, please
            email to <a href="mailto:kolokhin@devgamm.com">kolokhin@devgamm.com</a>.<br>
            We will change it ont he website and at your badge.
        </div>
    </div>
    <div id="bp-appointment" class="bp-appointment-form-wrapper modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Make an appointment with {{formAppointment.member.name}}</h4>
                </div>
                <form class="form-horizontal" name="formBPApp" ng-submit="formAppointment.submit()">
                    <div class="modal-body">
                        <div class="form-group required">
                            <label class="col-sm-4 control-label">Your message</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                            <textarea name="body" class="form-control" required="1" rows="4"
                                                      maxlength="140"
                                                      ng-model="formAppointment.body"
                                                      ng-pattern="/^[^А-Яа-яіє]*$/"></textarea>
                                    </div>
                                </div>
                                <div ng-show="formBPApp.body.$touched && formAppointment.body.$invalid"
                                     class="error">
                                    Please type your text here
                                </div>
                                <div ng-show="formBPApp.body.$error.pattern" class="error">Only latin symbols and digits please
                                </div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-4 control-label">Your name</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="text" name="name" class="form-control"
                                               ng-required="true" ng-model="formAppointment.name"
                                               ng-pattern="/^[^А-Яа-яіє]*$/">
                                    </div>
                                </div>
                                <div ng-show="formBPApp.name.$touched && formBPApp.name.$invalid"
                                     class="error">Please specify your name
                                </div>
                                <div ng-show="formBPApp.name.$error.pattern" class="error">Only latin symbols and digits please</div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-4 control-label">Your email</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="email" name="email" class="form-control"
                                               ng-required="true" ng-model="formAppointment.email"
                                               ng-pattern="/^[^А-Яа-яіє]*$/">
                                    </div>
                                </div>
                                <div ng-show="formBPApp.email.$touched && formBPApp.email.$invalid"
                                     class="error">Please specify your email
                                </div>
                                <div ng-show="formBPApp.email.$error.pattern" class="error">Only latin symbols and digits please
                                </div>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-4 control-label">{{formAppointment.n1}} + {{formAppointment.n2}}</label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="number" name="captcha" class="form-control"
                                               ng-required="true" ng-model="formAppointment.answer"
                                               required="1"                                                check-sum
                                               check-sum
                                               n1="{{formAppointment.n1}}"
                                               n2="{{formAppointment.n2}}"
                                            >
                                    </div>
                                </div>
                                <div ng-show="formBPApp.captcha.$touched && formBPApp.captcha.$invalid"
                                     class="error"> Please type correct answer
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <div class="error" ng-show="formBPApp.showErrorMessage">
                                    {{formAppointment.message}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button ng-disabled="formBPApp.$invalid" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="notify" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Info</h4>
                </div>
                <div class="modal-body">
                    <div ng-bind-html="modal_message"></div>
                </div>
                <div class="modal-footer">
                    <div class="">
                        <button id="notifyOk" type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>