<?php Yii::app()->clientScript->registerCssFile('discussion-panel.css'); ?>
<?php $len = count($data); $i = 0;
if (!empty($data) && is_array($data)) foreach ($data as $catId => $category) { $i++; ?>
    <div>
        <h3 class="category-element title"> <?=Yii::t('app', 'Discussion panel');?>: &#171;<?= $category['category']['title'];?>&#187;</h3>
        <div class="description" ><?= isset($category['category']['description']) ? $category['category']['description'] : ''; ?></div>
        <?php foreach ($category['data'] as $id => $row) { ?>
            <table>
                <tr>
                    <td class="photo">
                        <img src="<?= Yii::app()->Core->imgHost . $row['photo']; ?>"  border="0"/>
                        <?php if ($row['receive_email']) { ?>
                            <a class="send-notification" title="<?= $row['name']; ?>" data-type="speaker" data-id="<?= $row['id']; ?>" href="" onclick="return false;"><img src="<?= Yii::app()->Core->imgHost; ?>mail_icon.png" class="breadcrumbs-icon" border="0"/><?= Yii::t('app', 'Email'); ?></a>
                        <?php } ?>
                        <?php if (!empty($row['video'])) { ?>
                            <a href="<?= $row['video']; ?>" target="_blank"><img src="<?= Yii::app()->Core->imgHost; ?>video_icon.png" class="breadcrumbs-icon" border="0"/><?= Yii::t('app', 'Video'); ?></a>
                        <?php } ?>
                        <?php if (!empty($row['presentation'])) { ?>
                            <a href="<?= $row['presentation']; ?>" target="_blank"><img src="<?= Yii::app()->Core->imgHost; ?>slides_icon.png" class="breadcrumbs-icon" border="0"/><?= Yii::t('app', 'Slides'); ?></a>
                        <?php } ?>
                    </td>
                    <td class="company-logo"><img src="<?= Yii::app()->Core->imgHost . $row['company_logo']; ?>" border="0"/></td>
                    <?php if (empty($row['description']) || 3 == strlen($row['description'])) { ?>
                        <td class="description"><?= $row['bio']; ?></td>
                    <?php } else { ?>
                        <td class="description-half"><?= $row['description']; ?></td>
                        <td class="description-half"><?= $row['bio']; ?></td>
                    <?php } ?>
                </tr>
            </table>
        <?php } ?>
    </div>
<?php
    if ($len != $i) { ?>
        <div class="speaker-row"> </div>
<?php }}?>