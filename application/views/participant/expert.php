<?php Yii::app()->clientScript->registerCssFile('experts.css'); ?>
<?php $this->renderPartial('_dgbp_start', array('participant'=>'expert')); ?>

<table>
    <tbody>
        <?php if (!empty($data) && is_array($data)) foreach ($data as $expertInfo) {
                if (empty($expertInfo['data']) || !is_array($expertInfo['data'])) {
                    break;
                } else {
                    foreach ($expertInfo['data'] as $row) { ?>
                        <tr>
                            <td width="180">
                                <br/>
                                <img src="//devgamm.com/i/<?= $row['photo'];?>" alt="<?= $row['name']; ?>">
                                <img class="company-logo-img" src="//devgamm.com/i/<?= $row['company_logo'];?>" alt="<?= $row['name']; ?>">
                            </td>
                            <td class="bio-text">
                                <br/>
                                <?= $row['bio'];?>
                                <br/>
                            </td>
                        </tr>
             <?php }
                }
         } ?>
    </tbody>
</table>

<?php $this->renderPartial('_dgbp_end'); ?>