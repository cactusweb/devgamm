<?php $this->renderPartial('_dgbp_start', array('participant'=>'partner')); ?>

<table>
    <tbody>
    <?php if (!empty($data) && is_array($data)) foreach ($data as $row) { ?>
        <tr>
            <td width="100" valign="top" height="100">
                <img src="//devgamm.com/<?= Yii::app()->Core->confDir; ?>/images/<?= ($row['photo'])? $row['photo'] : 'user_avatar.gif'; ?>"
                     alt="<?= $row['name']; ?>">
            </td>
            <td width="300" valign="top" height="100">
                <strong><?= $row['name']; ?></strong>

                <p><?= $row['position']; ?></p>

                <p><?= Yii::t('app', 'Company'); ?>:
                    <a target="_blank" href="//<?= $row['company_url']; ?>"><?= $row['company_title']; ?></a>
                </p>

                <p style="margin-top:4%;">
                    <img align="left" style="padding-right:5px;" alt="" src="//console.devgamm.com/i/mail_icon.png"/>
                    <a title="<?= $row['name']; ?>" ng-click="makeAppointment('<?= $row['id']; ?>', 'partner')"><?= Yii::t('app', 'Make an appointment'); ?></a>
                </p>
            </td>
            <td width="400" valign="top" height="100">
                <p>
                    <img align="left" alt="" style="padding-right:5px;" src="//console.devgamm.com/i/lookfor.png"/><strong><?= Yii::t('app', "I'm looking for"); ?>:&nbsp;</strong><?= $row['lookingfor']; ?>
                </p>

                <p>
                    <img align="left" alt="" style="padding-right:5px;" src="//console.devgamm.com/i/offer_icon.png"/><strong><?= Yii::t('app', "I'm offering"); ?>:&nbsp;</strong><?= $row['propose']; ?>
                </p>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php $this->renderPartial('_dgbp_end'); ?>
