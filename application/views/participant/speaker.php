<?php Yii::app()->clientScript->registerCssFile('speaker.list.css'); $cssText = ''; ?>
<?php $this->renderPartial('_dgbp_start', array('participant'=>'speaker')); ?>

<?php if (!empty($data) && is_array($data)) foreach ($data as $catId => $category) {
    $cssText .= ".cat-$catId{border-color:{$category['category_color']};} .cat-$catId h3, .cat-$catId h4{color: {$category['category_color']};}"; ?>
    <div class="groupBlock cat-<?= $catId; ?>">
        <h3><?= $category['category']; ?></h3>
        <?php foreach ($category['data'] as $id => $row) { ?>
            <table class="speaker-row">
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2" class="topic cat-<?= $catId; ?>"><h4>&#171;<?= $row[0]['topic']; ?>&#187;</h4></td>
                </tr>
                <?php if (1 < count($row)) { ?>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2" class="description"><?= $row[0]['description']; ?></td>
                    </tr>
                    <?php foreach ($row as $speaker) { ?>
                        <tr>
                            <td class="photo">
                                <img src="<?= Yii::app()->Core->imgHost . $speaker['photo']; ?>" border="0"/>
                                <?php if ($speaker['receive_email']) { ?>
                                    <a class="send-notification" title="<?= $speaker['name']; ?>" data-foreign="<?=$speaker['foreign_speaker'];?>"
                                       ng-click="makeAppointment('<?= $speaker['id']; ?>', 'speaker')">
                                        <img src="<?= Yii::app()->Core->imgHost; ?>mail_icon.png" class="breadcrumbs-icon" border="0"/><?= Yii::t('app', 'Email'); ?>
                                    </a>
                                <?php } ?>
                                <?php if (!empty($speaker['video'])) { ?>
                                    <a href="<?= $speaker['video']; ?>" target="_blank"><?= Yii::t('app', 'Video'); ?></a>
                                <?php } ?>
                                <?php if (!empty($speaker['presentation'])) { ?>
                                    <a href="<?= $speaker['presentation']; ?>" target="_blank"><?= Yii::t('app', 'Slides'); ?></a>
                                <?php } ?>
                            </td>
                            <td class="company-logo"><img src="<?= Yii::app()->Core->imgHost . $row[0]['company_logo']; ?>" border="0"/></td>
                            <td colspan="2" class="description"><?= $speaker['bio']; ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td class="photo">
                            <img src="<?= Yii::app()->Core->imgHost . $row['0']['photo']; ?>"  border="0"/>
                            <?php if ($row[0]['receive_email']) { ?>
                                <a class="send-notification" title="<?= $row[0]['name']; ?>" data-foreign="<?=$row[0]['foreign_speaker'];?>"
                                   ng-click="makeAppointment('<?= $row[0]['id']; ?>', 'speaker')">
                                    <img src="<?= Yii::app()->Core->imgHost; ?>mail_icon.png" class="breadcrumbs-icon" border="0"/><?= Yii::t('app', 'Email'); ?>
                                </a>
                            <?php } ?>
                            <?php if (!empty($row[0]['video'])) { ?>
                                <a href="<?= $row[0]['video']; ?>" target="_blank"><img src="<?= Yii::app()->Core->imgHost; ?>video_icon.png" class="breadcrumbs-icon" border="0"/><?= Yii::t('app', 'Video'); ?></a>
                            <?php } ?>
                            <?php if (!empty($row[0]['presentation'])) { ?>
                                <a href="<?= $row[0]['presentation']; ?>" target="_blank"><img src="<?= Yii::app()->Core->imgHost; ?>slides_icon.png" class="breadcrumbs-icon" border="0"/><?= Yii::t('app', 'Slides'); ?></a>
                            <?php } ?>
                        </td>
                        <td class="company-logo"><img src="<?= Yii::app()->Core->imgHost . $row[0]['company_logo']; ?>" border="0"/></td>
                        <?php if (empty($row[0]['description']) || 3 == strlen($row['0']['description'])) { ?>
                            <td class="description"><?= $row['0']['bio']; ?></td>
                        <?php } else { ?>
                            <td class="description-half"><?= $row['0']['description']; ?></td>
                            <td class="description-half"><?= $row['0']['bio']; ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </table>
        <?php } ?>
    </div>
<?php } ?>
<?php $this->renderPartial('_dgbp_end'); ?>
<?php Yii::app()->clientScript->registerCss('speaker-color', $cssText);?>
