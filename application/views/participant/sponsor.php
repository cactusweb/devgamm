<?php Yii::app()->clientScript->registerCssFile('sponsor.css'); ?>
<?php $this->renderPartial('_dgbp_start', array('participant'=>'sponsor')); ?>

<?php if (!empty($data) && is_array($data)) foreach ($data as $row) { ?>
    <div class="groupBlocks cat-<?= strtolower($row['category']); ?>">
        <h3><?= Yii::t('app', $row['category']); ?></h3>
        <?php foreach ($row['data'] as $s) { ?>
            <div class="sponsor-full-row">
                <div class="left-side">
                    <a href="<?= $s['url']; ?>" target="_blank" title="<?= $s['title']; ?>"><img src="<?= Yii::app()->Core->imgHost . $s['logo']; ?>" class="sponsor"></a>
                </div>
                <div class="right-side">
                    <?= $s['description']; ?>
                    <p class="communication">
                        <img align="left" style="padding-right:5px;" alt="" src="<?= Yii::app()->Core->imgHost; ?>mail_icon.png"/>
                        <a title="<?= $s['title']; ?>" ng-click="makeAppointment('<?= $s['id']; ?>', 'sponsor')"><?= Yii::t('app', 'Make an appointment'); ?></a>
                    </p>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<?php $this->renderPartial('_dgbp_end'); ?>
