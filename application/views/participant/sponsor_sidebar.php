<?php Yii::app()->clientScript->registerCssFile('sponsor.css'); ?>
<div class="sponsors-container">
<?php if (!empty($data) && is_array($data)) foreach ($data as $row) { ?>
    <h3><?= Yii::t('app', $row['category']); ?></h3>
    <?php foreach ($row['data'] as $s) { ?>
        <div class="sponsor-row"><a href="<?= $s['url']; ?>" target="_blank" title="<?= $s['title']; ?>"><img src="<?= Yii::app()->Core->imgHost . $s['logo']; ?>" class="sponsor sponsor-<?= strtolower($row['category']); ?>"></a></div>
    <?php } ?>
<?php } ?>
</div>
