<div class="container-fluid">
    <div class="container">
        <h1>Manage Settings</h1>

        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'settings-grid',
            'dataProvider'=>$model->search(),
            'columns'=>array(
                'label',
                'value',
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}'
                ),
            ),
        )); ?>

    </div>
</div>