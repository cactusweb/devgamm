<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'settings-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="">
		<?php echo $form->textField($model,'value',array('style'=>'width: 100%;')); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>
    <br/>
	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    CKEDITOR.replace( 'wysiwyg', {height: '500px'} );
</script>