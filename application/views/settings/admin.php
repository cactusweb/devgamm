<div class="container-fluid">
    <div class="container">
        <h2>Awards</h2>
        <?php
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria'=>array(
                'condition'=>'id IN (5,6,11,14)',
            ),
        ));
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'settings-grid',
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                'label',
                array(
                    'name'=>'value',
                    'type'=>'raw',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}'
                ),
            ),
            'hideHeader'=>true,
            'summaryText'=>'',
            'htmlOptions'=>array('style'=>'padding-top: 0!important;')
        ));
        ?>

        <h2>Game Lynch</h2>
        <?php
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria'=>array(
                'condition'=>'id IN (7,8,12,15)',
            ),
        ));
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'settings-grid',
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                'label',
                array(
                    'name'=>'value',
                    'type'=>'raw',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}'
                ),
            ),
            'hideHeader'=>true,
            'summaryText'=>'',
            'htmlOptions'=>array('style'=>'padding-top: 0!important;')
        ));
        ?>

        <h2>GAMM:Play</h2>
        <?php
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria'=>array(
                'condition'=>'id IN (9,10,13,16)',
            ),
        ));
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'settings-grid',
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                'label',
                array(
                    'name'=>'value',
                    'type'=>'raw',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}'
                ),
            ),
            'hideHeader'=>true,
            'summaryText'=>'',
            'htmlOptions'=>array('style'=>'padding-top: 0!important;')
        ));
        ?>

        <h2>Email: Game Submitted</h2>
        <?php
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria'=>array(
                'condition'=>'id IN (2, 3)',
            ),
        ));
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'settings-grid',
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                'label',
                array(
                    'name'=>'value',
                    'type'=>'raw',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}'
                ),
            ),
            'hideHeader'=>true,
            'summaryText'=>'',
            'htmlOptions'=>array('style'=>'padding-top: 0!important;')
        ));
        ?>

        <h2>Edit Game: Deadline</h2>
        <?php
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria'=>array(
                'condition'=>'id IN (4)',
            ),
        ));
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'settings-grid',
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                'label',
                array(
                    'name'=>'value',
                    'type'=>'raw',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}'
                ),
            ),
            'hideHeader'=>true,
            'summaryText'=>'',
            'htmlOptions'=>array('style'=>'padding-top: 0!important;')
        ));
        ?>


        <h2>Meeting system:</h2>
        <?php
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria'=>array(
                'condition'=>'id IN (17, 18, 19)',
            ),
        ));
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'settings-grid',
            'dataProvider'=>$dataProvider,
            'columns'=>array(
                'label',
                array(
                    'name'=>'value',
                    'type'=>'raw',
                ),
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{update}'
                ),
            ),
            'hideHeader'=>true,
            'summaryText'=>'',
            'htmlOptions'=>array('style'=>'padding-top: 0!important;')
        ));
        ?>
    </div>
</div>