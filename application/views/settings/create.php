<div class="container-fluid">
    <div class="container">
        <h1>Create Settings</h1>

        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>