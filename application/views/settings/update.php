<div class="container-fluid">
    <div class="container">
        <h1>Update Settings: "<?php echo $model->label; ?>"</h1>

        <?php
        $settingTextId = array(14,15,16,17,19);
        $settingDatesId = array(4,11,12,13);
        if(in_array($model->id, $settingDatesId)){
            $partial = '_form_date';
        }elseif(in_array($model->id, $settingTextId)){
            $partial = '_form_text';
        }else{
            $partial = '_form';
        }
        echo $this->renderPartial($partial, array('model'=>$model));
        ?>

    </div>
</div>