<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'showcase-form',
        'enableAjaxValidation'=>false,
    )); ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="span3">
            <?php echo $form->labelEx($model,'tables'); ?>
            <?php $tables = range(0,50); unset($tables[0]); echo $form->dropDownList($model,'tables', $tables); ?>
            <?php echo $form->error($model,'tables'); ?>
        </div>

        <div class="span3">
            <?php echo $form->labelEx($model,'days'); ?>
            <?php $days = range(0,3); unset($days[0]); echo $form->dropDownList($model,'days', $days); ?>
            <?php echo $form->error($model,'days'); ?>
        </div>

        <div class="span3">
            <?php echo $form->labelEx($model,'timeslots'); ?>
            <?php $timeslots = range(0,2); unset($timeslots[0]); echo $form->dropDownList($model,'timeslots', $timeslots); ?>
            <?php echo $form->error($model,'timeslots'); ?>
        </div>
    </div>

    <div class="row buttons">
        <div class="span12">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>
<br/>
<br/>

<div class="form">
    <?php

    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'showcase-form',
        'enableAjaxValidation'=>false,
    )); ?>
    <?php echo $form->errorSummary($model); ?>

    <h1>Schedule:</h1>

    <table>
        <tr>
            <?php
            $models = Game::model()->findAll(array('condition'=>'ticket_id IS NOT NULL', 'order'=>'title ASC'));
            $list = array();
            $list['free'] = 'Free slot';
            $list['empty'] = 'Empty slot';
            $list['reserved'] = 'Reserved slot';
            $list += CHtml::listData($models, 'id', 'title');
            $timeslotsCounter = 1;

            foreach($model->schedule as $day_k => $day):
                ?>
                <td>
                    <table>
                        <tr>
                            <td class="span2 col-table" style="min-width: 60px;">&nbsp;</td>
                            <td class="span5  col-day">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <input type="text" name="Schedule[<?=$day_k;?>][date]" value="<?=$day["date"];?>" placeholder="Введите дату" style="width: 274px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <?php foreach($day["timeslot"] as $timeslot_k => $timeslot):  ?>
                                            <td class="span2  col-timeslot">
                                                <input type="text" name="Schedule[<?=$day_k;?>][timeslot][<?=$timeslot_k?>]" value="<?=$timeslot;?>" placeholder="Ввдеите время" style="width: 130px;" />
                                            </td>
                                        <?php endforeach ?>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php foreach($day["table"] as $table_k => $table):  ?>
                            <tr>
                                <td class="span2 col-table">
                                    <?= '<input type="text" value="'.$table['title'].'" name="Schedule['.$day_k.'][table]['.$table_k.'][title]" style="width: 40px;" placeholder="A*" />'; ?>
                                </td>
                                <td class="span5  col-day">
                                    <table>
                                        <tr>
                                            <?php foreach($table['timeslot'] as $timeslot_k => $timeslot):  ?>
                                                <td class="span2  col-timeslot">
                                                    <?= CHtml::dropDownList('Schedule['.$day_k.'][table]['.$table_k.'][timeslot]['.$timeslot_k.']', $timeslot, $list, array('style'=>'width: 144px;')); ?>
                                                </td>
                                            <?php endforeach ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                </td>
            <?php endforeach ?>
        </tr>
    </table>

    <?php echo $form->error($model,'schedule'); ?>

    <div class="row buttons">
        <div class="span12">
            <?php echo CHtml::submitButton('Save'); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->