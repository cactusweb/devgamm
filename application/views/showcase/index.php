<div class="dg-content-wrapper"  style=" padding: 0px; width: 780px; " >
    <?php foreach($model->schedule as $day_k => $day):  ?>
        <div class="dg-gammplay-day-title"><?= @$day['date']; ?></div>
        <table class="table table-bordered gamm-play-showcase">
            <tr>
                <th>Table #</th>
                <?php foreach($day["timeslot"] as $timeslot_k => $timeslot):  ?>
                    <th><?= ($timeslot_k == 1)? 'First stream:' : 'Second stream:' ?> <span><?= $timeslot; ?></span></th>
                <?php endforeach ?>
            </tr>
            <?php foreach($day["table"] as $table_k => $table):  ?>
            <tr>
                <td>
                    <?= $table['title']; ?>
                </td>
                <?php foreach($table['timeslot'] as $timeslot_k => $timeslot):  ?>
                    <?php if($timeslot == 'free'): ?>
                        <td class="dg-free-spot">
                            Free spot
                        </td>
                    <?php elseif($timeslot == 'reserved'): ?>
                        <td class="dg-reserved">
                            Reserved
                        </td>
                    <?php elseif($timeslot == 'empty'): ?>
                        <td class="dg-empty"></td>
                    <?php
                    else:
                        $game = Game::model()->findByPk($timeslot);
                        if(!$game){
                            echo '<td>No game with ID #'.$timeslot.'</td>';
                        }else{
                          ?>
                            <td>
                                <div class="dg-game">
                                    <div class="dg-game-ico">
                                        <img src="<?=$game->icon;?>" />
                                    </div>
                                    <div class="dg-game-title"><?=$game->title;?></div>
                                    <div class="dg-game-description"><?=$game->author;?></div>
                                </div>
                            </td>
                          <?php
                        }
                        ?>
                    <?php endif; ?>
                <?php endforeach ?>
            </tr>
            <?php endforeach ?>
        </table>
    <?php endforeach ?>
</div>