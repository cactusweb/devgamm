<div class="container-fluid">
    <div class="container">
        <h1>Update Showcase</h1>
        <br/>
        <?= (Yii::app()->user->hasFlash('msg'))? '<p><b style="color: darkred;">'.Yii::app()->user->getFlash('msg').'.</b></p><br/>' : ''; ?>
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>
