<?php
Yii::app()->clientScript->registerCoreScript('jquery.ui')
          ->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css')
          ->registerCssFile('search.widget.css')
          ->registerScriptFile('activity.js')
          ->registerScriptFile('search.widget.js'); ?>
<div class="container-fluid" id="search-form-widget">
    <div class="hero-unit">
        <h3><?= $h3; ?></h3>
        <?= !empty($h4) ? '<p>' . $h4 . '</p>' : '' ?>
    </div>
    <div class="row-fluid">
        <ul id="search-tabs" class="nav nav-tabs" data="<?= (int)$autoRun; ?>" data-callback="<?= $callback; ?>">
            <li<?= !$isShowAnswer || empty($data) ? ' class="active"' : ''; ?>><a href="#form" data-toggle="tab">Form</a></li>
            <li<?= $isShowAnswer && !empty($data) ? ' class="active"' : ''; ?>><a href="#answer" data-toggle="tab">Data</a></li>
        </ul>
        <div id="search-tabsContent" class="tab-content">
            <div class="tab-pane<?= !$isShowAnswer || empty($data) ? ' active' : ''; ?>" id="form">
                <form name="<?= isset($form['name']) ? $form['name'] : ''; ?>"
                    action="<?= isset($form['action']) ? $form['action'] : ''; ?>"
                    <?= isset($form['id']) ? ' id="' . $form['id'] . '"' : ''; ?>
                    class="form form-horizontal<?= isset($form['class']) ? ' ' . $form['class'] : ''; ?>">
                    <?php foreach($fields as $input) echo $input . '&nbsp;';  ?>
                    <div class="button">
                        <input id="get-data" class="btn btn-middle btn-primary" type="submit" value="Submit&nbsp;&#8658;"/>
                        <?php if ($isCSVAvailable) { ?>
                            <input id="get-csv-data" class="btn btn-primary" type="submit" name="csv" value="export as CSV&nbsp;&#8659;"/>
                        <?php } ?>
                    </div>
                </form>
            </div>
            <div class="tab-pane<?= $isShowAnswer && !empty($data) ? ' active' : ''; ?>" id="answer">
                    <?= !empty($data) ? $data : ''; ?>
            </div>
        </div>
    </div>
</div>