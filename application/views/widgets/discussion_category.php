<form>
    <div class="row-fluid<?= !empty($data) ? ' participant' : ''; ?>">
        <div class="span12">
            <div class="row-fluid">
                <div class="span3">
                    <div class="row-fluid">
                        <label>Title (en)</label>
                        <div class="span9"><input class="input" type="text" name="title_en" value="<?= isset($data['title_en']) ? $data['title_en'] : ''; ?>"></div>

                        <label>Description (en)</label>
                        <div class="span9"><textarea name="description_en" rows="5"><?= isset($data['description_en']) ? $data['description_en'] : ''; ?></textarea></div>
                    </div>
                </div>
                <div class="span3">
                    <div class="row-fluid">
                        <label>Title (ru)</label>
                        <div class="span9"><input class="input" type="text" name="title_ru" value="<?= isset($data['title_ru']) ? $data['title_ru'] : ''; ?>"></div>

                        <label>Description (ru)</label>
                        <div class="span9"><textarea name="description_ru" rows="5"><?= isset($data['description_ru']) ? $data['description_ru'] : ''; ?></textarea></div>
                    </div>
                </div>
                <div class="span1">
                    <input type='hidden' name="color" class="color-value" id="color-<?= isset($data['id']) ? $data['id'] : 'new'; ?>" value="<?= isset($data['color']) ? $data['color'] : ''; ?>"/>
                </div>
                <div class="span1">
                    <a href="" onclick="return false;" style="display: none;" id="sc<?= isset($data['id']) ? $data['id'] : ''; ?>" class="save-row"><span class="icon icon-ok"></span></a>
                </div>
            </div>
        </div>
    </div>
</form>