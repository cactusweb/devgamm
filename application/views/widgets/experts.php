<form>
    <div class="row-fluid<?= !empty($data) ? ' participant' : ''; ?>">
        <div class="span12">
            <div class="row-fluid">
                <div class="span1" data-rating="<?= isset($data['rating']) ? $data['rating'] : ''; ?>" data-id="<?= isset($data['id']) ? $data['id'] : ''; ?>">
                    <?php if ($data) { ?>
                        <a href="" onclick="return false;" class="up-rating"><span class="icon icon-arrow-up"></span></a><a href="" onclick="return false;" class="down-rating"><span class="icon icon-arrow-down"></span></a><br/>
                        <a href="" onclick="return false;" class="remove"><span class="icon icon-trash"></span></a>
                    <?php } ?>
                </div>
                <div class="span1"><img class="img-polaroid photo-preview" border="0" height="200px" src="<?= isset($data['photo']) ? Yii::app()->Core->imgHost . $data['photo'] : ''; ?>"/></div>
                <div class="span1"><img class="img-polaroid photo-preview" border="0" height="200px" src="<?= isset($data['company_logo']) ? Yii::app()->Core->imgHost . $data['company_logo'] : ''; ?>"/></div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Email</div>
                        <div class="span9"><input class="input" type="text" name="email" value="<?= isset($data['email']) ? $data['email'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Photo</div>
                        <div class="span9"><input type="text" name="photo" class="photo" value="<?= isset($data['photo']) ? $data['photo'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Company logo</div>
                        <div class="span9"><input class="input" type="text" name="company_logo" value="<?= isset($data['company_logo']) ? $data['company_logo'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Category</div>
                        <div class="span9">
                            <select name="category_id">
                                <?php foreach (Yii::app()->Core->expertCategory as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['category_id']) && $k == $data['category_id'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span5">
                            <label class="checkbox">
                                <input class="checkbox" name="foreign_speaker" value="1" type="checkbox" <?=!empty($data['foreign_speaker']) ? 'checked' : ''?>> Foreign speaker?
                             </label>
                         </div>
                        <div class="span9"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Name (en)</div>
                        <div class="span9"><input type="text" name="name_en" value="<?= isset($data['name_en']) ? $data['name_en'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Name (ru)</div>
                        <div class="span9"><input type="text" name="name_ru" value="<?= isset($data['name_ru']) ? $data['name_ru'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Bio (ru)</div>
                        <div class="span9"><textarea name="bio_ru" rows="5"><?= isset($data['bio_ru']) ? $data['bio_ru'] : ''; ?></textarea></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Bio (en)</div>
                        <div class="span9"><textarea name="bio_en" rows="5"><?= isset($data['bio_en']) ? $data['bio_en'] : ''; ?></textarea></div>
                    </div>
                </div>
                <div class="span1">
                    <a href="" onclick="return false;" style="display: none;" id="<?= isset($data['id']) ? $data['id'] : ''; ?>" class="save-row"><span class="icon icon-ok"></span></a>
                </div>
            </div>
        </div>
    </div>
</form>

