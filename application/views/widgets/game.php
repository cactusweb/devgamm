<form>
    <div class="game row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span1" data-rating="<?= isset($data['rating']) ? $data['rating'] : ''; ?>" data-id="<?= isset($data['id']) ? $data['id'] : ''; ?>">
                    <input type="hidden" name="id" value="<?= isset($data['id']) ? $data['id'] : ''; ?>">
                    <?php if ($data) { ?>
                        <a href="" onclick="return false;" class="up-rating"><span class="icon icon-arrow-up"></span></a><a href="" onclick="return false;" class="down-rating"><span class="icon icon-arrow-down"></span></a><br/>
                        <a href="" onclick="return false;" class="remove"><span class="icon icon-trash"></span></a>
                    <?php } ?>
                </div>

                <div class="span1">
                    <img class="img-polaroid photo-preview" border="0"
                         src="<?php
                            echo ($data['icon_ext']) ? '//devgamm.com/' . Yii::app()->Core->confDir . '/contest/contestGameIcon_' . $data['id'] . '.' . $data['icon_ext'] : '';
                            echo ($data['icon']) ? $data['icon'] : '';
                         ?>"
                        />
                </div>

                <div class="span5">
                    <div class="row-fluid">
                    <div class="span4"><?= isset($data['id']) ? 'ID: <strong>' . $data['id'] . '</strong>' : ''; ?> Title</div>
                        <div class="span8"><input class="input" type="text" name="title" value="<?= isset($data['title']) ? $data['title'] : ''; ?>"></div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">Type</div>
                        <div class="span8">
                            <select name="category_id">
                                <?php foreach (Yii::app()->Core->getGamesCategories() as $categoryId => $categoryInfo) { ?>
                                        <option value="<?=$categoryInfo['id'];?>"<?= isset($data['category_id']) && $categoryInfo['id'] == $data['category_id'] ? ' selected="selected"' : ''; ?>><?=$categoryInfo['title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">Ticket</div>
                        <div class="span8"><input class="input" type="text" name="ticket" value="<?= isset($data['ticket']) ? $data['ticket'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Email</div>
                        <div class="span8"><input class="input" type="text" name="email" value="<?= isset($data['email']) ? $data['email'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Phone</div>
                        <div class="span8"><input type="text" name="phone" value="<?= isset($data['phone']) ? $data['phone'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Location</div>
                        <div class="span8"><input type="text" name="location" class="phone" value="<?= isset($data['location']) ? $data['location'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Author</div>
                        <div class="span8"><input type="text" name="author" value="<?= isset($data['author']) ? $data['author'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Author type</div>
                        <div class="span8">
                            <select name="author_type">
                                <?php foreach (Yii::app()->Core->getGameAuthorTypes() as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['author_type']) && $k == $data['author_type'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Stage of development</div>
                        <div class="span8">
                            <select name="stage">
                                <?php foreach (Yii::app()->Core->getGameDevelopmentStages() as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['stage']) && $k == $data['stage'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Description</div>
                        <div class="span8"><textarea name="description" rows="5"><?= isset($data['description']) ? $data['description'] : ''; ?></textarea></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Publish date</div>
                        <div class="span8"><?= isset($data['date']) ? substr($data['date'], 0, 10) : ''; ?></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span4">Gameplay video</div>
                        <div class="span8"><input type="text" name="video" value="<?= isset($data['video']) ? $data['video'] : ''; ?>"></div>
                    </div>
                    <?php if (1 == $data['category_id']) { ?>
                        <div class="row-fluid">
                            <div class="span4">Game size</div>
                            <div class="span8"><input type="text" name="dimension" value="<?= isset($data['dimension']) ? $data['dimension'] : ''; ?>"></div>
                        </div>
                    <?php } ?>
                    <div class="row-fluid" style="white-space: nowrap;">
                        <div class="span4">To Lynch</div>
                        <div class="span8">
                            <select name="lynch">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['lynch']) && $k == $data['lynch'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                            <?php if($data['lynch']) echo ($data['lynch_drink'])? ' - drink' : ' - not drink' ?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">To Gamm:Play</div>
                        <div class="span8">
                            <select name="gammplay">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['gammplay']) && $k == $data['gammplay'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Icon</div>
                        <div class="span8">
                            <?= ($data['icon_ext'])? Yii::app()->Core->confDir . '/contest/contestGameIcon_' . $data['id'] . '.' . $data['icon_ext'] : ''; ?>
                            <?= ($data['icon'])? Yii::app()->Core->confDir . '/contest/contestGameIcon_' . $data['id'] . '.' . $data['icon_ext'] : ''; ?>
                            <input class="input" type="text" name="icon"
                              value="<?= ($data['icon_ext'])? Yii::app()->Core->confDir . '/contest/contestGameIcon_' . $data['id'] . '.' . $data['icon_ext'] : ''; ?>">
                            <input class="input" type="text" name="icon"
                               value="<?= ($data['icon'])? Yii::app()->Core->confDir . '/contest/contestGameIcon_' . $data['id'] . '.' . $data['icon_ext'] : ''; ?>"
                        </div>
                    </div>

                    <?php if (1 == $data['category_id']) { ?>
                        <div class="row-fluid">
                            <div class="span4">File</div>
                            <div class="span8"><?= isset($data['id']) ? Yii::app()->Core->confDir . '/contest/contestGame_' . $data['id'] . '.' . $data['file_ext'] : ''; ?></div>
                        </div>
                    <?php } else { ?>
                        <div class="row-fluid">
                            <div class="span4">Game links</div>
                            <div class="span8">
                                <?php if (in_array($data['category_id'], array(1, 2, 3))) { ?>
                                    <input type="text" name="link" value="<?= isset($data['link']) ? $data['link'] : ''; ?>">
                                <?php } elseif (!empty($data['link'])) { $links = unserialize($data['link']);
                                    switch ($data['category_id'])
                                    {
                                        case 4:
                                        ?>
                                            <input type="text" name="link[appstore]" value="<?= isset($links['appstore']) ? $links['appstore'] : ''; ?>"> - appstore<br/>
                                            <input type="text" name="link[gplay]" value="<?= isset($links['gplay']) ? $links['gplay'] : ''; ?>"> - google play<br/>
                                            <input type="text" name="link[web]" value="<?= isset($links['web']) ? $links['web'] : ''; ?>"> - web<br/>
                                            <input type="text" name="link[other]" value="<?= isset($links['other']) ? $links['other'] : ''; ?>"> - other
                                        <?php
                                            break;
                                        case 5:
                                        ?>
                                            <input type="text" name="link[win]" value="<?= isset($links['win']) ? $links['win'] : ''; ?>"> - Win<br/>
                                            <input type="text" name="link[mac]" value="<?= isset($links['mac']) ? $links['mac'] : ''; ?>"> - Mac<br/>
                                            <input type="text" name="link[steam]" value="<?= isset($links['steam']) ? $links['steam'] : ''; ?>"> - Steam<br/>
                                        <?php
                                            break;
                                    }
                                 } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (5 == $data['category_id']) { ?>
                        <div class="row-fluid">
                            <div class="span4">Steam code</div>
                            <div class="span8"><input type="text" name="steam_code" value="<?= isset($data['steam_code']) ? $data['steam_code'] : ''; ?>"></div>
                        </div>
                    <?php } ?>
                    <?php if (in_array($data['category_id'], array(2, 4))) { ?>
                        <div class="row-fluid">
                            <div class="span4">Platform</div>
                            <div class="span8"><input type="text" name="platform" value="<?= isset($data['platform']) ? $data['platform'] : ''; ?>"></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">Is Free-to-Play</div>
                            <div class="span8">
                                <select name="f2p">
                                    <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                        <option value="<?= $k; ?>"<?= isset($data['f2p']) && $k == $data['f2p'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row-fluid">
                        <div class="span4">More info</div>
                        <div class="span8">
                            <?= CHtml::link('View game', array('awards/gameview', 'id'=>$data['id']), array('class'=>'btn btn-success', 'target'=>'_blank')); ?>
                        </div>
                    </div>
                    <br/>
                    <div class="row-fluid">
                        <div class="span4">Is approved?</div>
                        <div class="span8">
                            <a href="" onclick="return false;" data-id="<?= $data['id']; ?>" data-type="<?= $data['approved']; ?>" class="approve btn<?= $data['approved'] ? ' btn-success' : '' ?>"><?= $data['approved'] ? '' : 'NOT '; ?>Approved</a>
                        </div>
                    </div>
                </div>
                <div class="span1">
                    <a href="" onclick="return false;" style="display: none;" id="<?= isset($data['id']) ? $data['id'] : ''; ?>" class="save-row"><span class="icon icon-ok"></span></a>
                </div>
            </div>
        </div>
    </div>
    <hr>
</form>

