<form>
    <div class="game row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <div class="span1" data-rating="<?= isset($data['rating']) ? $data['rating'] : ''; ?>" data-id="<?= isset($data['id']) ? $data['id'] : ''; ?>">
                    <input type="hidden" name="id" value="<?= isset($data['id']) ? $data['id'] : ''; ?>">
                    <?php if ($data) { ?>
                        <a href="" onclick="return false;" class="up-rating"><span class="icon icon-arrow-up"></span></a><a href="" onclick="return false;" class="down-rating"><span class="icon icon-arrow-down"></span></a><br/>
                        <a href="" onclick="return false;" class="remove"><span class="icon icon-trash"></span></a>
                    <?php } ?>
                </div>

                <div class="span1">
                    <img class="img-polaroid photo-preview" border="0"
                         src="<?php
                            echo ($data['icon']) ? $data['icon'] : '';
                         ?>"
                        />
                </div>

                <div class="span5">
                    <div class="row-fluid">
                    <div class="span4"><?= isset($data['id']) ? 'ID: <strong>' . $data['id'] . '</strong>' : ''; ?> Title</div>
                        <div class="span8"><input class="input" type="text" name="title" value="<?= isset($data['title']) ? $data['title'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Type</div>
                        <div class="span8">
                            <select name="category_id">
                                <option value="0"></option>
                                <?php foreach (Yii::app()->Core->getGamesCategories() as $categoryId => $categoryInfo) { ?>
                                    <option value="<?=$categoryInfo['id'];?>"<?= isset($data['category_id']) && $categoryInfo['id'] == $data['category_id'] ? ' selected="selected"' : ''; ?>><?=$categoryInfo['title'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Ticket</div>
                        <div class="span8"><input class="input" type="text" name="ticket" value="<?= isset($data['ticket']) ? $data['ticket'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Email</div>
                        <div class="span8"><input class="input" type="text" name="email" value="<?= isset($data['email']) ? $data['email'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Phone</div>
                        <div class="span8"><input type="text" name="phone" value="<?= isset($data['phone']) ? $data['phone'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Location</div>
                        <div class="span8"><input type="text" name="location" class="phone" value="<?= isset($data['location']) ? $data['location'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Author</div>
                        <div class="span8"><input type="text" name="author" value="<?= isset($data['author']) ? $data['author'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Author type</div>
                        <div class="span8">
                            <select name="author_type">
                                <?php foreach (Yii::app()->Core->getGameAuthorTypes() as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['author_type']) && $k == $data['author_type'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Stage of development</div>
                        <div class="span8">
                            <select name="stage">
                                <?php foreach (Yii::app()->Core->getGameDevelopmentStages() as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['stage']) && $k == $data['stage'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Description</div>
                        <div class="span8"><textarea name="description" rows="5"><?= isset($data['description']) ? $data['description'] : ''; ?></textarea></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Publish date</div>
                        <div class="span8"><?= isset($data['date']) ? substr($data['date'], 0, 10) : ''; ?></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Contact person</div>
                        <div class="span8"><?= isset($data['contact_person']) ? $data['contact_person'] : ''; ?></div>
                    </div>

                    <?php if($data['skype']): ?>
                        <div class="row-fluid">
                            <div class="span4">Skype</div>
                            <div class="span8"><?= $data['skype']; ?></div>
                        </div>
                    <?php endif; ?>
                    <div class="row-fluid">
                        <div class="span4">Team Size</div>
                        <div class="span8">
                            <select name="team_size">
                                <?php foreach (array('1', '2-3', '4-10', '11-20', '50', '>50') as $k => $v) { ?>
                                    <option value="<?= $v; ?>"<?= isset($data['team_size']) && $v == $data['team_size'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Game links</div>
                        <div class="span8">
                            <?php
                            $links = unserialize($data['link']);
                            $platforms = unserialize($data['platforms']);
                            $gamelinks = array();
                            foreach($platforms as $platform){
                                if(isset($platform['childs']) && is_array($platform['childs']) && count($platform['childs'])){
                                    foreach($platform['childs'] as $subplatform){
                                        if(trim($subplatform['link'])){
                                            $gamelinks[] = $subplatform;
                                        }
                                    }
                                }
                            }
                            foreach($gamelinks as $gamelink):
                                ?>
                                <input type="text" value="<?= $gamelink['link']; ?>" disabled /> - <?=$gamelink['title'];?><br/>
                            <?php endforeach; ?>
                            <input type="text" value="<?= $links['company']; ?>" disabled /> - company<br/>
                        </div>

                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span4">Gameplay video</div>
                        <div class="span8"><input type="text" name="video" value="<?= isset($data['video']) ? $data['video'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid" style="white-space: nowrap;">
                        <div class="span4">To Lynch</div>
                        <div class="span8">
                            <select name="lynch">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['lynch']) && $k == $data['lynch'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                            <?php if($data['lynch']) echo ($data['lynch_drink'])? ' - drink' : ' - not drink' ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">To Gamm:Play</div>
                        <div class="span8">
                            <select name="gammplay">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['gammplay']) && $k == $data['gammplay'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <?php if($data['gammplay']): ?>
                        <div class="row-fluid">
                            <div class="span4">Device for SC:</div>
                            <div class="span8"><?=$data['showcase_device'];?></div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">Time-slots for SC:</div>
                            <div class="span8"><?=$data['showcase_time'];?></div>
                        </div>

                    <?php endif; ?>

                    <div class="row-fluid">
                        <div class="span4">Free To Play</div>
                        <div class="span8">
                            <select name="f2p">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['f2p']) && $k == $data['f2p'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Premium</div>
                        <div class="span8">
                            <select name="premium">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['premium']) && $k == $data['premium'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Multiplayer</div>
                        <div class="span8">
                            <select name="multiplayer">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['multiplayer']) && $k == $data['multiplayer'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">Icon</div>
                        <div class="span8">
                            <?php echo ($data['icon']) ? '<a href="'.$data['icon'].'" target="_blank">'.$data['icon'].'</a>' : ''; ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">Screenshots</div>
                        <div class="span8">
                            <?php
                           $screenshots = explode(',', $data['screenshoots']);
                            if($screenshots){
                                foreach($screenshots as $screenshot){
                                    echo '<div><a href="'.$screenshot.'" target="_blank">'.$screenshot.'</a></div>';
                                }
                            }
                            ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">Platforms</div>
                        <div class="span8">
                            <ul>
                                <?php
                                $platforms = unserialize($data['platforms']);
                                if(is_array($platforms)){
                                    foreach($platforms as $platform){
                                        echo '<li>';
                                        echo $platform['title'];
                                        if(isset($platform['childs']) && is_array($platform['childs']) && count($platform['childs'])){
                                            echo '<ul style="padding-left: 25px;">';
                                            foreach($platform['childs'] as $subplatform){
                                                echo '<li>'.$subplatform['title'].'</li>';
                                            }
                                            echo '</ul>';
                                        }
                                        echo '</li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">Technology</div>
                        <div class="span8">
                            <?php
                            $technologies = unserialize($data['technology']);
                            $technologiesArray = array();
                            foreach($technologies as $technology){
                                $technologiesArray[] = (!empty($technology['comment']))? $technology['title'].': '.$technology['comment'] : $technology['title'];
                            }
                            echo implode(', ', $technologiesArray);
                            ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span4">Awards</div>
                        <div class="span8">
                            <select name="awards">
                                <?php foreach (array('no', 'yes') as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['awards']) && $k == $data['awards'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <?php if($data['awards']): ?>
                        <div class="row-fluid">
                            <div class="span4">Awards nominations</div>
                            <div class="span8"><?=$data['awards_nominations'];?></div>
                        </div>
                    <?php endif; ?>
                    <br/>
                    <div class="row-fluid">
                        <div class="span4">More info</div>
                        <div class="span3">
                            <?= CHtml::link('View game', array('awards/gameview', 'id'=>$data['id']), array('class'=>'btn btn-success', 'target'=>'_blank')); ?>
                        </div>
                        <div class="span3">
                            <?= CHtml::link('Update game', array('awards/register', '#'=>'/edit-game?token='.$data['token']), array('class'=>'btn btn-success', 'target'=>'_blank')); ?>
                        </div>
                    </div>
                    <br/>
                    <div class="row-fluid">
                        <div class="span4">Is approved?</div>
                        <div class="span8">
                            <a href="" onclick="return false;" data-id="<?= $data['id']; ?>" data-type="<?= $data['approved']; ?>" class="approve btn<?= $data['approved'] ? ' btn-success' : '' ?>"><?= $data['approved'] ? '' : 'NOT '; ?>Approved</a>
                        </div>
                    </div>
                </div>
                <div class="span1">
                    <a href="" onclick="return false;" style="display: none;" id="<?= isset($data['id']) ? $data['id'] : ''; ?>" class="save-row"><span class="icon icon-ok"></span></a>
                </div>
            </div>
        </div>
    </div>
    <hr>
</form>

