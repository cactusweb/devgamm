<form>
    <div class="row-fluid<?= !empty($data) ? ' participant' : ''; ?>">
        <div class="span12">
            <div class="row-fluid">
                <div class="span1" data-rating="<?= isset($data['rating']) ? $data['rating'] : ''; ?>" data-id="<?= isset($data['id']) ? $data['id'] : ''; ?>">
                    <?php if ($data) { ?>
                        <a href="" onclick="return false;" class="up-rating"><span class="icon icon-arrow-up"></span></a><a href="" onclick="return false;" class="down-rating"><span class="icon icon-arrow-down"></span></a><br/>
                        <a href="" onclick="return false;" class="remove"><span class="icon icon-trash"></span></a>
                    <?php } ?>
                </div>
                <div class="span2"><img class="img-polaroid photo-preview" border="0" height="200px" src="<?= isset($data['photo']) ? Yii::app()->Core->imgHost . $data['photo'] : ''; ?>"/></div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Email</div>
                        <div class="span9"><input class="input" type="text" name="email" value="<?= isset($data['email']) ? $data['email'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Name (en)</div>
                        <div class="span9"><input type="text" name="name_en" value="<?= isset($data['name_en']) ? $data['name_en'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Name (ru)</div>
                        <div class="span9"><input type="text" name="name_ru" value="<?= isset($data['name_ru']) ? $data['name_ru'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Photo</div>
                        <div class="span9"><input type="text" name="photo" class="photo" value="<?= isset($data['photo']) ? $data['photo'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Company title</div>
                        <div class="span9"><input type="text" name="company_title" value="<?= isset($data['company_title']) ? $data['company_title'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Company url</div>
                        <div class="span9"><input type="text" name="company_url" value="<?= isset($data['company_url']) ? $data['company_url'] : ''; ?>" placeholder="renatus.com"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Position</div>
                        <div class="span9"><input type="text" name="position" value="<?= isset($data['position']) ? $data['position'] : ''; ?>"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Looking for (en)</div>
                        <div class="span9"><textarea name="lookingfor_en" rows="5"><?= isset($data['lookingfor_en']) ? $data['lookingfor_en'] : ''; ?></textarea></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Propose (en)</div>
                        <div class="span9"><textarea name="propose_en" rows="5"><?= isset($data['propose_en']) ? $data['propose_en'] : ''; ?></textarea></div>
                    </div>
                </div>
                <div class="span1">
                    <a href="" onclick="return false;" style="display: none;" id="<?= isset($data['id']) ? $data['id'] : ''; ?>" class="save-row"><span class="icon icon-ok"></span></a>
                </div>
            </div>
        </div>
    </div>
</form>

