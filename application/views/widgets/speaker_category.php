<form>
    <div class="row-fluid<?= !empty($data) ? ' participant' : ''; ?>">
        <div class="span12">
            <div class="row-fluid">
                <div class="span1"></div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Title (en)</div>
                        <div class="span9"><input class="input" type="text" name="title_en" value="<?= isset($data['title_en']) ? $data['title_en'] : ''; ?>"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Title (ru)</div>
                        <div class="span9"><input class="input" type="text" name="title_ru" value="<?= isset($data['title_ru']) ? $data['title_ru'] : ''; ?>"></div>
                    </div>
                </div>
                <div class="span1">
                    <input type='hidden' name="color" class="color-value" id="color-<?= isset($data['id']) ? $data['id'] : 'new'; ?>" value="<?= isset($data['color']) ? $data['color'] : ''; ?>"/>
                </div>
                <div class="span1">
                    <a href="" onclick="return false;" style="display: none;" id="sc<?= isset($data['id']) ? $data['id'] : ''; ?>" class="save-row"><span class="icon icon-ok"></span></a>
                    <?php if (isset($data['id'])) { ?>
                        <a href="#" data-id="<?=$data['id'];?>" class="delete-category" title="Delete category"><span class="icon-trash"> </span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</form>