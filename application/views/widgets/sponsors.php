<form>
    <div class="row-fluid<?= !empty($data) ? ' participant' : ''; ?>">
        <div class="span12">
            <div class="row-fluid">
                <div class="span1" data-rating="<?= isset($data['rating']) ? $data['rating'] : ''; ?>" data-id="<?= isset($data['id']) ? $data['id'] : ''; ?>">
                    <?php if ($data) { ?>
                        <a href="" onclick="return false;" class="up-rating"><span class="icon icon-arrow-up"></span></a><a href="" onclick="return false;" class="down-rating"><span class="icon icon-arrow-down"></span></a><br/>
                        <a href="" onclick="return false;" class="remove"><span class="icon icon-trash"></span></a>
                    <?php } ?>
                </div>
                <div class="span2"><img class="img-polaroid photo-preview" border="0" height="200px" src="<?= isset($data['logo']) ? Yii::app()->Core->imgHost . $data['logo'] : ''; ?>"/></div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Email</div>
                        <div class="span9"><input class="input" type="text" name="email" value="<?= isset($data['email']) ? $data['email'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Url</div>
                        <div class="span9"><input class="input" type="text" name="url" value="<?= isset($data['url']) ? $data['url'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Title (en)</div>
                        <div class="span9"><input type="text" name="title_en" value="<?= isset($data['title_en']) ? $data['title_en'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Title (ru)</div>
                        <div class="span9"><input type="text" name="title_ru" value="<?= isset($data['title_ru']) ? $data['title_ru'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Logo</div>
                        <div class="span9"><input type="text" name="logo" class="photo" value="<?= isset($data['logo']) ? $data['logo'] : ''; ?>"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Category</div>
                        <div class="span9">
                            <select name="category_id">
                                <?php foreach (Yii::app()->Core->sponsorTypes as $k => $v) { ?>
                                    <option value="<?= $k; ?>"<?= isset($data['category_id']) && $k == $data['category_id'] ? ' selected="selected"' : ''; ?>><?= $v; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="row-fluid">
                        <div class="span3">Description (en)</div>
                        <div class="span9"><textarea name="description_en" rows="5"><?= isset($data['description_en']) ? $data['description_en'] : ''; ?></textarea></div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3">Description (ru)</div>
                        <div class="span9"><textarea name="description_ru" rows="5"><?= isset($data['description_ru']) ? $data['description_ru'] : ''; ?></textarea></div>
                    </div>
                </div>
                <div class="span1">
                    <a href="" onclick="return false;" style="display: none;" id="<?= isset($data['id']) ? $data['id'] : ''; ?>" class="save-row"><span class="icon icon-ok"></span></a>
                </div>
            </div>
        </div>
    </div>
</form>
