<?php

class Participant extends Widget
{
    public $data = array();
    public $type = 'participant';

    public function run()
    {
        $this->render($this->type, array(
            'data' => $this->data,
        ));
    }
}