<?php

class SearchCriteria extends Widget
{
    public $fields = array();
    public $form = array();
    public $data = array();
    public $autoRun = false;
    public $callback = '';
    public $isShowAnswer = true;
    public $h3 = '';
    public $h4 = '';
    public $isCSVAvailable = false;

    public function init()
    {
        if (empty($this->fields))
        {
            throw new CException(__METHOD__ . ' - can\'t process with empty fields or form data', E_USER_ERROR);
        }
        if (empty($this->form))
        {
            $this->form = array(
                'name' => 'SearchCriteria',
            );
        }
        return true;
    }

    public function run()
    {
        $fields = array();

        foreach ($this->fields as $field => $params)
        {
            if (!is_array($params))
            {
                $fields[] = $this->getHTML($params, $field);
                continue;
            }
            $fields[] = $this->getHTML(
                $params['type'],
                $field,
                isset($params['label']) ? $params['label'] : null,
                isset($params['value']) ? $params['value'] : null,
                isset($params['id']) ? $params['id'] : null,
                isset($params['class']) ? $params['class'] : null,
                isset($params['cloneable']) ? true : null
            );
        }
        return $this->render(__CLASS__, array(
            'fields' => $fields,
            'data' => $this->data,
            'autoRun' => $this->autoRun,
            'isShowAnswer' => $this->isShowAnswer,
            'h3' => $this->h3,
            'h4' => $this->h4,
            'isCSVAvailable' => $this->isCSVAvailable,
            'form' => $this->form,
            'callback' => $this->callback,
        ));
    }

    public function getHTML($type, $fieldTitle, $fieldLabel = null, $preValue = null, $htmlId = null, $htmlClass = null, $cloneable = null)
    {
        if (!isset($fieldTitle))
        {
            throw new CException(__METHOD__ . ' - incorrect input params specified', E_USER_ERROR);
        }
        if (isset($this->_getAvailableTypes()[$type]))
        {
            return $this->_getElementCode($type, $fieldTitle, $fieldLabel, $preValue, $htmlId, $htmlClass, $cloneable);
        }
        $field = $this->_getAvailableFields()[$type];
        if (is_array($field))
        {
            $answer = '';
            foreach ($field as $k => $v)
            {
                $fieldLabel = (is_array($fieldLabel) && isset($fieldLabel[$k])) ? $fieldLabel[$k] : '';
                $answer .= $this->getHTML($v, $fieldTitle . '[' . $k . ']', $fieldLabel, ((is_array($preValue) && isset($preValue[$k])) ? $preValue[$k] : ''), $htmlId, $htmlClass, $cloneable) . '&nbsp;&mdash;&nbsp;';
            }
            return substr($answer, 0, -19);
        }
        return $this->_getElementCode($field, $fieldTitle, $fieldLabel, $preValue, $htmlId, $htmlClass, $cloneable);
    }

    public static function getCountries()
    {
        return array(
            'AD' => 'Andorra',
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AP' => 'Asia/Pacific Region',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BQ' => 'Bonaire, Saint Eustatius and Saba',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CD' => 'Congo, The Democratic Republic of the',
            'CG' => 'Congo',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote d\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CW' => 'Curacao',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'EU' => 'Europe',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GW' => 'Guinea-Bissau',
            'GN' => 'Guinea',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island and McDonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KP' => 'Korea, Democratic People\'s Republic of',
            'KR' => 'Korea, Republic of',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States of',
            'MD' => 'Moldova, Republic of',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Bartelemey',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre and Miquelon',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SX' => 'Sint Maarten',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'SS' => 'South Sudan',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard and Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania, United Republic of',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'UM' => 'United States Minor Outlying Islands',
            'US' => 'United States',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis and Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
            '0' => 'Not detected',
             0 => 'Not detected',
             '' => 'Not detected',
            'A1' => 'Anonymous Proxy',
            'A2' => 'Satellite Provider',
            'O1' => 'Other Country',
        );
    }

    public static function getTimeShifts()
    {
        $timeShifts = array();
        for ($i = -12; $i <= 12; $i++)
        {
            $timeShifts[$i] = ($i > 0 ? '+' : '') . $i;
        }
        $timeShifts[0] = array('value' => 0);
        return $timeShifts;
    }

    public static function getAppsCriteria($isAddCommercial = true)
    {
        return Yii::app()->Analytics->getAppsCriteria($isAddCommercial);
    }

    public static function getAppsByPlatformCriteria($isAddCommercial = true)
    {
        $apps = $userApps = array();
        if (!empty(Yii::app()->user->apps))
        {
            $userApps = json_decode(Yii::app()->user->apps);
            $userApps = array_flip($userApps);
        }
        foreach (Yii::app()->Core->apps as $app => $data)
        {
            if ($isAddCommercial)
            {
                $apps[$data['p']][0] = '-Commercial-';
            }
            if (!empty($userApps) && !isset($userApps[$app]))
            {
                continue;
            }
            $apps[$data['p']][] = $data['title'];
        }
        return $apps;
    }

    public static function getMobileAppsCriteria($isAddCommercial = true)
    {
        $apps = SearchCriteria::getAppsByPlatformCriteria($isAddCommercial);
        return array_merge(isset($apps['ios']) ? $apps['ios'] : array(), isset($apps['android']) ? $apps['android'] : array());
    }

    protected function _getAvailableFields()
    {
        return array(
            'date' => 'datePicker',
            'dateRange' => array(
                'from' => 'datePicker',
                'to' => 'datePicker',
            ),
            'list' => 'select',
            'field' => 'input',
            'fieldRange' => array(
                'from' => 'input',
                'to' => 'input',
            ),
        );
    }

    protected function _getAvailableTypes()
    {
        return array(
            'datePicker' => 1,
            'select' => 1,
            'input' => 1,
            'checkbox' => 1,
            'line-break' => 1,
            'typeahead' => 1,
        );
    }

    private function _getElementCode($type, $fieldTitle, $fieldLabel, $preValue = null, $htmlId = null, $htmlClass = null, $cloneable = null)
    {
        switch ($type)
        {
            case 'typeahead':
            case 'input':
            case 'datePicker':
                $answer = '';
                if ($cloneable)
                {
                    $answer .= '<div><div>';
                }
                $answer .= (!empty($fieldLabel)) ? ('<label' . (isset($htmlId) ? ' for="' . $htmlId . '"' : '') . '>' . $fieldLabel . '</label>') : '';
                return  $answer . '<input'
                    . (isset($htmlId) ? ' id="' . $htmlId . '"' : '')
                    . ' type="text" name="' . $fieldTitle . ($cloneable ? '[]' : '') . '"'
                    . (isset($preValue) ? ('typeahead' == $type ? ' data-source=\'' . $preValue['_data_source'] . '\' value="' . $preValue['_value'] . '"' : ' value="' . $preValue . '"') : '')
                    . ' class="input' . ($type == 'datePicker' ? ' date-picker' : '') . (isset($htmlClass) ? ' ' . $htmlClass : '') . '"'
                    . ('typeahead' == $type ? ' autocomplete="off" data-provide="typeahead"' : '') . '/>' . ($cloneable ? '&nbsp;<a href="" data-type="manage-clone" onclick="$(this).parent().clone().find(\'input\').val(\'\').parent().find(\'a[data-type=manage-clone]\').attr(\'onclick\', \'$(this).parent().remove(); return false;\').text(\'&mdash;\').parent().appendTo($(this).parent().parent()); return false;">+</a></div></div>' : '');
                break;
            case 'select':
                $answer = '<div><div>' . (!empty($fieldLabel) ? "<label>$fieldLabel</label>" : '');
                $answer .= '<select' . (isset($htmlId) ? ' id="' . $htmlId . '"' : '') . ' name="' . $fieldTitle . ($cloneable ? '[]' : '') . '" class="select' . (isset($htmlClass) ? ' ' . $htmlClass : '') . '">';
                if (!empty($preValue) && is_array($preValue))
                {
                    foreach ($preValue as $k => $v)
                    {
                        if (is_array($v) && isset($v['value']))
                        {
                            $answer .= '<option value="' . $k . '" selected="selected">' . $v['value'] . '</option>';
                        }
                        elseif (!is_array($v))
                        {
                            $answer .= '<option value="' . $k . '">' . $v . '</option>';
                        }
                    }
                }
                return $answer . '</select>' . ($cloneable ? '&nbsp;<a href="" data-type="manage-clone" onclick="$(this).parent().clone().find(\'a\').attr(\'onclick\', \'$(this).parent().remove(); return false;\').text(\'&mdash;\').parent().appendTo($(this).parent().parent()); return false;">+</a>' : '') . '</div></div>';
                break;
            case 'line-break':
                return '</div><div class="controls">';
                break;
            case 'checkbox':
                return  '<label' . (isset($htmlId) ? ' for="' . $htmlId . '"' : '') . ' class="checkbox"><input' . (isset($htmlId) ? ' id="' . $htmlId . '"' : '') . ' type="checkbox" name="' . $fieldTitle . '"' . (isset($htmlClass) ? ' class="' . $htmlClass . '"' : '') . '/>'. (!empty($fieldLabel) ? $fieldLabel : '') . '</label>';
            default:
                throw new CException(__METHOD__ . ' - incorrect type specified: {' . $type . '}', E_USER_ERROR);
                break;
        }
        return '';
    }
}