   <div class="game-form-wrapper">
       <ul class="dg-breadcrumbs">
           <li>
               <a ui-sref="step1" class="active">1. Ticket</a>
           </li>
           <li>
               <a ui-sref="step2">2. Game</a>
           </li>
           <li>
               <a ui-sref="step3">3. Developer</a>
           </li>
           <li>
               <a ui-sref="step4">4. Awards</a>
           </li>
           <li>
               <a ui-sref="step5">5. Game Lynch</a>
           </li>
           <li>
               <a ui-sref="step6">6. GAMM:Play</a>
           </li>
       </ul>
        <form name="formS1" class="form-horizontal" ng-submit="form.submit()">

            <div class="form-group required">
                <label class="col-sm-4 control-label">
                    Ticket
                    <div class="hint"><a href="#" id="ticket-photo" >What is it?</a></div>
                </label>
                <div class="col-sm-8">
                    <div class="row ticket-number">
                        <div class="col-xs-2" ng-class="{'has-error': formS1.t1.$dirty && formS1.t1.$invalid}">
                            <input type="text" name="t1" ng-model="ticket.t1"
                                   class="form-control text-center t1"
                                   ng-required="true" ng-minlength="4" ng-maxlength="4"
                                   minlength="4" maxlength="4" ng-disabled="form.blocked">
                        </div>
                        <div class="col-xs-2" ng-class="{'has-error': formS1.t2.$dirty && formS1.t2.$invalid}">
                            <input type="text" name="t2" ng-model="ticket.t2"
                                   class="form-control text-center t2"
                                   ng-required="true" ng-minlength="4" ng-maxlength="4" maxlength="4"
                                   minlength="4" maxlength="4" ng-disabled="form.blocked">
                        </div>
                        <div class="col-xs-2"  ng-class="{'has-error': formS1.t3.$dirty && formS1.t3.$invalid}">
                            <input type="text" name="t3" ng-model="ticket.t3"
                                   class="form-control text-center t3"
                                   ng-required="true" ng-minlength="4" ng-maxlength="4"
                                   minlength="4" maxlength="4" ng-disabled="form.blocked">
                        </div>
                        <div class="col-xs-2" ng-class="{'has-error': formS1.t4.$dirty && formS1.t4.$invalid}">
                            <input type="text" name="t4" ng-model="ticket.t4"
                                   class="form-control text-center t4"
                                   ng-required="true" ng-minlength="4" ng-maxlength="4"
                                   minlength="4" maxlength="4" ng-disabled="form.blocked">
                        </div>
                        <div class="col-xs-2 throbber" ng-show="form.blocked"></div>
                    </div>

                    <div ng-show="formS1.$dirty || form.ticketHasError()" >
                        <div ng-show="formS1.$invalid" class="error">Wrong format of ticket number</div>
                        <div ng-show="ticket.error.notExists" class="error">This ticket does not exist in the system. Please contact organizers: contact@devgamm.com </div>
                        <div ng-show="ticket.error.alreadyUsed" class="error">This ticket already used for another game submission &laquo;{{ticket.error.game_title}}&raquo; <br>You can submit 1 game per ticket.</div>
                    </div>
                    <br>
                    <div ng-show="ticket.info.id">Ticket #<b>{{ticket.info.code}}</b> paid for {{ticket.info.firstname}} {{ticket.info.lastname}}.</div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button ng-disabled="formS1.$invalid" type="submit" class="btn btn-success">Continue</button>
                </div>
            </div>
            <div>
                <img src="/i/awards/ticket_number.png" class="ticket-photo" />
            </div>
        </form>
    </div>
