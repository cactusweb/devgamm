<div class="game-form-wrapper">
    <ul class="dg-breadcrumbs" ng-if="!editMode">
        <li>
            <a ui-sref="step1">1. Ticket</a>
        </li>
        <li>
            <a ui-sref="step2" class="active">2. Game</a>
        </li>
        <li>
            <a ui-sref="step3">3. Developer</a>
        </li>
        <li>
            <a ui-sref="step4">4. Awards</a>
        </li>
        <li>
            <a ui-sref="step5">5. Game Lynch</a>
        </li>
        <li>
            <a ui-sref="step6">6. GAMM:Play</a>
        </li>
    </ul>
    <ul class="dg-breadcrumbs" ng-if="editMode">
        <li>
            <a class="inactive">1. Ticket</a>
        </li>
        <li>
            <a ui-sref="step2" class="active">2. Game</a>
        </li>
        <li>
            <a ui-sref="step3" class="">3. Developer</a>
        </li>
        <li>
            <a class="inactive">4. Awards</a>
        </li>
        <li>
            <a class="inactive">5. Game Lynch</a>
        </li>
        <li>
            <a class="inactive">6. GAMM:Play</a>
        </li>
    </ul>
    <form class="form-horizontal" name="formS2" ng-submit="form.submit()">

        <div class="form-group required">
            <label class="col-sm-4 control-label">Game title</label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" name="gameTitle" class="form-control"
                               ng-required="true" ng-model="game.title"
                               ng-pattern="/^[^А-Яа-яіє]*$/">
                    </div>
                </div>
                <div ng-show="formS2.gameTitle.$touched && formS2.gameTitle.$invalid" class="error">Please add the game
                    title
                </div>
                <div ng-show="formS2.gameTitle.$error.pattern" class="error">Only latin symbols and digits please</div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Short game description
                <div class="hint">max 140 symbols</div>
                <span class="hint">{{140 - game.description.length}}</span>
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                            <textarea name="gameDescription" class="form-control" required="1" rows="4" maxlength="140"
                                      ng-model="game.description" ng-pattern="/^[^А-Яа-яіє]*$/"></textarea>
                    </div>
                </div>
                <div ng-show="formS2.gameDescription.$touched && formS2.gameDescription.$invalid" class="error">Please
                    add the game description
                </div>
                <div ng-show="formS2.gameDescription.$error.pattern" class="error">Only latin symbols and digits
                    please
                </div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Your game is for
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="checkbox-group">
                            <div class="checkbox" ng-repeat="category in game.platform">
                                <label>
                                    <input type="checkbox" ng-model="category.checked"
                                           ng-change="form.uncheckAll(category)"/>
                                    {{category.title}}
                                </label>

                                <div class="checkbox-group" ng-show="category.checked">
                                    <div class="checkbox" ng-repeat="subcat in category.childs">
                                        <label>
                                            <input type="checkbox" ng-model="subcat.checked"
                                                   ng-change="form.categoryChanged(subcat)"/>
                                            {{subcat.title}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div ng-show="game.error.platform" class="error">Please choose at least one category</div>
                    </div>
                </div>
            </div>
        </div>

        <div ng-show="form.isOkPlatform()">
            <div class="form-group required">
                <label class="col-sm-4 control-label">
                    Link to the game
                </label>
            </div>
            <div ng-repeat="category in game.platform">
                <div class="form-group form-inline" ng-repeat="subcat in category.childs" ng-show="subcat.checked">
                    <label class="col-sm-4 control-label sublabel">{{subcat.title}} version:</label>

                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-12" ng-init="catInputName='category-' + subcat.id" style="white-space: nowrap;">
                                <input type="url" class="form-control subcat_link"
                                       name="{{catInputName}}"
                                       ng-model="subcat.link"
                                       ng-pattern="/^[^А-Яа-яіє]*$/"
                                       valid-link>
                                <input type="checkbox" class="checkbox"
                                       ng-true-value="1"
                                       ng-false-value="0"
                                       ng-model="subcat.public_link"> public link
                            </div>
                        </div>
                        <div ng-show="formS2[catInputName].$error.pattern" class="error">Only latin symbols and digits
                            please
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label sublabel"></label>

                <div class="col-sm-8">
                    <div ng-show="form.isOkPlatform() && !form.isOkLink()" class="error">Please add at least one valid
                        link to your game
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Gameplay video
                <div class="hint">Link to YouTube</div>
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="url" class="form-control" name="linkVideo"
                               ng-required="true"
                               ng-model="game.linkVideo"
                               ng-pattern="/^[^А-Яа-яіє]*$/"
                               valid-link>
                    </div>
                </div>
                <div ng-show="formS2.linkVideo.$invalid" class="error">Please add valid
                    link to gameplay video
                </div>
                <div ng-show="formS2.linkVideo.$error.pattern" class="error">Only latin symbols and digits
                    please
                </div>
            </div>
        </div>

        <div class="form-group" ng-class="{required: !editMode}">
            <label class="col-sm-4 control-label">
                Screenshoots
            </label>

            <div class="col-sm-8">
                <div class="form-group row">
                    <div class="col-xs-12">
                        <div ng-show="game.screenshots.length">
                            <table>
                                <tr ng-repeat="file in game.screenshots">
                                    <td>{{$index + 1}}. {{file.name}}</td>
                                    <td><span class="delete" ng-click="form.deleteScreenshot($index)"></span></td>
                                </tr>
                            </table>
                        </div>
                        <button type="button" valid-file type="file" name="screenshot"
                                ng-model="form.screenshot"
                                ng-file-select
                                multiple="false"
                                accept="'image/jpeg,image/png'"
                                ng-accept="'image/jpeg,image/png'"
                                resetOnClick="false"
                                ng-disabled="game.screenshots.length == 3"
                            >Add screenshot...
                        </button>

                        <div class="upload_status" ng-show="form.upload && form.filePreloader"></div>
                    </div>
                </div>
                <div ng-show="!editMode && game.error.screenshots" class="error">Wrong image format, please add JPG or PNG</div>
                <div ng-show="!editMode && game.screenshots.length < 1" class="error">Please add at least one screenshot </div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Technology
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="checkbox-group">
                            <div class="checkbox" ng-repeat="tech in game.tech">
                                <label class="form-group-sm">
                                    <input type="checkbox" ng-model="tech.checked"/>
                                    {{tech.title}}
                                    <input type="text" class="form-control" ng-model="tech.comment"
                                           ng-show="tech.checked && tech.need_comment==1">
                                </label>
                            </div>
                        </div>
                        <div ng-show="!form.isOkTech()" class="error">Please choose at least one technology</div>
                    </div>

                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">
                Additional
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="checkbox-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="game.premium"/>
                                    Premium
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="game.freeToPlay"/>
                                    Free-to-pay
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="game.multiplayer"/>
                                    Multiplayer
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `
        </div>

        <div class="form-group" ng-class="{required: !editMode}">
            <label class="col-sm-4 control-label">
                Game icon
                <div class="hint">100x100px, jpg, gif or png</div>
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <div ng-show="game.icon.name">
                            <table>
                                <tr>
                                    <td>{{game.icon.name}}</td>
                                    <td><span class="delete" ng-click="form.deleteIcon()"></span></td>
                                </tr>
                            </table>
                        </div>
                        <button type="button" valid-file type="file" name="icon"
                                ng-model="form.icon"
                                ng-file-select
                                multiple="false"
                                accept="'image/jpeg,image/png'"
                                ng-accept="'image/jpeg,image/png'"
                                resetOnClick="false"
                                ng-disabled="game.icon.name"
                            >Add icon...
                        </button>

                        <div class="upload_status" ng-show="form.upload && form.filePreloader"></div>
                    </div>
                </div>
                <div ng-show="!editMode && !form.isOkIcon()" class="error">Please add an icon</div>
                <div ng-show="!editMode && formS2.icon.$touched && game.error.ico" class="error">Wrong size, it should
                    be 100x100px
                </div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Stage of development
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <select ng-model="game.devStage">
                            <option value="1">Prototype</option>
                            <option value="2">Alpha</option>
                            <option value="3">Beta</option>
                            <option value="4">Completed</option>
                            <option value="5">Released</option>
                        </select>

                        <div ng-show="game.error.stage" class="error">Please choose stage of development</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="button" class="btn btn-default" ui-sref="step1">Back</button>
                <button ng-disabled="formS2.$invalid" type="submit" class="btn btn-success">Continue</button>
            </div>
        </div>
    </form>
</div>
