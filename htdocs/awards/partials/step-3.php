<div class="game-form-wrapper">
    <ul class="dg-breadcrumbs" ng-if="!editMode">
        <li>
            <a ui-sref="step1">1. Ticket</a>
        </li>
        <li>
            <a ui-sref="step2">2. Game</a>
        </li>
        <li>
            <a ui-sref="step3" class="active">3. Developer</a>
        </li>
        <li>
            <a ui-sref="step4">4. Awards</a>
        </li>
        <li>
            <a ui-sref="step5">5. Game Lynch</a>
        </li>
        <li>
            <a ui-sref="step6">6. GAMM:Play</a>
        </li>
    </ul>
    <ul class="dg-breadcrumbs" ng-if="editMode">
        <li>
            <a class="inactive">1. Ticket</a>
        </li>
        <li>
            <a ui-sref="step2" class="">2. Game</a>
        </li>
        <li>
            <a ui-sref="step3" class="active">3. Developer</a>
        </li>
        <li>
            <a class="inactive">4. Awards</a>
        </li>
        <li>
            <a class="inactive">5. Game Lynch</a>
        </li>
        <li>
            <a class="inactive">6. GAMM:Play</a>
        </li>
    </ul>
    <form class="form-horizontal" name="formS3" ng-submit="form.submit()">

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Author
                <div class="hint">Your name or company name</div>
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="devAuthor" ng-required="true"
                               ng-model="dev.author" ng-pattern="/^[^А-Яа-яіє]*$/"/>
                    </div>
                </div>
                <div ng-show="formS3.devAuthor.$touched && formS3.devAuthor.$invalid" class="error">Please add an author
                    name
                </div>
                <div ng-show="formS3.devAuthor.$error.pattern" class="error">Only latin symbols and digits please</div>
                <div class="radio">
                    <label>
                        <input type="radio" ng-model="dev.type" value="0">
                        Independent developer
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" ng-model="dev.type" value="1">
                        Studio
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" ng-model="dev.type" value="2">
                        Company
                    </label>
                </div>
                <div ng-show="dev.type == undefined" class="error">Please add an developer type
                </div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Website
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="url" class="form-control" name="devSite" ng-required="true"
                               ng-model="dev.site"
                               ng-pattern="/^[^А-Яа-яіє]*$/"
                               valid-link/>
                    </div>
                </div>
                <div ng-show="formS3.devSite.$invalid" class="error">Please add valid website
                    URL
                </div>
                <div ng-show="formS3.devSite.$error.pattern" class="error">Only latin symbols and digits
                    please
                </div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                How many people worked on game?
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <select ng-model="dev.team" name="devTeam" required>
                            <option></option>
                            <option value="1">1</option>
                            <option value="2-3">2-3</option>
                            <option value="4-10">4-10</option>
                            <option value="11-20">11-20</option>
                            <option value="50">50</option>
                            <option value=">50">>50</option>
                        </select>

                        <div ng-show="formS3.devTeam.$invalid" class="error">Please choose a team size</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Name of contact person
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="devName" ng-required="true"
                               ng-model="dev.name" ng-pattern="/^[^А-Яа-яіє]*$/"/>

                    </div>
                </div>
                <div ng-show="formS3.devName.$touched && formS3.devName.$invalid" class="error">Please add a contact
                    person name
                </div>
                <div ng-show="formS3.devName.$error.pattern" class="error">Only latin symbols and digits please</div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                E-mail
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="email" class="form-control" name="devEmail" ng-required="true"
                               ng-model="dev.email" ng-pattern="/^[^А-Яа-яіє]*$/"/>
                    </div>
                </div>
                <div ng-show="formS3.devEmail.$touched && formS3.devEmail.$invalid" class="error">Please add the contact
                    email
                </div>
                <div ng-show="formS3.devEmail.$error.pattern" class="error">Only latin symbols and digits please</div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Phone
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="devPhone" ng-required="true"
                               ng-model="dev.phone" ng-pattern="/^[^A-Za-zА-Яа-яіє]*$/"/>
                    </div>
                </div>
                <div ng-show="formS3.devPhone.$touched && formS3.devPhone.$invalid" class="error">Please add the contact
                    phone
                </div>
                <div ng-show="formS3.devPhone.$error.pattern" class="error">Only digits, hyphen, space, + and ()
                    allowed
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">
                Skype
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="form-control"
                               name="devSkype"
                               ng-model="dev.skype"
                               ng-pattern="/^[^А-Яа-яіє]*$/" />
                    </div>
                </div>
                <div ng-show="formS3.devSkype.$error.pattern" class="error">Only latin symbols and digits please</div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                City
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="devCity" ng-required="true"
                               ng-model="dev.city" ng-pattern="/^[^А-Яа-яіє]*$/"/>
                    </div>
                </div>
                <div ng-show="formS3.devCity.$touched && formS3.devCity.$invalid" class="error">Please add the city
                    name
                </div>
                <div ng-show="formS3.devCity.$error.pattern" class="error">Only latin symbols and digits please</div>
            </div>
        </div>

        <div class="form-group required">
            <label class="col-sm-4 control-label">
                Country
            </label>

            <div class="col-sm-8">
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="devCountry" ng-required="true"
                               ng-model="dev.country" ng-pattern="/^[^А-Яа-яіє]*$/"/>
                    </div>
                </div>
                <div ng-show="formS3.devCountry.$touched && formS3.devCountry.$invalid" class="error">Please add the
                    country name
                </div>
                <div ng-show="formS3.devCountry.$error.pattern" class="error">Only latin symbols and digits please</div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="button" class="btn btn-default" ui-sref="step2">Back</button>
                <button ng-disabled="formS3.$invalid" type="submit" class="btn btn-success">Continue</button>
            </div>
        </div>
    </form>
</div>
