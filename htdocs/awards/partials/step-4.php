    <div class="game-form-wrapper">
        <ul class="dg-breadcrumbs">
            <li>
                <a ui-sref="step1">1. Ticket</a>
            </li>
            <li>
                <a ui-sref="step2">2. Game</a>
            </li>
            <li>
                <a ui-sref="step3">3. Developer</a>
            </li>
            <li>
                <a ui-sref="step4" class="active">4. Awards</a>
            </li>
            <li>
                <a ui-sref="step5">5. Game Lynch</a>
            </li>
            <li>
                <a ui-sref="step6">6. GAMM:Play</a>
            </li>
        </ul>
        <form class="form-horizontal" name="formS4" ng-submit="form.submit()">

            <div class="form-group" ng-show="awards.allowed">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value=""
                                           ng-model="awards.add"
                                           ng-disabled="!awards.allowed"/>
                                    Add game to <a href="{{awards.link}}" target="_blank">DevGAMM Awards</a> (presence at the conference is required)
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group" ng-show="awards.add">
                <label class="col-sm-12 control-label text-left">
                    I would like to submit my game to the following nominations:
                </label>
                <div class="col-sm-8 col-sm-offset-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <div ng-repeat="nomination in awards.nominations">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="nomination.checked"/>
                                        {{nomination.title}}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group" ng-show="awards.add">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 control-label text-left text-danger" ng-bind-html="awards.getDeadlineMessage()" ></label>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" class="btn btn-default" ui-sref="step3">Back</button>
                    <button ng-disabled="formS4.$invalid" ng-if="awards.add || !awards.allowed" type="submit" class="btn btn-success">Continue</button>
                    <button ng-if="!awards.add && awards.allowed" type="submit" class="btn btn-success">Skip</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
