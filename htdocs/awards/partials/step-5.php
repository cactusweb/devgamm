    <div class="game-form-wrapper">
        <ul class="dg-breadcrumbs">
            <li>
                <a ui-sref="step1">1. Ticket</a>
            </li>
            <li>
                <a ui-sref="step2">2. Game</a>
            </li>
            <li>
                <a ui-sref="step3">3. Developer</a>
            </li>
            <li>
                <a ui-sref="step4">4. Awards</a>
            </li>
            <li>
                <a ui-sref="step5" class="active">5. Game Lynch</a>
            </li>
            <li>
                <a ui-sref="step6">6. GAMM:Play</a>
            </li>
        </ul>
        <form class="form-horizontal" name="formS5" ng-submit="form.submit()">

            <div class="form-group" ng-show="lynch.allowed">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"
                                           ng-model="lynch.add"
                                           ng-disabled="!lynch.allowed"
                                           ng-click="checkStage()"/>
                                    Add game to <a href="{{lynch.link}}" target="_blank">Game Lynch</a> (presence at the conference is required)
                                </label>
                            </div>
                            <br/>
                            <div class="radio-group"  ng-show="lynch.add">
                                <div><b>Do you drink alcohol?</b></div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" ng-model="lynch.drink" value="1" />Yes, I do
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" ng-model="lynch.drink" value="0" />No, I don't
                                    </label>
                                </div>
                                <div ng-show="lynch.drink == undefined" class="error">Please select an answer</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="error" ng-show="raiseWarning">
                Only unfinished or unreleased games can take part in Game Lynch.<br>
                Please, skip this step.
            </div>

            <div class="form-group">
                <label class="col-sm-12 control-label text-left text-danger" ng-bind-html="lynch.getDeadlineMessage()" ></label>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" class="btn btn-default" ui-sref="step4">Back</button>
                    <button ng-if="lynch.add || !lynch.allowed"
                            ng-disabled="lynch.add && lynch.drink == undefined"
                            type="submit" class="btn btn-success">Continue</button>
                    <button ng-if="!lynch.add && lynch.allowed" type="submit" class="btn btn-success">Skip</button>
                </div>
            </div>
        </form>
    </div>
