    <div class="game-form-wrapper">
        <ul class="dg-breadcrumbs">
            <li>
                <a ui-sref="step1">1. Ticket</a>
            </li>
            <li>
                <a ui-sref="step2">2. Game</a>
            </li>
            <li>
                <a ui-sref="step3">3. Developer</a>
            </li>
            <li>
                <a ui-sref="step4">4. Awards</a>
            </li>
            <li>
                <a ui-sref="step5">5. Game Lynch</a>
            </li>
            <li>
                <a ui-sref="step6" class="active">6. GAMM:Play</a>
            </li>
        </ul>
        <form class="form-horizontal" name="formS6" ng-submit="form.submit()">

            <div class="form-group" ng-show="play.allowed">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="1"
                                           ng-model="play.add"
                                            ng-disabled="!play.allowed"/>
                                    I want to show my game at <a href="{{play.link}}" target="_blank">GAMM:Play</a> (presence is required)
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group required" ng-show="play.add">
                <label class="col-sm-4 control-label">
                    Your device to showcase game:
                </label>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="play.device.notebook"/>
                                        Notebook
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="play.device.smartphone"/>
                                        Smartphone
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="play.device.tablet"/>
                                        Tablet
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="play.device.VR_AR"/>
                                        VR/AR
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="play.device.console"/>
                                        Console
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label class="form-group-sm">
                                        <input type="checkbox" ng-model="play.device.other"/>
                                        Other <input ng-show="play.device.other" type="text" class="form-control" ng-model="play.device.otherText" >
                                    </label>
                                </div>
                                <div ng-show="play.error.device" class="error">Please specify your device</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group required" ng-show="play.add">
                <label class="col-sm-4 control-label">
                    I can showcase the game:
                    <div class="hint">Choose one or several time-slots</div>
                </label>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox-group" ng-repeat="showcase in play.showcase">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="showcase.checked"  ng-true-value="'{{showcase.title}}'"/>
                                        {{showcase.title}}
                                    </label>
                                </div>
                            </div>
                            <div ng-show="play.error.showcase" class="error">Please choose one or several time slots </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group required" ng-show="play.add">
                <label class="col-sm-4 control-label">
                    Logo or game screenshot
                    <div class="hint">1024x1024px, png, ai</div>
                </label>
                <div class="col-sm-8">
                    <div class="form-group row">
                        <div class="col-xs-12">
                            <div ng-show="play.logo.name">
                                <table>
                                    <tr>
                                        <td>{{play.logo.name}}</td>
                                        <td><span class="delete" ng-click="form.deleteLogo()"></span></td>
                                    </tr>
                                </table>
                            </div>
                            <button type="button" valid-file type="file" name="logo"
                                    ng-model="form.logo"
                                    ng-file-select
                                    multiple="false"
                                    accept="'image/jpeg,application/postscript'"
                                    ng-accept="'image/png,application/postscript'"
                                    resetOnClick="false"
                                    ng-disabled="play.logo.name"
                                >Add logo...</button>

                            <div class="upload_status" ng-show="form.upload && form.filePreloader"></div>
                        </div>
                    </div>
                    <div ng-show="!form.isOkLogo()" class="error">Please add logo or game screenshot</div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-12 control-label text-left text-danger" ng-bind-html="play.getDeadlineMessage()" ></label>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" class="btn btn-default" ui-sref="step5">Back</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>

    </div>
