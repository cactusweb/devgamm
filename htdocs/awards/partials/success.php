<div class="game-form-wrapper">
    <ul class="dg-breadcrumbs">
        <li>
            <a ui-sref="step1">1. Ticket</a>
        </li>
        <li>
            <a ui-sref="step2">2. Game</a>
        </li>
        <li>
            <a ui-sref="step3">3. Developer</a>
        </li>
        <li>
            <a ui-sref="step4">4. Awards</a>
        </li>
        <li>
            <a ui-sref="step5">5. Game Lynch</a>
        </li>
        <li>
            <a ui-sref="step6">6. GAMM:Play</a>
        </li>
    </ul>
    <div><p>Thank you for submitting your game.<br>
            Email us if you have any questions - <a href="mailto:contact@devgamm.com">contact@devgamm.com</a></p></div>
</div>