<?php

defined('BASE_DIR') || define('BASE_DIR', dirname(dirname(__FILE__)));
$yii = dirname(BASE_DIR) . '/framework/yii.php';
$config = BASE_DIR . '/application/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') || define('YII_DEBUG', true);  // @TODO вернуть в false
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') || define('YII_TRACE_LEVEL',3);

error_reporting(E_ALL ^ E_WARNING);

require_once $yii;

require_once BASE_DIR . '/application/components/WebApplication.php';

Yii::createApplication('WebApplication', $config)->run();
