-- -- settings
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `label` varchar(255) NOT NULL,
    `value` text NOT NULL,
    UNIQUE (`id`),
    PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

INSERT INTO `settings` (`id`, `name`, `label`, `value`) VALUES
(1, 'awards_deadline', 'awards deadline message', '<p>Games to Awards will be accepted till April 23, 2015 inclusive</p>\r\n'),
(2, 'game_submited_email_template', 'Email: Template', '<p>The game has been submitted successfully and will appear on our site soon.<br />\r\nYou can submit as many titles as you wish. Games are admitted till April 20.<br />\r\nPlease, address all your questions regarding the contest to <a href="mailto:contact@devgamm.com">contact@devgamm.com</a></p>\r\n'),
(3, 'game_submited_email_title', 'Email: Subject', '<p>DevGAMM Conference, Moscow 2015</p>\r\n'),
(4, 'game_edit_deadline', 'Edit Game: Deadline', '15.12.2015'),
(5, 'deadline_message_4_pre', 'Awards: Message before deadline', '<p>Pre deadline msg - step 4</p>\r\n'),
(6, 'deadline_message_4_post', 'Awards: Message after deadline', '<p>Post deadline msg - step 4</p>\r\n'),
(7, 'deadline_message_5_pre', 'Game Lynch: Message before deadline', '<p>Pre deadline msg - step 5</p>\r\n'),
(8, 'deadline_message_5_post', 'Game Lynch: Message after deadline', '<p>Post deadline msg - step 5</p>\r\n'),
(9, 'deadline_message_6_pre', 'GAMM:Play: Message before deadline', '<p>Pre deadline msg - step 6</p>\r\n'),
(10, 'deadline_message_6_post', 'GAMM:Play: Message after deadline', '<p>Post deadline msg - step 6</p>\r\n'),
(11, 'deadline_date_4', 'Awards: Deadline', '10.09.2015'),
(12, 'deadline_date_5', 'Game Lynch: Deadline', '10.09.2015'),
(13, 'deadline_date_6', 'GAMM:Play: Deadline', '10.09.2015'),
(14, 'awards_link', 'Awards: URL to Awards', 'http://devgamm.com/hamburg2015/en/games/awards/'),
(15, 'lynch_link', 'Game Lynch: URL to Lynch', 'http://devgamm.com/hamburg2015/en/conference/lynch/'),
(16, 'gammplay_link', 'GAMM:Play: URL to GAMM:Play', 'http://devgamm.com/hamburg2015/en/conference/gammplay/'),
(17, 'conversation_on', 'Meeting: enabled/disabled (1 = on; 0 = off)', '1'),
(18, 'conversation_off_msg', 'Meeting: Disabled message', '<p>Meeting system currently unavailable</p>\r\n'),;
(19, 'pitch_match_url', 'Pitch&Match URL', '');
-- --

-- -- game_category_new
DROP TABLE IF EXISTS `game_category_new`;
CREATE TABLE `game_category_new` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `icon` varchar(255) NOT NULL,
  UNIQUE (`id`),
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

INSERT INTO `game_category_new` (`id`, `parent_id`, `title`, `icon`) VALUES
(1, NULL, 'Mobile', ''),
(2, NULL, 'Consoles', ''),
(3, NULL, 'Social', ''),
(4, NULL, 'Web', ''),
(5, NULL, 'PC/Desktop', ''),
(20, 1, 'iOS', '/i/category/20/apple.png'),
(21, 1, 'Android', '/i/category/21/android.png'),
(22, 1, 'Windows Phone', '/i/category/22/windows.png'),
(23, 2, 'PS', ''),
(24, 2, 'Xbox', ''),
(25, 2, 'Wii U', ''),
(26, 3, 'Facebook', '/i/category/26/fb.png'),
(27, 3, 'VK', '/i/category/27/vk.png'),
(28, 3, 'Odnoklassniki', '/i/category/28/od.png'),
(29, 3, 'MoiMir@Mail.ru', '/i/category/29/mailru.png'),
(30, 5, 'Mac OS', '/i/category/30/apple.png'),
(31, 5, 'Linux', ''),
(32, 5, 'Steam', '/i/category/32/steam.png'),
(33, 4, 'Browser', ''),
(34, 5, 'Windows', '');
-- --

-- -- game_nomination
DROP TABLE IF EXISTS `game_nomination`;
CREATE TABLE `game_nomination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `menu_index` int(11) NOT NULL DEFAULT '1',
  UNIQUE (`id`),
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

INSERT INTO `game_nomination` (`id`, `title`, `menu_index`) VALUES
(1, 'Excellence in Visual Art', 1),
(2, 'Excellence in Game Design', 1),
(3, 'Excellence in Narrative', 1),
(4, 'Excellence in Audio', 1);
-- --

-- -- game_technology
DROP TABLE IF EXISTS `game_technology`;
CREATE TABLE `game_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `need_comment` int(11) NOT NULL DEFAULT '0',
  UNIQUE (`id`),
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;

INSERT INTO `game_technology` (`id`, `title`, `icon`, `comment`, `need_comment`) VALUES
(1, 'Unity', '', '', 0),
(2, 'Unreal Engine', '', '', 0),
(3, 'C#', '', '', 0),
(4, 'HTML5', '', '', 0),
(5, 'Java', '', '', 0),
(6, 'Java Script', '', '', 0),
(7, 'Flash/Air', '', '', 0),
(8, 'Marmalade', '', '', 0),
(9, 'Cocos', '', '', 0),
(10, 'Custom engine', '', '', 0),
(11, 'Other', '', '', 1);
-- --


DELIMITER $$

DROP PROCEDURE IF EXISTS upgrade_tables_2015 $$
CREATE PROCEDURE upgrade_tables_2015()
BEGIN

-- game
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='token' AND TABLE_NAME='game') ) THEN
    ALTER TABLE `game` ADD `token` VARCHAR(32) NOT NULL AFTER `showcase_logo`;
END IF;
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='lynch_drink' AND TABLE_NAME='game') ) THEN
    ALTER TABLE `game` ADD `lynch_drink` INT(11) NOT NULL DEFAULT '0' AFTER `lynch`;
END IF;
--
-- partner
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='ticket_id' AND TABLE_NAME='partner') ) THEN
    ALTER TABLE `partner` ADD `ticket_id` INT(11) NULL DEFAULT NULL AFTER `rating`;
END IF;
--
-- ticket
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='access_token' AND TABLE_NAME='ticket') ) THEN
    ALTER TABLE `ticket` ADD `access_token` VARCHAR(32) NULL DEFAULT NULL AFTER `event_title`;
END IF;
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='last_login' AND TABLE_NAME='ticket') ) THEN
    ALTER TABLE `ticket` ADD `last_login` TIMESTAMP NULL DEFAULT NULL AFTER `access_token`;
END IF;
--

END $$

CALL upgrade_tables_2015() $$

DELIMITER ;